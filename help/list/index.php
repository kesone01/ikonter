<?php
session_start();
require '../../config.php';
require '../../lib/session_user.php';

	    if (isset($_POST['kirim'])) {
			require '../../lib/session_login.php';
			$post_subjek = $conn->real_escape_string(trim(filter($_POST['subjek'])));
			$post_pesan = $conn->real_escape_string(trim(filter($_POST['pesan'])));

	        $error = array();
	        if (empty($post_subjek)) {
			    $error ['subjek'] = '*Tidak Boleh Kosong.';
	        } else if (strlen($post_subjek) > 200) {
			    $error ['subjek'] = '*Maksimal Pengisian Subjek Adalah 200 Karakter.';
	        }
	        if (empty($post_pesan)) {
			    $error ['pesan'] = '*Tidak Boleh Kosong.';
	        } else if (strlen($post_pesan) > 500) {
			    $error ['pesan'] = '*Maksimal Pengisian Pesan Adalah 500 Karakter.';
	        } else {

				$insert_tiket = $conn->query("INSERT INTO tiket VALUES ('', '$sess_username', '$post_subjek', '$post_pesan', '$date', '$time', '$date $time', 'Pending','1','0')");
				if ($insert_tiket == TRUE) {
					$_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Tiketmu Berhasil Dibuat, Harap Menunggu Respon Dari Admin Ya.<script>swal("Berhasil!", "Tiketmu Berhasil Dibuat Nih.", "success");</script>');
				} else {
					$_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
				}

			}
		}

		require("../../lib/header.php");

?>

                <div class="container-fluid">

 
                  
                        

                    <!-- Content Row -->

                    <div class="row">


                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">DAFTAR TICKET</h6>
                                    <a href="../" class="btn btn-primary btn-sm" style="float:right;"><i class="fas fa-comments"></i> BUAT TICKET</a>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                <form class="form-horizontal" method="GET">
                                    <div class="row">
                                        <div class="form-group col-lg-3">
                                            <label>Tampilkan Beberapa</label>
                                            <select class="form-control" name="tampil">
                                                <option value="10">Default</option>
                                                <option value="20">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>                                                
                                        <div class="form-group col-lg-3">
                                            <label>Filter Status</label>
                                            <select class="form-control" name="status">
                                                <option value="">Semua</option>
                                                <option value="Pending" >Pending</option>
                                                <option value="Closed" >Closed</option>
                                                <option value="Waiting" >Waiting</option>
                                                <option value="Responded" >Responded</option>
                                            </select>
                                        </div>                                                
                                        <div class="form-group col-lg-3">
                                            <label>Cari Kata Kunci</label>
                                            <input type="text" class="form-control" name="search" placeholder="Cari Kata Kunci" value="">
                                        </div>
                                        <div class="form-group col-lg-3">
                                            <label>Submit</label>
                                            <button type="submit" class="btn btn-block btn-primary">Cari</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                        <thead>
                                            <tr>
                                                <th>Tanggal & Waktu</th>
                                                <th>Update Terakhir</th>
                                                <th>Subjek</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php 
// start paging config
if (isset($_GET['search'])) {
    $cari = $conn->real_escape_string(filter($_GET['search']));
    $cari_status = $conn->real_escape_string(filter($_GET['status']));

    $cek_tiket = "SELECT * FROM tiket WHERE subjek LIKE '%$cari%' AND status LIKE '%$cari_status%' AND user = '$sess_username' ORDER BY id DESC"; // edit
} else {
    $cek_tiket = "SELECT * FROM tiket WHERE user = '$sess_username' ORDER BY id DESC"; // edit
}
if (isset($_GET['search'])) {
$cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
$records_per_page = $cari_urut; // edit
} else {
    $records_per_page = 10; // edit
}

$starting_position = 0;
if(isset($_GET["halaman"])) {
    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
}
$new_query = $cek_tiket." LIMIT $starting_position, $records_per_page";
$new_query = $conn->query($new_query);
// end paging config
while ($data_tiket = $new_query->fetch_assoc()) {
    if ($data_tiket['status'] == "Pending") {
        $label = "warning";
        $btn = "";
    } else if ($data_tiket['status'] == "Closed") {
        $label = "danger";
        $btn = "disabled";
    } else if ($data_tiket['status'] == "Waiting") {
        $label = "info";   
        $btn = ""; 
    } else if ($data_tiket['status'] == "Responded") {
        $label = "success";
        $btn = "";       
    }
?>
                                            <tr>
                                                <td><?php echo tanggal_indo($data_tiket['date']); ?>, <?php echo $data_tiket['time']; ?></td>
                                                <td><?php echo time_elapsed_string($data_tiket['update_terakhir']); ?></td>
                                                <td><span class="btn btn-warning btn-elevate btn-pill btn-elevate-air btn-sm"><?php echo $data_tiket['subjek']; ?></span></td>
                                                <td><span class="btn btn-<?php echo $label; ?> btn-elevate btn-pill btn-elevate-air btn-sm"><?php echo $data_tiket['status']; ?></span></td>
                                                <td align="center"><a href="<?php echo $config['web']['url']; ?>page/help-reply?id=<?php echo $data_tiket['id']; ?>" class="btn btn-primary btn-elevate btn-pill btn-elevate-air btn-sm <?php echo $btn; ?>" ><i class="fa fa-reply"></i> Balas</a></td>
                                            </tr>   
<?php } ?>
                                        </tbody>
                                    </table>
                                    <br>
                                   <div class="paginate_button page-item previous disabled">
                                        <ul class="pagination justify-content-center"">
<?php
// start paging link
if (isset($_GET['search'])) {
$cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
} else {
$cari_urut =  10;
}  
if (isset($_GET['search'])) {
    $cari = $conn->real_escape_string(filter($_GET['search']));
    $cari_status = $conn->real_escape_string(filter($_GET['status']));
    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
} else {
    $self = $_SERVER['PHP_SELF'];
}
$cek_tiket = $conn->query($cek_tiket);
$total_records = mysqli_num_rows($cek_tiket);
echo "<li class='page-link'><a href='#'>Total Data : ".$total_records."</a></li>&nbsp";
if($total_records > 0) {
    $total_pages = ceil($total_records/$records_per_page);
    $current_page = 1;
    if(isset($_GET["halaman"])) {
        $current_page = $conn->real_escape_string(filter($_GET["halaman"]));
        if ($current_page < 1) {
            $current_page = 1;
        }
    }
    if($current_page > 1) {
        $previous = $current_page-1;
    if (isset($_GET['search'])) {
    $cari = $conn->real_escape_string(filter($_GET['search']));
    $cari_status = $conn->real_escape_string(filter($_GET['status']));
    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1&tampil=".$cari_urut."&status=".$cari_status."&search=".$cari."'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."&tampil=".$cari_urut."&status=".$cari_status."&search=".$cari."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
} else {
        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
}
}
    // limit page
    $limit_page = $current_page+3;
    $limit_show_link = $total_pages-$limit_page;
    if ($limit_show_link < 0) {
        $limit_show_link2 = $limit_show_link*2;
        $limit_link = $limit_show_link - $limit_show_link2;
        $limit_link = 3 - $limit_link;
    } else {
        $limit_link = 3;
    }
    $limit_page = $current_page+$limit_link;
    // end limit page
    // start page
    if ($current_page == 1) {
        $start_page = 1;
    } else if ($current_page > 1) {
        if ($current_page < 4) {
            $min_page  = $current_page-1;
        } else {
            $min_page  = 3;
        }
        $start_page = $current_page-$min_page;
    } else {
        $start_page = $current_page;
    }
    // end start page
    for($i=$start_page; $i<=$limit_page; $i++) {
    if (isset($_GET['cari'])) {
    $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
    $cari_status = $conn->real_escape_string(filter($_GET['status']));
    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
        if($i==$current_page) {
            echo "<li class='btn btn-primary btn-sm'><a href='#'>".$i."</a></li>";
        } else {
            echo "<li class='btn btn-info btn-sm'><a href='".$self."?halaman=".$i."&tampil=".$cari_urut."&status=".$cari_status."&search=".$cari."'>".$i."</a></li>";
        }
    } else {
        if($i==$current_page) {
            echo "<li class='btn btn-info btn-sm'><a href='#'>".$i."</a></li>";
        } else {
            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$i."'>".$i."</a></li>";
        }        
    }
    }
    if($current_page!=$total_pages) {
        $next = $current_page+1;
    if (isset($_GET['search'])) {
    $cari = $conn->real_escape_string(filter($_GET['search']));
    $cari_status = $conn->real_escape_string(filter($_GET['status']));
    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."&tampil=".$cari_urut."&status=".$cari_status."&search=".$cari."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."&tampil=".$cari_urut."&status=".$cari_status."&search=".$cari."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
} else {
        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
    }
}
}
// end paging link
?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Help -->
		
		</div>
		<?php 
require '../../lib/footer.php';
?>