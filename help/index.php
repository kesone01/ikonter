<?php
session_start();
require '../config.php';
require '../lib/session_user.php';

	    if (isset($_POST['kirim'])) {
			require '../lib/session_login.php';
			$post_subjek = $conn->real_escape_string(trim(filter($_POST['subjek'])));
			$post_pesan = $conn->real_escape_string(trim(filter($_POST['pesan'])));

	        $error = array();
	        if (empty($post_subjek)) {
			    $error ['subjek'] = '*Tidak Boleh Kosong.';
	        } else if (strlen($post_subjek) > 200) {
			    $error ['subjek'] = '*Maksimal Pengisian Subjek Adalah 200 Karakter.';
	        }
	        if (empty($post_pesan)) {
			    $error ['pesan'] = '*Tidak Boleh Kosong.';
	        } else if (strlen($post_pesan) > 500) {
			    $error ['pesan'] = '*Maksimal Pengisian Pesan Adalah 500 Karakter.';
	        } else {

				$insert_tiket = $conn->query("INSERT INTO tiket VALUES ('', '$sess_username', '$post_subjek', '$post_pesan', '$date', '$time', '$date $time', 'Pending','1','0')");
				if ($insert_tiket == TRUE) {
					$_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Tiketmu Berhasil Dibuat, Harap Menunggu Respon Dari Admin Ya.<script>swal("Berhasil!", "Tiketmu Berhasil Dibuat Nih.", "success");</script>');
				} else {
					$_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
				}

			}
		}

		require("../lib/header.php");

?>

                <div class="container-fluid">

 
                  
                        

                    <!-- Content Row -->

                    <div class="row">


                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">OPEN TICKET</h6>
                                    <a href="list" class="btn btn-primary btn-sm" style="float:right;"><i class="fas fa-list"></i> DAFTAR TICKET</a>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                      
         
                            <?php
                            if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<?php echo $_SESSION['hasil']['pesan'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                            }
                            ?>
							
							<form class="form-horizontal" method="POST">
								<input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
									<div class="form-group">
										<label class="col-lg-10 control-label">Subjek</label>
										<div class="col-lg-12">
											<select class="form-control" name="subjek" id="subjek">
											    <option value="Pesanan">Pesanan</option>
											    <option value="Isi Saldo">Isi Saldo</option>
											    <option value="Layanan">Layanan</option>
											    <option value="Lainnya">Lainnya</option>
											</select>
										    <span class="form-text text-muted"><?php echo ($error['subjek']) ? $error['subjek'] : '';?></span>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-lg-10 control-label">Pesan</label>
										<div class="col-lg-12">
											<textarea type="text" class="form-control" placeholder="Tulis Keluhan Pesanan, Deposit, Tentang Layanan, Atau Yang Lainya" value="<?php echo $post_pesan; ?>" name="pesan" rows="5"></textarea>
										    <span class="form-text text-muted"><?php echo ($error['pesan']) ? $error['pesan'] : '';?></span>
										</div>
									</div>
									<div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-lg-5 col-xl-5">
                                                </div>
                                                <div class="col-lg-5 col-xl-5">
                                                    <button type="submit" name="kirim" class="btn btn-primary btn-elevate btn-pill btn-elevate-air">Kirim</button>
                                                    <button type="reset" class="btn btn-danger btn-elevate btn-pill btn-elevate-air">Reset</button>
                                                </div>
                                            </div>
                                        </div>
									</div>
								</form>
                            </div>
                            

        </div>      </div>      </div>
        <!-- End Content -->

        <!-- Start Scrolltop -->
		
		<!-- End Scrolltop -->


<?php 
require '../lib/footer.php';
?>