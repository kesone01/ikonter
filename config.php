<?php
//************************************************
//
//************************************************

date_default_timezone_set('Asia/Jakarta');
error_reporting(0);

$maintenance = 0; // Maintenance? 1 = ya 0 = tidak

$settingURL = 'https://iaccesoris.com/';
// $settingURL = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
// $settingURL .= "://".$_SERVER['HTTP_HOST'];
// $settingURL .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

if($maintenance == 1) {
    die("<center>Sedang dalam perbaikan, coba lagi nanti.</center>");
}
// database
$config['db'] = array(
    'host' => 'localhost',
    'name' => 'iaccesor_konter',  //name dabatase
    'username' => 'iaccesor_admin', //username yourtabase
    'password' => 'busXErdAKc_*' //password your database
);

$conn = mysqli_connect($config['db']['host'], $config['db']['username'], $config['db']['password'], $config['db']['name']);
$mysqli = new mysqli($config['db']['host'],$config['db']['username'],$config['db']['password'],$config['db']['name']);
if(!$conn) {
	die("Koneksi Gagal : ".mysqli_connect_error());
	}
$config['web'] = array(
	'url' => $settingURL
);
// date & time
$date = date("Y-m-d");
$time = date("H:i:s");
require("lib/function.php");
require("lib/setting.php");
require("lib/JsonGetLayanan.php");
$getOperator = new JsonGetLayanan();
//$getOperator->setPathJson("../Jsn/jsn.json");

function connect($end_point, $post)
{
    $_post = array();
    if (is_array($post)) {
        foreach ($post as $name => $value) {
            $_post[] = $name . '=' . urlencode($value);
        }
    }
    $ch = curl_init($end_point);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    if (is_array($post)) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $_post));
    }
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    $result = curl_exec($ch);
    if (curl_errno($ch) != 0 && empty($result)) {
        $result = false;
    }
    curl_close($ch);
    return $result;
}

?>