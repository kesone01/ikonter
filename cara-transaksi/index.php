<?php

require '../config.php';
require '../lib/database.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $data['title']; ?></title>
<meta name="description" content="<?php echo $data['short_title']; ?>" />
    <meta name="keywords" content="<?php echo $data['short_title']; ?>, smm ppob, ppob termurah, game online terlengkap" />
    <meta name="author" content="Konterin" />
	<!-- Site favicon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/faviconn.png" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-white sidebar sidebar-primary accordion toggled" >

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center">
                 <div class="sidebar-brand-icon text-gray-200 rounded">
        <img width="100%" src="/image/logo/logo.png">
                </div>
               
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/">
                    <i class="fas fa-home"></i>
                    <span>Home</span></a>
            </li>

            <!-- Divider -->

      <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/status">
                    <i class="fas fa-sync"></i>
                    <span>Status Transaksi</span></a>
            </li>

<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/cara-transaksi">
                    <i class="fas fa-shopping-basket"></i>
                    <span>Cara Transaksi</span></a>
            </li>
			
			<hr class="sidebar-divider my-1">
			            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/daftar-harga">
                    <i class="fas fa-list"></i>
                    <span>Daftar Harga</span></a>
            </li>
			
						
						<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/kontak-kami">
                    <i class="fas fa-envelope"></i>
                    <span>Kontak Kami</span></a>
            </li>
			<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/api">
                    <i class="fas fa-fw fa-code"></i>
                    <span>API Documentation</span></a>
            </li>

          
<div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

               <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                 
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

               

            

                </nav>

                <!-- Begin Page Content -->
                <div class="container-fluid">

 
                    <!-- Content Row -->
                    <div class="row">
					  <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-shopping-basket"></i> CARA TRANSAKSI</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
          <div class="row">
             <div class="col-sm-4 col-md-4">
                <!-- Start User Friendly Block -->
                <div class="box-content text-center">
                   <div class="block-icon">
                      <i class="fa fa-3x fa-user-plus color-primary"></i>
                   </div>
                   <h3>Melakukan Pendaftaran</h3>
                                 <hr>
                   <p>Pendaftaran tanpa biaya 100% Gratis, setelah mendaftar akun anda langsung aktif dan dapat melakukan deposit.</p>
                </div>
                <!-- End Block -->
             </div>
             <div class="col-sm-4 col-md-4">
                <!-- Start Supper Fast Block -->
                <div class="box-content text-center">
                   <div class="block-icon">
                      <i class="fas fa-3x fa-plus-circle color-success"></i>
                   </div>
                   <h3>Melakukan Deposit Saldo</h3>
                                 <hr>
                   <p>Langkah selanjutnya melakukan Deposit Saldo agar dapat digunakan untuk transaksi semua produk terlengkap dari kami.</p>
                </div>
                <!-- End Block -->
             </div>
             <div class="col-sm-4 col-md-4">
                <!-- Start Analytics Block -->
                <div class="box-content text-center">
                   <div class="block-icon">
                      <i class="fa fa-3x fa-shopping-cart color-danger"></i>
                   </div>
                   <h3>Melakukan Transaksi</h3>
                                 <hr>
                   <p>Langkah terakhir melakukan transaksi anda dengan produk terlengkap dan termurah dari kami KONTERIN.</p>
                </div>
                <!-- End Block -->
             </div>
          </div>
			</div>		</div>	
			                    <div class="row">

			<div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-university"></i> METODE PEMBAYARAN</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
								<div class="row justify-content-center">
								
                                   <div class="col-4 mt-3">
                <center>
                 
                            <img src="/img/QRIS_logo.svg" width="99%" >
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-5">
          
               </div>
                </center>
                 </div>
	
				 
				 
				                             <div class="col-4 mt-3">
                <center>
      
                                     <img src="/img/bca-logo.png" width="68%" >
               
           
                </center>
                 </div>
				             </div>
                                  <p>Pembayaran di Konterin Sangat Mudah dan Proses Cepat, Kami Menyediakan Metode Pembayaran <b>BCA</b> dan <b>QRIS</b>. Untuk Pembayaran <b>QRIS</b> Dapat Menerima Pembayaran melalui <b>SEMUA BANK</b> ataupun <b>SEMUA E-WALLET</b>.</p>
                              <p>Semua Proses di Konterin dari Transaksi maupun Deposit akan Diproses Secara <b>Otomatis.</b> Sistem Kami selalu Online dan membaca selama 7x24 JAM NONSTOP. Jadi anda Tidak Perlu Menunggu lama.</p>  
                              </div>
                            </div>
        

<?php 
require '../lib/footer.php';
?>