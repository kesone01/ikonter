<?php

require '../config.php';
require '../lib/database.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $data['title']; ?></title>
<meta name="description" content="<?php echo $data['short_title']; ?>" />
    <meta name="keywords" content="<?php echo $data['short_title']; ?>, smm ppob, ppob termurah, game online terlengkap" />
    <meta name="author" content="Konterin" />
	<!-- Site favicon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/faviconn.png" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-white sidebar sidebar-primary accordion toggled" >

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center">
                 <div class="sidebar-brand-icon text-gray-200 rounded">
        <img width="100%" src="/image/logo/logo.png">
                </div>
               
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/">
                    <i class="fas fa-home"></i>
                    <span>Home</span></a>
            </li>

            <!-- Divider -->

      <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/status">
                    <i class="fas fa-sync"></i>
                    <span>Status Transaksi</span></a>
            </li>

<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/cara-transaksi">
                    <i class="fas fa-shopping-basket"></i>
                    <span>Cara Transaksi</span></a>
            </li>
			
					<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/daftar-harga">
                    <i class="fas fa-list"></i>
                    <span>Daftar Harga</span></a>
            </li>
			
						
						<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/kontak-kami">
                    <i class="fas fa-envelope"></i>
                    <span>Kontak Kami</span></a>
            </li>
			<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/api">
                    <i class="fas fa-fw fa-code"></i>
                    <span>API Documentation</span></a>
            </li>

          
<div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

               <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                 
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

               

            

                </nav>

                <!-- Begin Page Content -->
                <div class="container-fluid">
				 <!-- Content Row -->
                    <div class="row">
					  <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-fw fa-code"></i> API DOCUMENTATION</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
				        <div class="table-responsive">
				            <table class="table table-bordered">
					            <tbody>
						            <tr>
							            <td>HTTP Method</td>
							            <td>POST</td>
						            </tr>
						            <tr>
							            <td>API URL</td>
							            <td><?php echo $config['web']['url'] ?>api/</td>
						            </tr>
						            <tr>
							            <td>Format Respons</td>
							            <td>JSON</td>
						            </tr>
						            <tr>
							            <td>Api Kategori</td>
							            <td><form class="form-horizontal" role="form" method="POST">
									            <div class="form-group">
										            <div class="col-md-8 pull-left">
											            <select class="form-control" id="api">
											            	<option value="0">Pilih salah satu...</option>
											            	<option value="api_top_up">Api Pembelian</option>
												            <option value="api_account_information">Api Informasi Akun</option>
											            </select>
										         
								            </form>
							            </td>
						            </tr>
					            </tbody>
				            </table>
				        </div>
				  
		    </div>
		
		<!-- End Page API Documentation -->

		</div>
		<!-- End Content -->

        <!-- Start Scrolltop -->
		
		<!-- End Scrolltop -->
                      <div class="row">
					  <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"></h6>
                                    
                                </div>
				<div class="card-body" id="fitur">
	</div>
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.js"></script>
		<script type="text/javascript">
	       var htmlobjek;
            $(document).ready(function(){

                $("#api").change(function(){
                    var api = $("#api").val();
                $.ajax({
                    url: '<?php echo $config['web']['url'] ?>ajax/api-include.php',
                    data: 'api='+api,
                    type: 'POST',
                    dataType: 'html',
                    success: function(msg){
                        $("#fitur").html(msg);
                    }
                });
            });
        });
		</script>
		
		<br />
		<br />

<?php
include("../lib/footer.php");
?>