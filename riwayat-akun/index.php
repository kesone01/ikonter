<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/check_status.php';
require '../lib/header.php';
?>

<!-- Begin Page Content -->
                <div class="container-fluid">

 
                  
                        

                    <!-- Content Row -->

                    <div class="row">


                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                               <form class="form-horizontal" method="GET">
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">RIWAYAT PEMBELIAN</h6>
                                                 <div float="right">

                                            <input type="text" class="form-control" name="cari" placeholder="CARI NO/TUJUAN" value="">
                               
                                        </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
			          
             
                              	<div class="row align-items-center">
					 <div class="form-group col-lg-3">

                                            <select class="form-control" name="tampil" hidden>
                                                <option value="10">Default</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>                                                
                                        <div class="form-group col-lg-3" hidden>

                                            <select class="form-control" name="status">
                                                <option value="">Semua</option>
                                                <option value="Pending">Pending</option>
                                                <option value="Processing">Processing</option>
                                                <option value="Success">Success</option>
                                                <option value="Error">Error</option>
                                                <option value="Partial">Partial</option>
                                            </select>
                                        </div>                                                
                           
                                              <button type="submit" class="btn btn-block btn-primary" hidden>Cari</button>

                                    </div>
                                </form>
                               <div class="table-responsive">
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                        <thead>
                                            <tr>

                                                <th>KODE TRX</th>
                                                <th>TGL&WAKTU</th>
                                                <th>PRODUK</th>
                                                <th>TUJUAN</th>
                                                <th>HARGA</th>
                                                <th>STATUS</th>
                                                <th>DETAIL</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php 
// start paging config
if (isset($_GET['cari'])) {
    $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
    $cari_status = $conn->real_escape_string(filter($_GET['status']));

    $cek_pesanan = "SELECT * FROM pembelian_pulsa WHERE target LIKE '%$cari_oid%' AND status LIKE '%$cari_status%' AND user = '$sess_username' ORDER BY id DESC"; // edit
} else {
    $cek_pesanan = "SELECT * FROM pembelian_pulsa WHERE user = '$sess_username' ORDER BY id DESC"; // edit
}
if (isset($_GET['cari'])) {
$cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
$records_per_page = $cari_urut; // edit
} else {
    $records_per_page = 10; // edit
}

$starting_position = 0;
if(isset($_GET["halaman"])) {
    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
}
$new_query = $cek_pesanan." LIMIT $starting_position, $records_per_page";
$new_query = $conn->query($new_query);
// end paging config
while ($data_pesanan = $new_query->fetch_assoc()) {
    if ($data_pesanan['status'] == "Pending") {
        $label = "warning";
    } else if ($data_pesanan['status'] == "Partial") {
        $label = "danger";
    } else if ($data_pesanan['status'] == "Error") {
        $label = "danger";    
    } else if ($data_pesanan['status'] == "Processing") {
        $label = "primary";    
    } else if ($data_pesanan['status'] == "Success") {
        $label = "success";    
    }
    if ($data_pesanan['refund'] == "0") {
        $icon2 = "times-circle";
        $label2 = "danger"; 
    } else if ($data_pesanan['refund'] == "1") {
        $icon2 = "check";
        $label2 = "success";
    }
?>
                                            <tr>
                                                    <td align="center" hidden><?php if($data_pesanan['place_from'] == "API") { ?><i class="fa fa-random"></i><?php } else { ?><i class="flaticon-globe"></i><?php } ?></td>
                                                <td><span class="view_all_order badge badge-info" data-toggle="modal" id="<?php echo $data_pesanan['id']; ?>" data-target='#myDetailAll'><?php echo $data_pesanan['oid']; ?></span></td>
                                                <td><?php echo tanggal_indo($data_pesanan['date']); ?>, <?php echo $data_pesanan['time']; ?></td>
                                                <td><?php echo $data_pesanan['layanan']; ?></td>
                                                <td style="min-width: 200px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control form-control-sm" value="<?php echo $data_pesanan['target']; ?>" id="target-<?php echo $data_pesanan['oid']; ?>" readonly="">
                                                    <button data-toggle="tooltip" title="Copy Target" class="btn btn-primary btn-sm" type="button" onclick="copy_to_clipboard('target-<?php echo $data_pesanan['oid']; ?>')"><i class="fas fa-copy text-white"></i></button>
                                                </div>
                                                <td>Rp <?php echo number_format($data_pesanan['harga'],0,',','.'); ?></td>
                                                <td><span class="btn btn-<?php echo $label; ?> btn-elevate btn-pill btn-elevate-air btn-sm"><?php echo $data_pesanan['status']; ?></span></td>
                                                <td align="center">
                                                    <a href="<?php echo $config['web']['url'] ?>page/receipt?oid=<?php echo $data_pesanan['oid']; ?>" class="btn btn-primary btn-elevate btn-circle btn-icon"><i class="fa fa-receipt"></i></a>
                                                </td>
                                            </tr>   
<?php } ?>
                                        </tbody>
                                    </table>
                                    <br>

                                    <div class="paginate_button page-item previous disabled">
                                        <ul class="pagination justify-content-center"">
<?php
// start paging link
if (isset($_GET['cari'])) {
$cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
} else {
$cari_urut =  10;
}  
if (isset($_GET['cari'])) {
    $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
} else {
    $self = $_SERVER['PHP_SELF'];
}
$cek_pesanan = $conn->query($cek_pesanan);
$total_records = mysqli_num_rows($cek_pesanan);
echo "<div class='page-link'>Total Data : ".$total_records."</div><br> &nbsp&nbsp&nbsp ";
if($total_records > 0) {
    $total_pages = ceil($total_records/$records_per_page);
    $current_page = 1;
    if(isset($_GET["halaman"])) {
        $current_page = $conn->real_escape_string(filter($_GET["halaman"]));
        if ($current_page < 1) {
            $current_page = 1;
        }
    }
    if($current_page > 1) {
        $previous = $current_page-1;
    if (isset($_GET['cari'])) {
    $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
    $cari_status = $conn->real_escape_string(filter($_GET['status']));
    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
        echo "<li class='page-item'><a href='".$self."?halaman=1&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
        echo "<li class='dataTables_paginate paging_simple_numbers'><a href='".$self."?halaman=".$previous."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
} else {
        echo "<li class='btn btn-white btn-sm'><a href='".$self."?halaman=1'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
        echo "<li class='btn btn-white btn-sm'><a href='".$self."?halaman=".$previous."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
}
}
    // limit page
    $limit_page = $current_page+3;
    $limit_show_link = $total_pages-$limit_page;
    if ($limit_show_link < 0) {
        $limit_show_link2 = $limit_show_link*2;
        $limit_link = $limit_show_link - $limit_show_link2;
        $limit_link = 3 - $limit_link;
    } else {
        $limit_link = 3;
    }
    $limit_page = $current_page+$limit_link;
    // end limit page
    // start page
    if ($current_page == 1) {
        $start_page = 1;
    } else if ($current_page > 1) {
        if ($current_page < 4) {
            $min_page  = $current_page-1;
        } else {
            $min_page  = 3;
        }
        $start_page = $current_page-$min_page;
    } else {
        $start_page = $current_page;
    }
    // end start page
    for($i=$start_page; $i<=$limit_page; $i++) {
    if (isset($_GET['cari'])) {
    $cari_oid = $conn->real_escape_string(filter($_GET['cari']));

        if($i==$current_page) {
            echo "<li class='btn btn-white btn-sm'><a href='#'>".$i."</a></li>";
        } else {
            echo "<li class='btn btn-white btn-sm'><a href='".$self."?halaman=".$i."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'>".$i."</a></li>";
        }
    } else {
        if($i==$current_page) {
            echo "<li class='page-item'><a class='btn btn-primary btn-sm' href='#'>".$i."</a></li> &nbsp";
        } else {
            echo "<li class='page-item'><a class='btn btn-primary btn-sm' href='".$self."?halaman=".$i."'>".$i."</a></li>&nbsp";
        }        
    }
    }
    if($current_page!=$total_pages) {
        $next = $current_page+1;
    if (isset($_GET['cari'])) {
    $cari_oid = $conn->real_escape_string(filter($_GET['cari']));

        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
} else {
        echo "<li class='btn btn-white btn-sm'><a href='".$self."?halaman=".$next."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
        echo "<li class='btn btn-white btn-sm'><a href='".$self."?halaman=".$total_pages."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
    }
}
}
// end paging link
?>

                                      
        </div>
        <!-- End Content -->


<?php
require '../lib/footer.php';
?>

	   