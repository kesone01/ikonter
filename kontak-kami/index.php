<?php

require '../config.php';
require '../lib/database.php';
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $data['title']; ?></title>
        <meta name="description" content="<?php echo $data['short_title']; ?>"/>
        <meta name="keywords"
              content="<?php echo $data['short_title']; ?>, smm ppob, ppob termurah, game online terlengkap"/>
        <meta name="author" content="iaccesoris"/>
        <!-- Site favicon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/faviconn.png"/>
        <!-- Custom fonts for this template-->
        <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
                href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
                rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="/css/sb-admin-2.min.css" rel="stylesheet">

    </head>

    <body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-white sidebar sidebar-primary accordion toggled">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center">
                <div class="sidebar-brand-icon text-gray-200 rounded">
                    <img width="100%" src="/image/logo/logo.png">
                </div>

            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/">
                    <i class="fas fa-home"></i>
                    <span>Home</span></a>
            </li>

            <!-- Divider -->

            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/status">
                    <i class="fas fa-sync"></i>
                    <span>Status Transaksi</span></a>
            </li>

            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/cara-transaksi">
                    <i class="fas fa-shopping-basket"></i>
                    <span>Cara Transaksi</span></a>
            </li>

            <hr class="sidebar-divider my-1">
            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/daftar-harga">
                    <i class="fas fa-list"></i>
                    <span>Daftar Harga</span></a>
            </li>

            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/kontak-kami">
                    <i class="fas fa-envelope"></i>
                    <span>Kontak Kami</span></a>
            </li>

            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/api">
                    <i class="fas fa-fw fa-code"></i>
                    <span>API Documentation</span></a>
            </li>


            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>


                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">


                </nav>

                <!-- Begin Page Content -->
                <div class="container-fluid">


                    <!-- Content Row -->
                    <div class="row">
                        <div class="col-xl-6 col-lg-7">
                            <div class="card shadow mb-4">

                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-envelope"></i> KONTAK
                                        KAMI</h6>

                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-7 col-md-7 justify-content-center">
                                            <p>Alamat : JL. Karonsih Utara 8, 50181</p>
                                            <p>WhatsApp : <a href="https://wa.me/62895358390756">08595358390756</a></p>
                                            <p>Email : <a href="mailto:cs@iaccesoris.com">cs@iaccesoris.com</a></p>


                                            <!-- End Block -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-xl-6 col-lg-7">
                            <div class="card shadow mb-4">

                                <div
                                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-map-marker"></i> MAPS
                                    </h6>

                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1441.1287459033128!2d110.3522425007502!3d-6.998911373685066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708abe2ec10511%3A0x9653d0579a943d08!2sJl.%20Karonsih%20Utara%20VIII%2C%20Ngaliyan%2C%20Kec.%20Ngaliyan%2C%20Kota%20Semarang%2C%20Jawa%20Tengah%2050181!5e0!3m2!1sen!2sid!4v1636347721898!5m2!1sen!2sid"
                                                width="100%" height="120" style="border:0;" allowfullscreen=""
                                                loading="lazy"></iframe>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">

                                <div
                                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-info-circle"></i>
                                        KETENTUAN LAYANAN</h6>

                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="row justify-content-center">

                                        <table class="table table-striped hidden-phone" id="kt_table_1">
                                            <thead>

                                            <tr>
                                                <td>

                                                    <div id="tos-content">
                                                        <p>Dengan bertransaksi di <a href="/">iaccesoris</a> maka secara
                                                            otomatis Anda telah menyetujui segala kebijakan yang
                                                            berlaku. Harap membaca kebijakan baik-baik sebelum Anda
                                                            melakukan transaksi.</p>
                                                        <h3>A. Hal Umum</h3>
                                                        <ol>
                                                            <li>Iaccesoris bertindak sebagai perantara antara pembeli
                                                                dengan penyedia layanan operator. iaccesoris bukan
                                                                merupakan bagian dari operator.
                                                            </li>
                                                            <li>Hal-hal seperti gangguan operator; sinyal internet
                                                                lemah; atau keluhan lainnya terkait operator merupakan
                                                                hal diluar kendali iaccesoris.
                                                            </li>
                                                        </ol>
                                                        <h3>B. Pembelian Produk</h3>
                                                        <ol>
                                                            <li>Keterangan produk ditampilkan di website. Apabila
                                                                keterangan dirasa kurang lengkap, pembeli dapat
                                                                menghubungi customer service. Sebelum bertransaksi mohon
                                                                untuk dibaca baik-baik keterangan produk yang akan
                                                                dipesan.
                                                            </li>
                                                            <li>Harga dapat berubah sewaktu-waktu tanpa pemberitahuan di
                                                                website. Tergantung kebijakan dari operator pusat.
                                                            </li>
                                                            <li>Data pesanan seperti produk yang dipesan dan nomor HP
                                                                tujuan didapatkan dari form order yang sudah diinput
                                                                oleh pelanggan. Pastikan Anda telah memasukkan data
                                                                dengan benar. Kesalahan input data yang mengakibatkan
                                                                produk tidak dapat terkirim ke nomor HP tujuan atau
                                                                akibat lainnya bukan merupakan tanggung jawab
                                                                iaccesoris.
                                                            </li>
                                                        </ol>
                                                        <h3>C. Pembayaran</h3>
                                                        <ol>
                                                            <li>Pembayaran dapat dilakukan dengan menggunakan mata uang
                                                                Rupiah apabila menggunakan pilihan metode pembayaran
                                                                transfer bank, mata uang Dollar Amerika (USD) apabila
                                                                menggunakan pilihan pembayaran PayPal, dan BTC apabila
                                                                menggunakan metode pembayaran BitCoin.
                                                            </li>
                                                            <li>Metode bank transfer dapat dilakukan menggunakan
                                                                internet banking, mobile banking, sms banking, transfer
                                                                ATM, atau transfer langsung ke cabang bank.
                                                            </li>
                                                            <li>Biaya transfer yang berlaku (apabila ada) sepenuhnya
                                                                merupakan tanggung jawab pembeli.
                                                            </li>
                                                            <li>Pembayaran menggunakan PayPal wajib menggunakan metode
                                                                pembayaran pribadi / keluarga / teman (personal payment)
                                                                dengan mencantumkan faktur (invoice) yang didapatkan
                                                                pada saat melakukan pemesanan. Apabila tidak sesuai,
                                                                dana akan dikembalikan dengan biaya PayPal yang
                                                                dibebankan kepada pembeli.
                                                            </li>
                                                            <li>Gangguan pihak bank / E-Curency sehingga mengakibatkan
                                                                sistem tidak bisa membaca mutasi bukan merupakan
                                                                tanggung jawab iaccesoris. Setelah melakukan transfer
                                                                harap menyimpan bukti transfer.
                                                            </li>
                                                            <li>Kesalahan dalam penulisan nomor rekening dan informasi
                                                                lainnya oleh pembeli atau kelalaian pihak bank bukan
                                                                merupakan tanggung jawab iaccesoris
                                                            </li>
                                                            <li>Iaccesoris tidak menerima pembayaran di belakang.</li>
                                                            <li>Rekening bank / E- Curency yang digunakan pembeli untuk
                                                                mentransfer uang adalah rekening yang sah dimana pemilik
                                                                rekening mengetahui dan setuju dengan transaksi yang
                                                                dilakukan pembelian, iaccesoris tidak dapat dituntut dan
                                                                tidak dapat dimintai tanggung jawab atas kerugian yang
                                                                dialami pemilik rekening pengirim.
                                                            </li>
                                                            <li>Jika tidak ada pembayaran melebihi batas waktu dari
                                                                sistem, maka pesanan otomatis dibatalkan.
                                                            </li>
                                                            <li>Segala biaya yang timbul dari proses pembayaran akan
                                                                menjadi tanggung jawab Pembeli.
                                                            </li>
                                                        </ol>
                                                        <h3>D. Pembatalan Transaksi</h3>
                                                        <ol>
                                                            <li>Produk mengalami gangguan, nomor yang diinput tidak
                                                                berlaku/diluar masa tenggang, atau sebab kuat lainnya
                                                                maka pembeli berhak melakukan pembatalan transaksi.
                                                            </li>
                                                            <li>Proses pengisian produk dan belum ada laporan
                                                                sukses/gagal dari server pusat, maka pembelian tidak
                                                                bisa dibatalkan.
                                                            </li>
                                                            <li>Pesanan yang dilakukan pada saat jam mutasi perbankan
                                                                offline dan sedang menunggu proses, tidak bisa
                                                                dibatalkan.
                                                            </li>
                                                            <li>Dalam hal kesalahan transfer yang diakibatkan kelalaian
                                                                pelanggan (salah mentransfer Nominal / Jumlah dan Berita
                                                                yang telah diinstruksikan oleh sistem), maka refund /
                                                                pengembalian dana akan dilakukan setelah pembeli
                                                                menghubungi Customer Service dengan menunjukan bukti
                                                                transfer.
                                                            </li>
                                                            <li>Refund / Pengembalian tidak berlaku bagi pelanggan yang
                                                                langsung transfer tanpa mengisi form order atau menerima
                                                                permintaan transfer dari iaccesoris. Karena itu
                                                                pelanggan hanya diperbolehkan transfer jika sudah
                                                                menerima rincian pembayaran.
                                                            </li>
                                                            <li>iaccesoris berhak melakukan pembatalan transaksi secara
                                                                sepihak jika terjadi pelanggaran oleh pengguna.
                                                            </li>
                                                            <li>Pembatalan transaksi yang diakibatkan gangguan pada
                                                                sistem, kenaikan harga, atau stok habis, maka uang
                                                                pemesan akan dikembalikan tanpa potongan apapun (selama
                                                                Menggunakan BANK BCA, BNI, MANDIRI)selambat-lambatnya
                                                                2x24 jam sejak pemberitahuan pembatalan transaksi
                                                                dikirim ke pemesan.
                                                            </li>
                                                            <li>Refund / Pengembalian dana hanya bisa di lakukan pada
                                                                rekening yang dibuat untuk melakukan transfer /
                                                                pembayaran.
                                                            </li>
                                                            <li>Pengajuan refund / pengembalian dana dapat dilakukan
                                                                melalui kontak yang tertera pada iaccesoris.
                                                            </li>
                                                        </ol>
                                                        <h3>E. Pengiriman Produk</h3>
                                                        <ol>
                                                            <li>Produk dikirim setelah pembeli melakukan transfer sesuai
                                                                dengan instruksi sistem dan melakukan konfirmasi
                                                                pembayaran dengan cara klik tombol konfirmasi pada
                                                                halaman transaksi.
                                                            </li>
                                                            <li>Produk lama terkirim diakibatkan kesalahan pembeli
                                                                seperti tidak melakukan konfirmasi pembayaran melalui
                                                                tombol yang sudah disediakan, bukan merupakan tanggung
                                                                jawab Iaccesoris.
                                                            </li>
                                                            <li>Jika pada saat pengecekan mutasi transfer terjadi
                                                                gangguan pada sistem online banking, atau saat internet
                                                                banking offline maka transaksi akan dilakukan setelah
                                                                normal dan pengguna tidak dapat menuntut apapun pada
                                                                kondisi ini.
                                                            </li>
                                                            <li>Kesalahan pembelian / order dalam menuliskan Nomor HP,
                                                                ID PLN, atau informasi lainnya yang mengakibatkan produk
                                                                tidak bisa dikirim menjadi tanggung jawab pembeli /
                                                                pemesan.
                                                            </li>
                                                            <li>Komplain maksimal 3 hari setelah pemesanan produk,
                                                                apabila sudah melakukan pembayaran namun produk belum
                                                                terkirim atau karena sebab kuat lainnya. Komplain lebih
                                                                dari 3 hari setelah order, maka produk dianggap sudah
                                                                terkirim dan sukses.
                                                            </li>
                                                        </ol>
                                                        <h3>F. Deposit</h3>
                                                        <ol>
                                                            <li>Saldo deposit tidak bisa diuangkan kembali, kecuali
                                                                iaccesoris telah berhenti melayani penjualan pulsa dan
                                                                produk lainnya.
                                                            </li>
                                                            <li>Jika ditemukan dalam 1 akun deposit dengan berbeda-beda
                                                                atas nama bank pengirim, maka iaccesoris berhak
                                                                memverifikasi ulang atau melakukan blokir sementara
                                                                untuk di verifikasi.
                                                            </li>
                                                        </ol>
                                                        <h3>G. Garansi</h3>
                                                        <ol>
                                                            <li>Garansi uang kembali apabila iaccesoris tidak dapat
                                                                memproses pemesanan, dikarenakan kesalahan iaccesoris
                                                            </li>
                                                            <li>Garansi yang diberikan Iaccesoris adalah terkait
                                                                keaslian dan pengiriman produk.
                                                            </li>
                                                            <li>Garansi diberikan pada jam kerja yang dicantumkan pada
                                                                website.
                                                            </li>
                                                            <li>Pengajuan komplain yang dilakukan di luar kontak yang
                                                                tercantum di situs ini, bukan tanggung jawab iaccesoris
                                                                dan iaccesoris tidak berkewajiban untuk menjawabnya
                                                            </li>
                                                            <li>Jika situs penyedia / provider menghentikan layanan
                                                                karena masalah internalnya, iaccesoris tidak bertanggung
                                                                jawab dan tidak dapat dituntut memberikan ganti rugi
                                                                dengan alasan apapun.
                                                            </li>
                                                        </ol>
                                                        <h3>H. Pengembalian Dana</h3>
                                                        <p>Pengembalian dana kepada pembeli dilakukan apabila:</p>
                                                        <ol>
                                                            <li>Pengisian produk gagal karena produk mengalami
                                                                gangguan/ditutup.
                                                            </li>
                                                            <li>Pengembalian dana selambat-lambatnya dalam waktu 2 x 24
                                                                jam dari waktu order.
                                                            </li>
                                                            <li>Komplain pembeli maksimal 3 hari setelah melakukan
                                                                order, melebihi batas waktu tidak ada pengembalian dana.
                                                            </li>
                                                            <li>Pembeli tidak dapat melakukan permintaan pengembalian
                                                                dana karena kesalahan sendiri, misalkan salah transfer
                                                                atau sebab kuat lainnya.
                                                            </li>
                                                        </ol>
                                                        <h3>I. Pemblokiran Akun dan Nomor HP</h3>
                                                        <p>Iaccesoris barhak memblokir akun member atau nomor HP pembeli
                                                            jika:</p>
                                                        <ol>
                                                            <li>Terbukti membuat akun lebih dari satu dan melakukan
                                                                aktivitas transaksi yang mencurigakan.
                                                            </li>
                                                            <li>Terbukti menyalahgunakan rekening iaccesoris untuk jasa
                                                                penjualan lain yang dapat merugikan iaccesoris.
                                                            </li>
                                                            <li>Akun member digunakan untuk menyalahgunakan layanan
                                                                iaccesoris seperti menipu, dan lain sebagainya.
                                                            </li>
                                                            <li>Akun member memiliki banyak rekening yang berbeda-beda
                                                                dan melakukan aktivitas transaksi yang mencurigakan.
                                                            </li>
                                                            <li>dan sebab kuat lainnya.</li>
                                                        </ol>
                                                        <h3>J. Informasi Produk</h3>
                                                        <ol>
                                                            <li>iaccesoris hanya menjual produk yang ada dalam list form
                                                                order.
                                                            </li>
                                                            <li>Jika terjadi perubahan layanan, fitur, jumlah kuota dan
                                                                lain-lain, maka iaccesoris tidak bertanggung jawab atas
                                                                perbedaan/perubahan ini, dan tidak dapat dituntut ganti
                                                                rugi dengan alasan apapun.
                                                            </li>
                                                        </ol>
                                                        <h3>K. Hal-Hal yang Tidak Terduga</h3>
                                                        <ol>
                                                            <li>Dalam hal koneksi internet terputus yang mengakibatkan
                                                                server iaccesoris tidak dapat online sehingga transaksi
                                                                tidak dapat dilakukan, maka iaccesoris tidak dapat
                                                                dituntut atas segala kerugian yang ditimbulkan.
                                                            </li>
                                                            <li>iaccesoris tidak bertanggung jawab atas perubahan
                                                                layanan atau penghentian layanan oleh provider /
                                                                penyedia situs dengan alasan apapun.
                                                            </li>
                                                            <li>iaccesoris tidak bertanggung jawab atas terganggunya
                                                                transaksi akibat gangguan pada sistem jaringan internet
                                                                atau gangguan jaringan perbankan atau gangguan jaringan
                                                                telepon seluler atau sistem penyedia situs.
                                                            </li>
                                                        </ol>
                                                        <h3>L. Privasi dan Kerahasiaan</h3>
                                                        <ol>
                                                            <li>iaccesoris menjamin kerahasiaan dan privasi pelanggan.
                                                            </li>
                                                            <li>iaccesoris tidak pernah dan tidak akan pernah membagi
                                                                informasi apapun seperti nomor rekening, email, nomor
                                                                telepon, dan/atau SMS pelanggan kepada pihak manapun.
                                                            </li>
                                                        </ol>
                                                        <h3>M. Perselisihan</h3>
                                                        <ol>
                                                            <li>Perjanjian ini tunduk kepada hukum Republik Indonesia.
                                                            </li>
                                                            <li>Apabila terjadi perselisihan maka akan diselesaikan
                                                                secara musyawarah mufakat.
                                                            </li>
                                                        </ol>
                                                        <h3>N. Tambahan</h3>
                                                        <ol>
                                                            <li>Hal-hal yang belum tercantum dalam Kebijakan ini akan di
                                                                sampaikan kemudian hari.
                                                            </li>
                                                        </ol>
                                                    </div>


                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


<?php
require '../lib/footer.php';
?>