<?php
require("../config.php");

if (isset($_POST['referral'])) {
        // Cek apakah ada permintaan Ajax
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            die();
        }

    $referral = $conn->real_escape_string(trim(filter($_POST['referral'])));
    $cek_kode = $conn->query("SELECT * FROM users WHERE kode_referral = '$referral'");
    $cek_kode_ulang = mysqli_num_rows($cek_kode);
    $data_kode = mysqli_fetch_assoc($cek_kode);

    if($cek_kode->num_rows > 0) {
        echo 0; // Tampilkan pesan
    }else{ // Jika belum ada
        echo 1; // Tampilkan pula pesan
    }
}
?>