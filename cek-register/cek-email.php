<?php
require("../config.php");

if (isset($_POST['email'])) {
        // Cek apakah ada permintaan Ajax
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            die();
        }

    $email = $conn->real_escape_string(trim(filter($_POST['email'])));
    $cek_email = $conn->query("SELECT * FROM users WHERE email = '$email'");
    $cek_email_ulang = mysqli_num_rows($cek_email);
    $data_email = mysqli_fetch_assoc($cek_email);

    if($cek_email->num_rows > 0) {
        echo 0; // Tampilkan pesan
    }else{ // Jika belum ada
        echo 1; // Tampilkan pula pesan
    }
}
?>