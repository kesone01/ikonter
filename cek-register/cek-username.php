<?php
require("../config.php");

if (isset($_POST['username'])) {
        // Cek apakah ada permintaan Ajax
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            die();
        }

    $username = $conn->real_escape_string(trim(filter($_POST['username'])));
    $cek_pengguna = $conn->query("SELECT * FROM users WHERE username = '$username'");
    $cek_pengguna_ulang = mysqli_num_rows($cek_pengguna);
    $data_pengguna = mysqli_fetch_assoc($cek_pengguna);


    if($cek_pengguna->num_rows > 0) {
        echo 0; // Tampilkan pesan
    }else{ // Jika belum ada
        echo 1; // Tampilkan pula pesan
    }
}
?>