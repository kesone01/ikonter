<?php
require("../config.php");

if (isset($_POST['no_hp'])) {
        // Cek apakah ada permintaan Ajax
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            die();
        }

    $no_hp = $conn->real_escape_string(trim(filter($_POST['no_hp'])));
    $cek_no_hp = $conn->query("SELECT * FROM users WHERE no_hp = '$no_hp'");
    $cek_no_hp_ulang = mysqli_num_rows($cek_no_hp);
    $data_no_hp = mysqli_fetch_assoc($cek_no_hp);


    if($cek_no_hp->num_rows > 0) {
        echo 0; // Tampilkan pesan
    }else{ // Jika belum ada
        echo 1; // Tampilkan pula pesan
    }
}
?>