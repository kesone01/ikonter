<?php
require("../config.php");

if (isset($recaptcha['success'])) {
        // Cek apakah ada permintaan Ajax
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            die();
        }
        function dapetin($url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            $data = curl_exec($ch);
            curl_close($ch);
            return json_decode($data, true);
    }
        $secret_key = '6Le5l_cUAAAAAENaQC6ZrSsGuN2doQY36h-vNue5'; //masukkan secret key-nya berdasarkan secret key masing-masing saat create api key nya
        $captcha = $_POST['g-recaptcha-response'];
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) . '&response=' . $captcha;
        $recaptcha = dapetin($url);
        if ($recaptcha['success'] == false) {
            echo 0; // Tampilkan pesan
        }else{ // Jika belum ada
            echo 1; // Tampilkan pula pesan
        }
    }
?>