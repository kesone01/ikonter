<?php
session_start();
require '../config.php';
require '../lib/database.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $data['title']; ?></title>
<meta name="description" content="<?php echo $data['short_title']; ?>" />
    <meta name="keywords" content="<?php echo $data['short_title']; ?>, smm ppob, ppob termurah, game online terlengkap" />
    <meta name="author" content="Konterin" />
	<!-- Site favicon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/faviconn.png" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-white sidebar sidebar-primary accordion toggled" >

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center">
               <div class="sidebar-brand-icon text-gray-200 rounded">
        <img width="100%" src="/image/logo/logo.png">
                </div>
               
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/">
                    <i class="fas fa-home"></i>
                    <span>Home</span></a>
            </li>

            <!-- Divider -->

      <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/status">
                    <i class="fas fa-sync"></i>
                    <span>Status Transaksi</span></a>
            </li>

<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/cara-transaksi">
                    <i class="fas fa-shopping-basket"></i>
                    <span>Cara Transaksi</span></a>
            </li>

          
          						<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/daftar-harga">
                    <i class="fas fa-list"></i>
                    <span>Daftar Harga</span></a>
            </li>
          			
						<hr class="sidebar-divider my-1">

						<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/kontak-kami">
                    <i class="fas fa-envelope"></i>
                    <span>Kontak Kami</span></a>
            </li>
			<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/api">
                    <i class="fas fa-fw fa-code"></i>
                    <span>API Documentation</span></a>
            </li>

          
<div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                   

			   <ul class="navbar-nav ml-auto">
			   <div class="topbar-divider d-none d-sm-block"></div>
			   <li class="nav-item dropdown no-arrow">
                                     <a class="btn btn-dark btn-sm" href="/auth/login">Masuk</a>
          <a class="btn btn-danger btn-sm" href="/auth/register">Daftar</a>
                        </li>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

 
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                JUMLAH PRODUK</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $count_produk; ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-shopping-cart fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                TRANSAKSI SUKSES</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $count_pesanan_pulsa;?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-check-circle fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">MEMBER TERDAFTAR
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"> <?php echo $total_pengguna; ?></div>
                                                </div>
                                                
                                               
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-user fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                               TRANSAKSI PENDING</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $pendingg; ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-sync fa-2x text-gray-300 fa-spin"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Content Row -->

                 <div class="row ">
									
                                        <!-- bootstrap modal start -->
                                        <div class="col-sm-6">
										
                                            <!-- Notification card start -->
               									
                                                                     <div class="card shadow mb-1">

                                         
                 	                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>

                        </ol>
                         <div class="carousel-inner">
                            <div class="carousel-item active">
              <img class="d-block img-fluid" src="/img/slider/img.png"  height="50%" alt="Slide Pertama">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Sebelumnya</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Selanjutnya</span>
                        </a>
                    </div>
                </div>
								</div>
									   
                                        <div class="col-sm-6">
										
                                                                     <div class="card shadow mb-1">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">PEMBERITAHUAN & BERITA</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                           
                                                                  
        <?php
 
// start paging config
if (isset($_GET['cari'])) {
    $cari_oid = $conn->real_escape_string(filter($_GET['cari']));

    $cek_pesanan = "SELECT * FROM berita WHERE target LIKE '%$cari_oid%' ORDER BY id DESC"; // edit
} else {
    $cek_pesanan = "SELECT * FROM berita ORDER BY id DESC"; // edit
}
if (isset($_GET['cari#status	'])) {
$cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
$records_per_page = $cari_urut; // edit
} else {
    $records_per_page = 2; // edit
}

$starting_position = 0;

if(isset($_GET["halaman"])) {
    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
}
$new_query = $cek_pesanan." LIMIT $starting_position, $records_per_page";
$new_query = $conn->query($new_query);
// end paging config

while ($data_berita  = $new_query->fetch_assoc()) {
 if ($data_berita['tipe'] == "INFO") {
                        $label = "info";
			        } else if ($data_berita['tipe'] == "PERINGATAN") {
                        $label = "warning";
			        } else if ($data_berita['tipe'] == "PENTING") {
                        $label = "danger";
			        }

			        if ($data_berita['icon'] == "PESANAN") {
                        $label_icon = "flaticon2-shopping-cart";
			        } else if ($data_berita['icon'] == "LAYANAN") {
                        $label_icon = "flaticon-signs-1";
			        } else if ($data_berita['icon'] == "DEPOSIT") {
                        $label_icon = "flaticon-coins";
			        } else if ($data_berita['icon'] == "PENGGUNA") {
                        $label_icon = "flaticon2-user";
			        } else if ($data_berita['icon'] == "PROMO") {
                        $label_icon = "flaticon2-percentage";
			        }

?>
	
            <ul class="list-group list-group-divider list-group-full">
			       <li class="list-group-item">
        	            <h6><small><span class="badge badge-<?php echo $label; ?> kt-badge--inline"><?php echo $data_berita['tipe']; ?></span> </small><a href="<?php echo $config['web']['url'] ?>page/news-details?id=<?php echo $data_berita['id']; ?>" class="text-xs font-weight-bold text-primary text-uppercase mb-1 "><?php echo $data_berita['title']; ?></a><small>      (<?php echo tanggal_indo($data_berita['date']); ?>)</small></h6>
      	        <span class="text-xs "><?php 
								 			 $teks = $data_berita['konten'];

//pemotongan dengan karakter sejumlah 100
		 echo substr($teks, 0, 75) . '...';
		
								 
								 
								 
								 ?> <a href="<?php echo $config['web']['url'] ?>page/news-details?id=<?php echo $data_berita['id']; ?>" class="text-dark font-weight-bold text-uppercase mb-1">READMORE <i class="fa fa-arrow-right"></i></a></span>  
                                    

	             </li>
			<?php } ?>	 
    </div> 
<div class="row">
                        <div class="col-12">
                                    <a class="btn btn-info btn-sm col-12" href="/pemberitahuan">Lihat Semua..</a>
                                </div>   
								</div>
                                                        </div>
                                                    </div>
													
                                        </div>
		  <!--Start Slide -->

           
                

                    <!-- Content Row -->

                    <div class="row">


                        <div class="col-xl-6 col-lg-7">
                            <div class="card shadow mb-2">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">PRODUK REGULER</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
								<div class="row">
								                   <div class="col-3 mt-2">
                <center>
                  <a href="/order/pulsa" >
                    <img src="/img/produk/smartphone.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ">
                                               PULSA REGULER</div>
                  </a>
                </center>
                 </div>
	
				 
				 
				                             <div class="col-3 mt-2">
                <center>
                  <a href="/order/paket-internet">
                     <img src="/img/produk/wifi.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               PAKET INTERNET</div>
                  </a>
                </center>
                 </div>
                                  
                <div class="col-3 mt-2">
                <center>
                  <a href="/order/voucher-internet">
                   <img src="/img/produk/coupon.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               VOUCHER INTERNET</div>
                  </a>
                </center>
                 </div>
                                  
				 
				 		                             <div class="col-3 mt-2">
                <center>
                  <a href="/order/tlp-sms">
                     <img src="/img/produk/chatting.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               PAKET TELP/SMS</div>
                  </a>
                </center>
                 </div>
				 
				 		                             <div class="col-3 mt-2">
                <center>
                  <a href="/order/token-pln">
                 <img src="/img/produk/wireless.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               TOKEN PLN</div>
                  </a>
                </center>
                 </div>

                                  
				 				 		                             <div class="col-3 mt-2">
                <center>
                  <a href="/order/playstore">
                  <img src="/img/produk/plys.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               GOOGLE PLAY</div>
                  </a>
                </center>
                 </div>
		
				 
				             </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-xl-6 col-lg-7">
                            <div class="card shadow mb-2">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">PRODUK PASCABAYAR</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
								<div class="row">
                                          <div class="col-4 mt-2">
                <center>
                  <a href="/order/pln-pascabayar">
                    <img src="/img/produk/wireless.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                              TAGIHAN PLN</div>
                    
                  </a>
                </center>
                 </div>
	
				 
				 
				                             <div class="col-4 mt-2">
                <center>
                  <a href="/order/internet-pascabayar">
                     <img src="/img/produk/telkom.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               TAGIHAN INTERNET</div>
                  </a>
                </center>
                 </div>
                                  
             <div class="col-4 mt-2">
                <center>
                  <a href="/order/bpjs">
                   <img src="/img/produk/bpjs.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               BPJS</div>
                  </a>
                </center>
                 </div>
                                  
      <div class="col-4 mt-2">
                <center>
                  <a href="/order/hp-pascabayar">
                     <img src="/img/produk/tap.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               HP PASCABAYAR</div>
                  </a>
                </center>
                 </div>
                 
                                                        </div> </div></div></div>
                        <!-- Area Chart -->
                        <div class="col-xl-6 col-lg-7">
                            <div class="card shadow mb-3">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">LAYANAN E-WALLET</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
								<div class="row">
								
                                   <div class="col-3 mt-3">
                <center>
                  <a href="/order/dana">
                   <img src="/img/dana.png" class="rounded" width="70" height="50">
       <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                              SALDO DANA</div>
                  </a>
                </center>
                 </div>
	
				 
				 
				                             <div class="col-3 mt-3">
                <center>
                  <a href="/order/ovo">
                  <img src="/img/ovo.png" class="rounded" width="50" height="50">
           <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               SALDO OVO</div>
                  </a>
                </center>
                 </div>
				 
				 		                             <div class="col-3 mt-3">
                <center>
                  <a href="/order/gopay">
                  <img src="/img/gopay.png" class="rounded" width="80" height="50">
<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               SALDO GO-JEK</div>
                  </a>
                </center>
                 </div>
				 
				 		                             <div class="col-3 mt-3">
                <center>
                  <a href="/order/link-aja">
                            <img src="/img/link-aja.png"  class="rounded" width="50" height="50">
<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                              SALDO LINK-AJA</div>
                  </a>
                </center>
                 </div>
				 
				 				 		                             <div class="col-3 mt-4">
                <center>
                  <a href="/order/shoppepay">
                            <img src="/img/shoppe.png" class="rounded" width="70" height="50">
<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                              SALDO SHOPEE</div>
                  </a>
				  
                </center>
				
                 </div>
				 
				 				 		                             <div class="col-3 mt-4">
                <center>
                  <a href="/order/grab">
                            <img src="/img/grab.svg" class="rounded" width="50" height="60">
<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               SALDO GRAB</div>
                  </a>
                </center>
                 </div>
				 		
				 
				 
				             </div>
                                </div>
                            </div>
                        </div>
						

           
		                      <div class="col-xl-6 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">TOPUP & VOUCHER GAME</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
								<div class="row">
								                                  <div class="col-3 mt-3">
                <center>
                  <a href="/order/pointblank">
                            <img src="/img/PointBlank_ID_tile.jpg" width="50px" height="50px" class="rounded">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                           POINT BLANK</div>
                  </a>
                </center>
                 </div>
                                   <div class="col-3 mt-3">
                <center>
                  <a href="/order/mobile-legends">
                            <img src="/img/mobile-legend1.png" width="50px" height="50px">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                              MOBILE LEGENDS</div>
                  </a>
                </center>
                 </div>
	
				 
				 
				                             <div class="col-3 mt-3">
                <center>
                  <a href="/order/call-of-dutty">
                                       <img src="/img/cod.png" width="50px" height="50px">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               COD MOBILE</div>
                  </a>
                </center>
                 </div>
				 
				 		                             <div class="col-3 mt-3">
                <center>
                  <a href="/order/pubg">
                              <img src="/img/pubg.png" width="50px" height="50px">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               PUBG MOBILE</div>
                  </a>
                </center>
                 </div>
				 
				 		                             <div class="col-3 mt-4">
                <center>
                  <a href="/order/valor">
                        <img src="/img/valor.png" width="50px" height="50px">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               ARENA OF VALOR</div>
                  </a>
                </center>
                 </div>
				 		
		       <div class="col-3 mt-4">
                <center>
                  <a href="/order/hdi">
                        <img src="/img/hdi.png" width="50px" height="50px"  class="rounded">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               HIGGS DOMINO</div>
                  </a>
                </center>
                 </div>
                                         <div class="col-3 mt-4">
                <center>
                  <a href="/order/free-fire">
                        <img src="/img/freefire_tile.jpg" width="50px" height="50px"  class="rounded">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               FREE FIRE</div>
                  </a>
                </center>
                 </div>
                                  
                                                                 <div class="col-3 mt-4">
                <center>
                  <a href="/order/steamidr">
                        <img src="/img/steam_tile.jpg" width="50px" height="50px"  class="rounded">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                               STEAM WALLET IDR</div>
                  </a>
                </center>
                 </div>
                                  
                                  
				             </div>
                                </div>
                            </div>
                        </div>
		   
		   
		  

            </div>

            </div>
            <!-- End of Main Content -->
        <?php 
require '../lib/footer.php';
?>