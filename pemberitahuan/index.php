<?php
session_start();
require("../config.php");
require("../lib/database.php");
if (!isset($_SESSION['user'])) {    
exit(header("Location: ".$config['web']['url']."main/"));
} else {     
require("../lib/header.php");  
$sess_username = $_SESSION['user']['username'];
?> 
  <!-- Begin Page Content -->
                <div class="container-fluid">

 
                  
                        

                    <!-- Content Row -->

                    <div class="row">


                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">PEMBERITAHUAN & BERITA</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
									 <?php
			        $cek_berita = $conn->query("SELECT * FROM berita ORDER BY id DESC");
			        while ($data_berita = $cek_berita->fetch_assoc()) {
			        if ($data_berita['tipe'] == "INFO") {
                        $label = "info";
			        } else if ($data_berita['tipe'] == "PERINGATAN") {
                        $label = "warning";
			        } else if ($data_berita['tipe'] == "PENTING") {
                        $label = "danger";
			        }

			        if ($data_berita['icon'] == "PESANAN") {
                        $label_icon = "flaticon2-shopping-cart";
			        } else if ($data_berita['icon'] == "LAYANAN") {
                        $label_icon = "flaticon-signs-1";
			        } else if ($data_berita['icon'] == "DEPOSIT") {
                        $label_icon = "flaticon-coins";
			        } else if ($data_berita['icon'] == "PENGGUNA") {
                        $label_icon = "flaticon2-user";
			        } else if ($data_berita['icon'] == "PROMO") {
                        $label_icon = "flaticon2-percentage";
			        }
			        ?>
<h6 class="m-0 font-weight-bold text-primary">
<small>
<span class="badge badge-<?php echo $label; ?> kt-badge--inline"><?php echo $data_berita['tipe']; ?>
</span></small>
<b style="text-transform: uppercase;">
<?php echo $data_berita['title']; ?>
                                               
</b>

												<small> 
                                                <span class="kt-notes__desc">
                                                    (<?php echo tanggal_indo($data_berita['date']); ?>)
                                                </span></small></h6>

		   <p>
		   <?php 
								 			 $teks = $data_berita['konten'];

//pemotongan dengan karakter sejumlah 100
		 echo substr($teks, 0, 90) . '...';
		
								 
					
								 
								 ?> 		
								 <a href="<?php echo $config['web']['url'] ?>page/news-details?id=<?php echo $data_berita['id']; ?>" class="text-dark font-weight-bold text-uppercase mb-1">READMORE <i class="fa fa-arrow-right"></i></a> 
								 	</p> 
								 <hr>
								 
<?php 
					}?>		  

            </div>

            </div>


        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->




<?php 
}
require '../lib/footer.php';
?>