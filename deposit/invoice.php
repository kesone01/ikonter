<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/header.php';

if (isset($_GET['kode_deposit'])) {
    $post_kode = abs($_GET['kode_deposit']);
    $cek_deposit = $conn->query("SELECT * FROM deposit WHERE kode_deposit = '$post_kode' AND username = '$sess_username'");
    $data_deposit = mysqli_fetch_assoc($cek_deposit);
    $provider = mysqli_fetch_assoc($cek_deposit);


    if ($data_deposit['status'] == "Pending") {
        $label = "warning";
    } else if ($data_deposit['status'] == "Error") {
        $label = "danger";
    } else if ($data_deposit['status'] == "Verifikasi Pembayaran") {
        $label = "primary";
    } else if ($data_deposit['status'] == "Success") {
        $label = "success";
    }

    if ($cek_deposit->num_rows == 0) {

        header("Location: " . $config['web']['url'] . "deposit");

    } else {


        if (isset($_POST['njay'])) {
            $post_kode = abs($_GET['kode_deposit']);
            $conn->query("UPDATE deposit set status = 'Error' where kode_deposit = '$post_kode' AND username = '$sess_username'");
            echo '<script>
						var url = "' . $config['web']['url'] . 'deposit"; // URL Tujuan
						var count = 1; // dalam detik
						function countDown() {
							if (count > 0) {
							    count--;
							    var waktu = count + 1;
							    setTimeout("countDown()", 1000);
							} else {
							    window.location.href = url;
							}
						}
						countDown();
					</script>';
        }

        ?>


        <div class="container-fluid">
            <!-- Content Row -->
            <div class="row">
                <div class="col-xl-12 col-lg-7">
                    <div class="card shadow mb-4">

                        <div
                                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">INVOICE PEMBAYARAN
                                #<?php echo $data_deposit['kode_deposit']; ?></h6>

                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <div class="invoice-wrap">
                                <div class="invoice-box">
                                    <div class="invoice-header">
                                        <div class="logo text-center">
                                            <img src="/image/logo/logo.png" width="35%" alt="">
                                        </div>
                                    </div>
                                    <h5 class="text-center mb-30 weight-500">
                                        <strong><font
                                                    face="BatangChe"><?php echo tanggal_indo($data_deposit['date']); ?>
                                                , <?php echo $data_deposit['time']; ?></font></strong></h5>


                                </div>


                                <div class="kt-invoice__actions">
                                    <div class="kt-invoice__container">
                                        <button type="button" class="btn btn-info btn-sm" style="float:left;"
                                                onclick="window.print();"><i class="fa fa-print fa-lg"></i> Print
                                            Invoice
                                        </button>

                                        <form class="form-horizontal" role="form" method="POST" action="">
                                            <?php if ($data_deposit['status'] !== "Pending") { ?>
                                                <a href="<?php echo $config['web']['url']; ?>deposit/riwayat"
                                                   class="btn btn-danger btn-sm" style="float:right;">Kembali</a>
                                            <?php } ?>


                                            <?php if ($data_deposit['status'] !== "Success" and $data_deposit['status'] !== "Error" and $data_deposit['status'] !== "Verifikasi Pembayaran") { ?>
                                            <input type="hidden" name="csrf_token"
                                                   value="<?php echo $config['csrf_token'] ?>">
                                            <button type="submit" class="btn btn-danger btn-sm" style="float:right;"
                                                    name="njay" value="<?php echo $data_deposit['kode_deposit']; ?>"><i
                                                        class="fa fa-close fa-lg"></i> Batalkan
                                            </button>

                                    </div>
                                </div>
                                <?php } ?>
                                <?php
                                if (isset($_SESSION['hasil'])) {
                                    ?>
                                    <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible"
                                         role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <b class="text-left"><i
                                                    class="fa fa-spinner fa-lg fa-spin"></i> <?php echo $_SESSION['hasil']['pesan'] ?>
                                        </b>
                                    </div>
                                    <?php
                                    unset($_SESSION['hasil']);
                                }
                                ?>
                                <br>

                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="kt_table_1">
                                        <tr>
                                            <td colspan="2" class="text-center">
                                                <strong><font face="BatangChe"><h4><font face="Algerian">DETAIL
                                                                PEMBAYARAN</font></h4></font></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>TIPE TRANSFER</td>
                                            <td><?php echo $data_deposit['provider']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>PENERIMA</td>
                                            <td><?php echo $data_deposit['penerima']; ?></td>
                                        </tr>
                                        <?php if ($data_deposit['provider'] == "QRIS") { ?>
                                            <tr>
                                                <td>SCAN QR<br><small>(SILAHKAN SCAN <b>QR KODE</b> INI<BR> SUPPORT <b>SEMUA
                                                            BANK</b> BCA,BRI,BNI,MANDIRI DLL.<BR>
                                                        <b>SEMUA E-WALLET</b> DANA-OVO-GOPAY-LINKAJA-SHOPEPAY-DLL)</td>
                                                <td><img src="<?php echo $data_deposit['pengirim']; ?>" height="300"
                                                         width="300"><br></td>
                                            </tr>
                                        <?php } ?>


                                        <tr>
                                            <td>SALDO YANG DIDAPATKAN</td>
                                            <td>
                                                <b>Rp <?php echo number_format($data_deposit['get_saldo'], 0, ',', '.'); ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>JUMLAH PEMBAYARAN</td>
                                            <td class="kt-font-danger kt-font-xl kt-font-boldest">
                                                Rp <?php echo number_format($data_deposit['jumlah_transfer'], 0, ',', '.'); ?></td>
                                        </tr>

                                        <tr>
                                            <td>STATUS DEPOSIT</td>
                                            <td>
                                                <span class="badge badge-<?php echo $label; ?>"><?php if ($data_deposit['status'] !== "Success" and $data_deposit['status'] !== "Error") { ?>
                                                        <i class="fas fa-sync fa-lg fa-spin text-dark"></i><?php } ?> <?php echo $data_deposit['status']; ?></span>

                                            </td>
                                        <tr>

                                            <?php if ($data_deposit['status'] !== "Success" and $data_deposit['status'] !== "Error") { ?>
                                            <td colspan="2">
                                                <span class="badge badge-danger">HARAP DIBACA!</span><small> Jika sudah
                                                    transfer tapi masih Status <span
                                                            class="badge badge-<?php echo $label; ?>"><?php if ($data_deposit['status'] !== "Success" and $data_deposit['status'] !== "Error") { ?><?php } ?><?php echo $data_deposit['status']; ?></span>
                                                    melebihi <b> 15 menit </b> silahkan hubungi kontak <a
                                                            href="<?php echo $config['web']['url']; ?>help/">Bantuan</a>
                                                </small>
                                                <?php } ?>
                                            </td>
                                        </tr>

                                        </tr>
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        </div>

        </div>
        <!-- End Content -->

        <!-- Start Scrolltop -->

        <!-- End Scrolltop -->

        <?php ?>

        <?php
        require '../lib/footer.php';
    }
} else {
    header("Location: " . $config['web']['url'] . "deposit/history");
}
?>