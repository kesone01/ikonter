<?php
session_start();
require("../config.php");
require '../lib/session_user.php';

if (isset($_POST['buat'])) {
    require '../lib/session_login.php';
    $post_metode = $conn->real_escape_string($_POST['radio7']);
    $post_tipe = $conn->real_escape_string($_POST['radio6']);
    $post_provider = $conn->real_escape_string($_POST['provider']);
    $post_pengirim = $conn->real_escape_string(trim(filter($_POST['pengirim'])));
    $post_jumlah = $conn->real_escape_string(trim(filter($_POST['jumlah'])));
    $post_jumlah1 = $_POST['jumlah'];
    $post_pin = $conn->real_escape_string(trim(filter($_POST['pin'])));

    $cek_metod = $conn->query("SELECT * FROM metode_depo WHERE id = '$post_provider' AND status = 'Aktif' ORDER BY id ASC");
    $data_metod = $cek_metod->fetch_assoc();
    $cek_metod_rows = mysqli_num_rows($cek_metod);

    $cek_depo = $conn->query("SELECT * FROM deposit WHERE username = '$sess_username' AND status = 'Pending'");
    $data_depo = $cek_depo->fetch_assoc();
    $count_depo = mysqli_num_rows($cek_depo);
    $deposit_id = $conn->query("SELECT * FROM deposit ORDER by id DESC limit 1")->fetch_assoc();

    if ($post_metode == "saldo_sosmed") {
        $post_metodee = "Saldo Sosmed";
    } else if ($post_metode == "saldo_top_up") {
        $post_metodee = "Saldo Top Up";
    }

    $tipe_saldo = $post_metode;
    $kode = acak_nomor(3) . acak_nomor(3);

    $error = array();
    if (empty($post_metode)) {
        $error ['radio7'] = '*Wajib Pilih Salah Satu.';
    }
    if (empty($post_tipe)) {
        $error ['radio6'] = '*Wajib Pilih Salah Satu.';
    }
    if (empty($post_provider)) {
        $error ['provider'] = '*Wajib Pilih Salah Satu.';
    }
    if (empty($post_pengirim)) {
        $error ['pengirim'] = '*Tidak Boleh Kosong.';
    }
    if (empty($post_jumlah)) {
        $error ['jumlah'] = '*Tidak Boleh Kosong.';
    }
    if (empty($post_pin)) {
        $error ['pin'] = '*Tidak Boleh Kosong.';
    } else if ($post_pin <> $data_user['pin']) {
        $error ['pin'] = '*PIN Yang Kamu Masukkan Salah.';
    } else {

        if ($cek_metod_rows == 0) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Tipe Pembayaran Tidak Tersedia.<script>swal("Ups Gagal!", "Tipe Pembayaran Tidak Tersedia.", "error");</script>');
        } else if ($count_depo >= 1) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Kamu Masih Memiliki Deposit Yang Berstatus Pending.<script>swal("Ups Gagal!", "Kamu Masih Memiliki Deposit Berstatus Pending.", "error");</script>');
        } else if ($post_jumlah < $data_metod['minimal']) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Minimal Deposit Adalah Rp ' . $data_metod['minimal'] . '.<script>swal("Ups Gagal!", "Minimal Deposit Adalah Rp ' . $data_metod['minimal'] . '.", "error");</script>');
        } else {
            if ($data_metod['tipe'] == 'QRIS') {
                $depoid = $deposit_id['id'] + 1;
                $get_saldo = $post_jumlah * $data_metod['rate'];
                $merchant_reff = 'KONTERIN' . $depoid;
                $saldofee = $post_jumlah + 300;
                $post_api = array(
                    'key' => 'db2e5811f94b40d09aa4de101ffeba4656d8222316300d259559500082545192',
                    'action' => 'create',
                    'buyer_name' => 'KONTERIN',
                    'merchant_reff' => $merchant_reff,
                    'type' => 'dynamic',
                    'report' => 'https://konter.in/qriscallback.php',
                    'quantity' => $saldofee
                );

                $deposit = json_decode(connect('https://atlantic-pedia.co.id/api/qris', $post_api), true);
                if ($deposit['result'] == true) {
                    $qr = $deposit['data']['qr_image'] . "&type=qr";
                    $insert = $conn->query("INSERT INTO deposit VALUES ('', '$kode', '$sess_username', '" . $data_metod['tipe'] . "', '" . $data_metod['provider'] . "', '$qr', '" . $data_metod['tujuan'] . " " . $data_metod['nama_penerima'] . "', '" . $data_metod['catatan'] . "','$saldofee', '$get_saldo', '$tipe_saldo', 'Pending', '$date', '$time')");


                    if ($insert == TRUE) {
                        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Berhasil ' . $post_metodee . '.<script>swal("Berhasil!", "Kamu Berhasil Buat Deposit.", "success");</script>');

                    } else {
                        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
                    }
                } else {
                    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
                }

            } else {
                if ($data_metod['tipe'] == 'Transfer Bank') {

                    $post_jumlah = $post_jumlah + rand(000, 999);
                }

                $get_saldo = $post_jumlah * $data_metod['rate'];
                $saldo = number_format($get_saldo, 0, ',', '.');
                $tanggal = date('Y-m-d'); // Tanggal Request
                $kodec = 'ulindzgn-4zKv55K5DNBvBOi'; // Kode Akses
                $jumlah = $get_saldo; // Jumlah
                $bank = $data_metod['provider']; // BRI/BNI/BCA/GOPAY/OVO

                $url = "https://cekduit.my.id/api/input.php";

                $curlHandle = curl_init();
                curl_setopt($curlHandle, CURLOPT_URL, $url);
                curl_setopt($curlHandle, CURLOPT_POSTFIELDS, "kode=" . $kodec . "&jumlah=" . $jumlah . "&bank=" . $bank . "&tanggal=" . $tanggal);
                curl_setopt($curlHandle, CURLOPT_HEADER, 0);
                curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
                curl_setopt($curlHandle, CURLOPT_POST, 1);
                $result = curl_exec($curlHandle);
                curl_close($curlHandle);

                $b = json_decode($result, true);

                $insert = $conn->query("INSERT INTO deposit VALUES ('', '$kode', '$sess_username', '" . $data_metod['tipe'] . "', '" . $data_metod['provider'] . "', '$post_pengirim', '" . $data_metod['tujuan'] . " " . $data_metod['nama_penerima'] . "', '" . $data_metod['catatan'] . "','$post_jumlah', '$get_saldo', '$tipe_saldo', 'Pending', '$date', '$time')");
                if ($insert == TRUE) {
                    $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Berhasil ' . $post_metodee . '.<script>swal("Berhasil!", "Kamu Berhasil Buat Deposit.", "success");</script>');
                } else {
                    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
                }
            }


        }
    }
}

require("../lib/header.php");

?>
    <div class="container-fluid">
        <!-- Content Row -->
        <div class="row">
            <div class="col-xl-8 col-lg-7">
                <div class="card shadow mb-4">

                    <div
                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">DEPOSIT</h6>
                        <a href="riwayat" class="btn btn-primary btn-sm" style="float:right;"><i
                                    class="fas fa-list"></i> RIWAYAT DEPOSIT</a>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <?php
                        $cek_depo = $conn->query("SELECT * FROM deposit WHERE username = '$sess_username' AND status = 'Pending' ORDER BY id DESC");
                        while ($data_depo = $cek_depo->fetch_assoc()) {

                            ?>
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                            <script>
                                var url = "<?php echo $config['web']['url'] ?>deposit/invoice?kode_deposit=<?php echo $data_depo['kode_deposit']; ?>"; // URL Tujuan
                                var count = 1; // dalam detik
                                function countDown() {
                                    if (count > 0) {
                                        count--;
                                        var waktu = count + 1;
                                        setTimeout("countDown()", 1000);
                                    } else {
                                        window.location.href = url;
                                    }
                                }

                                countDown();
                            </script>
                            <?php
                        }
                        ?>
                        <?php
                        if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                                <div id="respon"></div>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                        }
                        ?>

                        <form class="form-horizontal" role="form" method="POST">

                            <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">

                            <input type="radio" name="radio7" id="metod" value="saldo_top_up" checked hidden>

                            <span class="form-text text-muted"><?php echo ($error['radio7']) ? $error['radio7'] : ''; ?></span>

                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Kategori Pembayaran</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="kt-radio-list">
                                        <select class="form-control" name="radio6" id="tipe">
                                            <option value
                                            "0">-PILIH PEMBAYARAN-</option>
                                            <option value="Transfer Bank">TRANSFER BANK</option>
                                            <option value="QRIS">QRIS (SEMUA BANK DAN SEMUA E-WALLET)</option>
                                        </select>


                                        <span class="form-text text-muted"><?php echo ($error['radio6']) ? $error['radio6'] : ''; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Tipe Pembayaran</label>
                                <div class="col-lg-9 col-xl-6">
                                    <select class="form-control" name="provider" id="provider">
                                        <option value="0">-PILIH KATEGORI PEMBAYARAN DULU-</option>
                                    </select>
                                    <span class="form-text text-muted"><?php echo ($error['provider']) ? $error['provider'] : ''; ?></span>
                                </div>
                            </div>
                            <div id="pembayaran"></div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">PIN</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="fas fa-key text-primary"></i></span></div>
                                        <input type="password" name="pin" class="form-control"
                                               placeholder="Masukkan PIN Kamu">
                                    </div>
                                    <span class="form-text text-muted"><b
                                                style="color:red"><?php echo ($error['pin']) ? $error['pin'] : ''; ?></b></span>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-3 col-xl-3">
                                        </div>
                                        <div class="col-lg-9 col-xl-9">
                                            <button type="submit" name="buat"
                                                    class="btn btn-primary btn-elevate btn-pill btn-elevate-air"><i
                                                        class="fas fa-plus-circle"></i> Deposit
                                            </button>
                                            <button type="reset"
                                                    class="btn btn-danger btn-elevate btn-pill btn-elevate-air"><i
                                                        class="fas fa-sync"></i> Reset
                                            </button>
                                        </div>
                                    </div>
                        </form>


                    </div>
                    <?php
                    function IPnya()
                    {
                        $ipaddress = '';
                        if (getenv('HTTP_CLIENT_IP'))
                            $ipaddress = getenv('HTTP_CLIENT_IP');
                        else if (getenv('HTTP_X_FORWARDED_FOR'))
                            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
                        else if (getenv('HTTP_X_FORWARDED'))
                            $ipaddress = getenv('HTTP_X_FORWARDED');
                        else if (getenv('HTTP_FORWARDED_FOR'))
                            $ipaddress = getenv('HTTP_FORWARDED_FOR');
                        else if (getenv('HTTP_FORWARDED'))
                            $ipaddress = getenv('HTTP_FORWARDED');
                        else if (getenv('REMOTE_ADDR'))
                            $ipaddress = getenv('REMOTE_ADDR');
                        else
                            $ipaddress = 'IP Tidak Dikenali';

                        return $ipaddress;
                    }

                    $ipaddress = $_SERVER['REMOTE_ADDR'];


                    ?>

                </div>

            </div>


            <div class="alert alert-warning checkout-security-msg rounded">
                <i class="fa fa-lock fa-lg fa-3x"></i>
                <p style="padding-left:50px;margin-top:-43px;"><small>Halaman ini sudah aman dan terlindungi. Alamat IP
                        untuk login Anda saat ini adalah (<strong><?php echo IPnya(); ?></strong>) Sedang login.
                        <br> <?php echo $_SERVER['HTTP_USER_AGENT']; ?></small></p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>


    <div class="col-xl-4 col-lg-8">
        <div class="card shadow mb-4">

            <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">INFORMASI</h6>

            </div>
            <!-- Card Body -->
            <div class="card-body">
                <p>Deposit Pada <b><?php echo $data['short_title']; ?></b> Menggunakan Sistem Verifikasi Otomatis, Saldo
                    Akan Bertambah Pada Menit Yang Sama Ketika Kamu <b>Sukses</b> Melakukan Deposit.</p>
                <p>Jika Terjadi Kendala Dalam Melakukan <b>Deposit</b> Kamu Bisa ke Menu <b><a href="/help">Open
                            Ticket/Bantuan</a></b> Untuk Melaporkan Tentang Masalah Kamu.</p>

            </div>
        </div>
    </div>
    </div>
    <!-- End Page Top Up Balance -->

    </div>
    <!-- End Content -->

    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[id=tipe]').change(function () {
                var tipe = this.value;
                $.ajax({
                    url: '<?php echo $config['web']['url']; ?>ajax/provider-top-up-balance.php',
                    data: 'tipe=' + tipe,
                    type: 'POST',
                    dataType: 'html',
                    success: function (msg) {
                        $("#provider").html(msg);
                    }
                });
            });
            $("#provider").change(function () {
                var provider = $("#provider").val();
                $.ajax({
                    url: '<?php echo $config['web']['url']; ?>ajax/payment-top-up-balance.php',
                    data: 'provider=' + provider,
                    type: 'POST',
                    dataType: 'html',
                    success: function (msg) {
                        $("#pembayaran").html(msg);
                    }
                });
            });
            $("#jumlah").change(function () {
                var pembayaran = $("#pembayaran").val();
                var jumlah = $("#jumlah").val();
                $.ajax({
                    url: '<?php echo $config['web']['url']; ?>ajax/rate-top-up-balance.php',
                    type: 'POST',
                    dataType: 'html',
                    data: 'pembayaran=' + pembayaran + '&jumlah=' + jumlah,
                    success: function (result) {
                        $("#rate").val(result);
                    }
                });
            });
        });
    </script>

    <br/>
    <br/>

<?php
require("../lib/footer.php");
?>