<?php

require '../config.php';
require '../lib/database.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $data['title']; ?></title>
<meta name="description" content="<?php echo $data['short_title']; ?>" />
    <meta name="keywords" content="<?php echo $data['short_title']; ?>, smm ppob, ppob termurah, game online terlengkap" />
    <meta name="author" content="Konterin" />
	<!-- Site favicon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/faviconn.png" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-white sidebar sidebar-primary accordion toggled" >

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center">
                <div class="sidebar-brand-icon text-gray-200 rounded">
        <img width="100%" src="/image/logo/logo.png">
                </div>
               
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/">
                    <i class="fas fa-home"></i>
                    <span>Home</span></a>
            </li>

            <!-- Divider -->

      <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/status">
                    <i class="fas fa-sync"></i>
                    <span>Status Transaksi</span></a>
            </li>

<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/cara-transaksi">
                    <i class="fas fa-shopping-basket"></i>
                    <span>Cara Transaksi</span></a>
            </li>
			
			
			<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/daftar-harga">
                    <i class="fas fa-list"></i>
                    <span>Daftar Harga</span></a>
            </li>
											<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/kontak-kami">
                    <i class="fas fa-envelope"></i>
                    <span>Kontak Kami</span></a>
            </li>
			<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/api">
                    <i class="fas fa-fw fa-code"></i>
                    <span>API Documentation</span></a>
            </li>

          
<div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

               <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search"  method="GET" action="st/">

                                    <div class="input-group">
                                        <input type="number" name="cari" class="form-control bg-light border-0 small" placeholder="Cari Nomor HP ..." aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>


                    </ul>

                </nav>

                <!-- Begin Page Content -->
                <div class="container-fluid">

 
                    <!-- Content Row -->
                    <div class="row">
					  <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-sync fa-spin"></i> STATUS TRANSAKSI</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">

	<div id="stat"></div>

                      <div class="row">
            <div class="col-sm-4">
          <p class="font-15 max-width-900"> 
                <b>
               <i class="fa fa-info-circle"></i>   Keterangan Status 
                </b>
              </p>
              <table class="table" style="margin-bottom: 15px;">
                <tbody>
                  <tr>
                    <td style="width: 40px;">
<span class="badge badge-success">Success</span>
                    </td>
                    <td>
                      Pengisian Berhasil
                    </td>
                  </tr>
                  <tr>
                    <td>
              
                        <span class="badge badge-warning">Pending</span>

                    </td>
                    <td>
                      Sedang kami proses.
                    </td>
                  </tr>
                  <tr>
                   
                  <tr>
                    <td>
                      <span class="badge" style="width: 34px;">
                        <span class="badge badge-danger">Error</span>
                      </span>
                    </td>
                    <td>
                      Refund, Nomer habis masa aktif atau Nomer salah.
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-sm-8">
              <p>
                <b>
                   <i class="fa fa-info-circle"></i>  Pulsa Telpon, Token, atau Voucher Belum Masuk?
                </b>
              </p>
              <ul>
                <li>
                  Terlebih dahulu cek pulsa Anda. Kadang pulsa sudah masuk mendahului report / notifikasi melalui SMS.
                </li>
                <li>
                  Khusus Token PLN, Token akan otomatis kami kirimkan ke HP anda atau bisa langsung anda cetak di halaman informasi order.
                </li>
              </ul>
             
              </ul>
            </div>
          </div>
			</div>
			
		</div>
	</div>

<?php 
require '../lib/footer.php';
?>
