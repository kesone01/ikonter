
<?php

require '../../config.php';
require '../../lib/database.php';
?>
    <title><?php echo $data['title']; ?></title>
 <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<?php
                    if (isset($_SESSION['hasil'])) {
                    ?>
                    <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $_SESSION['hasil']['pesan'] ?>
                    </div>
                    <?php
                    unset($_SESSION['hasil']);
                    }
                    ?>
					
					
                           <?php if($data_deposit['status'] !== "Error" ) { ?>
                    <div class="table-responsive">
                        <table class="table table-striped hidden-phone" >
                            <thead>
                                <tr >
                                    <th>TGL/WAKTU</th>
                                    <th>PRODUK</th>
                                    <th>TUJUAN</th>
                                    <th>HARGA</th>
                                    <th>STATUS</th>
                                </tr>
                            </thead>
                            <tbody>
<?php 
// start paging config
if (isset($_GET['cari'])) {
    $cari_oid = $conn->real_escape_string(filter($_GET['cari']));

    $cek_pesanan = "SELECT * FROM pembelian_pulsa WHERE target LIKE '%$cari_oid%' ORDER BY id DESC"; // edit
} else {
    $cek_pesanan = "SELECT * FROM pembelian_pulsa ORDER BY id DESC"; // edit
}
if (isset($_GET['cari#status	'])) {
$cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
$records_per_page = $cari_urut; // edit
} else {
    $records_per_page = 10; // edit
}

$starting_position = 0;

if(isset($_GET["halaman"])) {
    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
}
$new_query = $cek_pesanan." LIMIT $starting_position, $records_per_page";
$new_query = $conn->query($new_query);
// end paging config

while ($data_pesanan = $new_query->fetch_assoc()) {
    if ($data_pesanan['status'] == "Pending") {
        $label = "warning";
    } else if ($data_pesanan['status'] == "Partial") {
        $label = "danger";
    } else if ($data_pesanan['status'] == "Error") {
        $label = "danger";    
    } else if ($data_pesanan['status'] == "Processing") {
        $label = "primary";    
    } else if ($data_pesanan['status'] == "Success") {
        $label = "success";    
    }
    if ($data_pesanan['refund'] == "0") {
        $icon2 = "times-circle";
        $label2 = "danger"; 
    } else if ($data_pesanan['refund'] == "1") {
        $icon2 = "check";
        $label2 = "success";
    }
	
?>
                                <tr>
                                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>?order_id=<?php echo $data_pesanan['oid']; ?>" class="form-inline" role="form" method="POST"> 
                                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                                    <td><?php echo tanggal_indo($data_pesanan['date']); ?>, <?php echo $data_pesanan['time']; ?></td>

                                    <td><?php echo $data_pesanan['layanan']; ?></td>
                                    <td style="min-width: 100px;">
                                       
                                        <?php 
$noTlp = $data_pesanan['target'];
echo substr($noTlp, 0, -3) . 'XXX';
 
                                      ?>
                                     </td>
                        
                                    <td width="15%">Rp <?php echo number_format($data_pesanan['harga'],0,',','.'); ?></td>
       
                             
							<td><span class="badge badge-<?php echo $label; ?>"><?php echo $data_pesanan['status']; ?></span></td>                                       
                                               <?php } ?>
                                    </form>
                                </tr>
<?php } ?>
                            </tbody>
                        </table>
						
				