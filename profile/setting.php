<?php
session_start();
require '../config.php';
require '../lib/session_login.php';
require '../lib/session_user.php';

if (isset($_POST['ganti_profil'])) {
    $nama_depan = $conn->real_escape_string(trim(filter($_POST['nama_depan'])));
    $nama_belakang = $conn->real_escape_string(trim(filter($_POST['nama_belakang'])));
    $email = $conn->real_escape_string(trim(filter($_POST['email'])));
    $no_hp = $conn->real_escape_string(trim(filter($_POST['no_hp'])));
    $pin = $conn->real_escape_string(trim(filter($_POST['pin1'])));

    $cek_email = $conn->query("SELECT * FROM users WHERE email = '$email'");
    $cek_email_ulang = mysqli_num_rows($cek_email);
    $data_email = mysqli_fetch_assoc($cek_email);

    $cek_no_hp = $conn->query("SELECT * FROM users WHERE no_hp = '$no_hp'");
    $cek_no_hp_ulang = mysqli_num_rows($cek_no_hp);
    $data_no_hp = mysqli_fetch_assoc($cek_no_hp);

    $error = array();
    if (empty($nama_depan)) {
        $error ['nama_depan'] = '*Tidak Boleh Kosong.';
    }
    if (empty($nama_belakang)) {
        $error ['nama_belakang'] = '*Tidak Boleh Kosong.';
    }
    if (empty($email)) {
        $error ['email'] = '*Tidak Boleh Kosong.';
    } else if ($cek_email_ulang == 0) {
        $error ['email'] = '*Email Sudah Terdaftar.';
    }
    if (empty($no_hp)) {
        $error ['no_hp'] = '*Tidak Boleh Kosong.';
    } else if ($cek_no_hp_ulang == 0) {
        $error ['no_hp'] = '*Nomor HP Sudah Terdaftar.';
    }
    if (empty($pin)) {
        $error ['pin1'] = '*Tidak Boleh Kosong.';
    } else if ($pin <> $data_user['pin']) {
        $error ['pin1'] = '*PIN Yang Kamu Masukkan Salah.';
    } else {

        if ($conn->query("UPDATE users SET nama_depan = '$nama_depan', nama_belakang = '$nama_belakang', email = '$email', no_hp = '$no_hp' WHERE username = '$sess_username'") == true) {
            $_SESSION['hasil'] = array('alert' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Yeah, Data Profil Kamu Berhasil Diubah.<script>swal("Berhasil!", "Data Profil Kamu Berhasil Diubah.", "success");</script>');
        } else {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Gagal', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
        }

    }

} else if (isset($_POST['ganti_password'])) {
    $password = $conn->real_escape_string(trim(filter($_POST['password_lama'])));
    $password_baru = $conn->real_escape_string(trim(filter($_POST['password_baru'])));
    $konf_pass_baru = $conn->real_escape_string(trim(filter($_POST['konf_pass_baru'])));

    $cek_passwordnya = password_verify($password, $data_user['password']);
    $hash_passwordnya = password_hash($password_baru, PASSWORD_DEFAULT);

    $error = array();
    if (empty($password)) {
        $error ['password_lama'] = '*Tidak Boleh Kosong.';
    }
    if (empty($password_baru)) {
        $error ['password_baru'] = '*Tidak Boleh Kosong.';
    } else if (strlen($password_baru) < 6) {
        $error ['password_baru'] = '*Kata Sandi Minimal 6 Karakter.';
    }
    if (empty($konf_pass_baru)) {
        $error ['konf_pass_baru'] = '*Tidak Boleh Kosong.';
    } else if (strlen($konf_pass_baru) < 6) {
        $error ['konf_pass_baru'] = '*Kata Sandi Minimal 6 Karakter.';
    } else if ($password_baru <> $konf_pass_baru) {
        $error ['konf_pass_baru'] = '*Konfirmasi Kata Sandi Baru Tidak Sesuai.';
    } else {

        if ($cek_passwordnya <> $data_user['password']) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Kata Sandi Lama Yang Kamu Masukkan Tidak Sesuai.<script>swal("Gagal!", "Kata Sandi Lama Yang Kamu Masukkan Tidak Sesuai.", "error");</script>');
        } else {

            if ($conn->query("UPDATE users SET password = '$hash_passwordnya' WHERE username = '$sess_username'") == true) {
                $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Kata Sandi Kamu Berhasil Diubah.<script>swal("Berhasil!", "Kata Sandi Kamu Berhasil Diubah.", "success");</script>');
            } else {
                $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
            }

        }

    }

} else if (isset($_POST['ganti_pin'])) {
    $pin = $conn->real_escape_string(trim(filter($_POST['pin_lama'])));
    $pin_baru = $conn->real_escape_string(trim(filter($_POST['pin_baru'])));
    $konfirmasi_pin_baru = $conn->real_escape_string(trim(filter($_POST['konfirmasi_pin_baru'])));

    $error = array();
    if (empty($pin)) {
        $error ['pin_lama'] = '*Tidak Boleh Kosong.';
    }
    if (empty($pin_baru)) {
        $error ['pin_baru'] = '*Tidak Boleh Kosong.';
    } else if (strlen($pin_baru) <> 6) {
        $error ['pin_baru'] = '*PIN Harus 6 Digit.';
    }
    if (empty($konfirmasi_pin_baru)) {
        $error ['konfirmasi_pin_baru'] = '*Tidak Boleh Kosong.';
    } else if (strlen($konfirmasi_pin_baru) <> 6) {
        $error ['konfirmasi_pin_baru'] = '*PIN Harus 6 Digit.';
    } else if ($pin_baru <> $konfirmasi_pin_baru) {
        $error ['konfirmasi_pin_baru'] = '*Konfirmasi PIN Baru Tidak Sesuai.';
    } else {

        if ($pin <> $data_user['pin']) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, PIN Lama Yang Kamu Masukkan Tidak Sesuai.<script>swal("Gagal!", "PIN Lama Yang Kamu Masukkan Tidak Sesuai.", "error");</script>');
        } else {

            if ($conn->query("UPDATE users SET pin = '$pin_baru' WHERE username = '$sess_username'") == true) {
                $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip! PIN Kamu Berhasil Diubah.<script>swal("Berhasil!", "PIN Kamu Berhasil Diubah.", "success");</script>');
            } else {
                $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
            }

        }

    }

} else if (isset($_POST['ganti_api_key'])) {
    $api_barunya = acak(13);
    if ($conn->query("UPDATE users SET api_key = '$api_barunya' WHERE username = '$sess_username'") == true) {
        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Yeah, Api Key Kamu Berhasil Diubah.<script>swal("Berhasil!", "API Key Kamu Berhasil Diubah.", "success");</script>');
    } else {
        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
    }
}

require("../lib/class.phpmailer.php");
if (isset($_POST['kirim_email'])){
$email = $data_user['email'];
$cek_pengguna = $conn->query("SELECT * FROM users WHERE email = '$email'");
$cek_pengguna_ulang = mysqli_num_rows($cek_pengguna);
$data_pengguna = mysqli_fetch_assoc($cek_pengguna);
$verifAccount = $config['web']['url']."auth/verification-account.php";
$kode_verifikasi = acak_nomor(3).acak_nomor(3);
$pengguna = $data_pengguna['username'];
$mail = new PHPMailer;
$mail->IsSMTP();
$mail->SMTPSecure = 'ssl';
$mail->Host = "mail.iaccesoris.com"; //host masing2 provider email
$mail->SMTPDebug = 2;
$mail->Port = 465;
$mail->SMTPAuth = true;
$mail->Username = "no-reply@iaccesoris.com"; //user email
$mail->Password = "Mask!@#$";  //password email
$mail->SetFrom("no-reply@iaccesoris.com","Iaccesoris"); //set email pengirim
$mail->Subject = "Verifikasi Akun Iaccesoris"; //subyek email
$mail->AddAddress("$email","");  //tujuan email
$mail->MsgHTML("<html>
                <head>
                    <meta name='viewport' content='width=device-width, initial-scale=1'>
                    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
                    <meta name='author' content='Konterin'>
                    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.0.7/css/all.css'>
                    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet'>
                    <style type='text/css'>
                        body,table,td,a{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{mso-table-lspace:0pt;mso-table-rspace:0pt}table{border-collapse:collapse!important}body{font-family:'Dosis';height:100%!important;margin:0!important;padding:0!important;width:100%!important}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}@media screen and (max-width:600px){h1{font-size:32px!important;line-height:32px!important}}div[style*='margin: 16px 0;']{margin:0!important}
                    </style>
                </head>

                <body>
                <p>Ini adalah email otomatis yang dikirim oleh sistem. Mohon untuk tidak membalas ke email ini karena kami tidak memantau pesan yang masuk ke email ini. Untuk bertanya/keluhan, silahkan menghubungi kami melalui whatsapp</p>
                <p>Hallo $pengguna</p>
                <p>Tinggal satu langkah lagi, Kamu akan menjadi bagian dari member Iaccesoris.</p>
                <p>Berikut ini adalah kode verifikasi kamu <b>$kode_verifikasi</b></p>
                <p>Silahkan Klik <a href=$verifAccount>Link Ini</a></p>
                <b>Terima Kasih</b>
                <font color='#888888'><br>
                Iaccesoris<br>
                <br>
                </font>
                </body>
                </html>");
if ($mail->Send());
if ($conn->query("UPDATE users SET kode_verifikasi = '$kode_verifikasi' WHERE username = '".$data_pengguna['username']."'") == true) {
    $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Kode Verifikasi Berhasil Dikirim Ke Email Kamu.<script>swal("Berhasil!", "Kode Verifikasi Berhasil Dikirim. Silahkan Cek Email kamu di Folder Inbox/Spam!", "success");</script>');
} else {
    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
}

}
if (isset($_POST['kirim_kode'])) {
    $pin = acak_nomor(3) . acak_nomor(3);
    $email = $data_user['email'];
    $cek_pengguna = $conn->query("UPDATE users SET pin = '$pin' WHERE email = '$email' AND username = '$sess_username'");
    $cek_pengguna_ulang = mysqli_num_rows($cek_pengguna);
    $data_pengguna = mysqli_fetch_assoc($cek_pengguna);

    $error = array();


    if ($data_email['pin'] == "Sudah Dikirim") {
        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Akun Kamu Sudah Di Verifikasi.<script>swal("Gagal!", "Akun Kamu Sudah Di Verifikasi.", "error");</script>');

    } else {

        $pengguna = $data_pengguna['username'];

        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->SMTPSecure = 'ssl';
        $mail->Host = "mail.iaccesoris.com"; //host masing2 provider email
        $mail->SMTPDebug = 2;
        $mail->Port = 465;
        $mail->SMTPAuth = true;
        $mail->Username = "no-reply@iaccesoris.com"; //user email
        $mail->Password = "Mask!@#$";  //password email
        $mail->SetFrom("no-reply@iaccesoris.com", "iaccesoris"); //set email pengirim
        $mail->Subject = "Permintaan Reset PIN Baru Kamu"; //subyek email
        $mail->AddAddress("$email", "");  //tujuan email
        $mail->MsgHTML("<html>

<head>
    
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name='author' content='Konterin'>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.0.7/css/all.css'>
    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet'>
    <style type='text/css'>
        body,table,td,a{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{mso-table-lspace:0pt;mso-table-rspace:0pt}table{border-collapse:collapse!important}body{font-family:'Dosis';height:100%!important;margin:0!important;padding:0!important;width:100%!important}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}@media screen and (max-width:600px){h1{font-size:32px!important;line-height:32px!important}}div[style*='margin: 16px 0;']{margin:0!important}
    </style>
</head>

<body style='background-color:#f4f4f4;margin:0 !important;padding:0 !important;'>
    <div
        style='display:none;font-size:1px;color:#fefefe;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;'>
        Kami senang Anda ada di sini! Bersiaplah untuk menggunakan akun baru Anda.
    </div>

    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
        <tr>
            <td bgcolor='#6772e5' align='center'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width:600px;'>
                    <tr>
                        <td style='padding-bottom:40px;'></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor='#6772e5' align='center' style='padding:0px 10px 0px 10px;'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>
                    <tr>
                        <td bgcolor='#ffffff' align='center' valign='top'
                            style='padding:40px 20px 20px 20px;border-radius:4px 4px 0px 0px;letter-spacing:4px;line-height:48px;'>
                            <h1 style='color: #111111;font-size:48px;font-weight:bold;margin:0;'>
                                <img src='https://konter.in/image/logo/logo.png'>
                            </h1>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor='#f4f4f4' align='center' style='padding:0px 10px 0px 10px;'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width:600px;'>
                    <tr>
                        <td bgcolor='#ffffff' align='left' style='padding:20px 30px 40px 30px;line-height:25px;'>
                            <p style='color:#666666;font-size:18px;font-weight:bold;margin:0;'>
              
                                Kamu baru saja melakukan permintaan Reset PIN pada akun kamu, PIN kamu saat ini ada dibawah ini:
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor='#ffffff' align='left'>
                            <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td bgcolor='#ffffff' align='center' style='padding:40px 30px 60px 30px;'>
                                        <table border='0' cellspacing='0' cellpadding='0'>
                                            <tr>
                                                <td align='center' style='border-radius:3px;font-weight: bold;font-size:40px;' bgcolor='#ffe705'>
                                                  $pin 
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                     <td bgcolor='#ffffff' align='left' style='padding:0px 30px 0px 30px;line-height:25px;'>
                            <p style='color:#666666;font-size: 18px;font-weight:bold;margin:0;'>
                                Silahkan simpan PIN kamu saat ini agar bisa dapat bertransaksi ^_^
                            </p>
                        </td>
                          </tr>
                        <tr>
                        <td bgcolor='#ffffff' align='left' style='padding:0px 30px 0px 30px;line-height:25px;'>
                            <p style='color:#666666;font-size: 18px;font-weight:bold;margin:0;'>
                                Terima kasih telah mempercayai kami Iaccesoris sebagai kebutuhan digital kesayangan anda!
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor='#ffffff' align='left'
                            style='padding:0px 30px 40px 30px;border-radius:0px 0px 4px 4px;line-height:25px;'>
                            <p style='color:#666666;font-size:18px;font-weight:bold;margin:0;'>
                            <br>
                               <small> Iaccesoris</small><br>
                               
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor='#f4f4f4' align='center' style='padding:30px 10px 0px 10px;'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width:600px;'>
                    <tr>
                        <td bgcolor='#f4f4f4' align='left' style='padding:0px 30px 30px 30px;line-height:18px;'>
                            <p style='color:#666;font-size:14px;font-weight:bold;margin:0;'>
                                Anda menerima email ini karena Anda baru saja mendaftar untuk akun baru di,
                                <a href='https://konter.in/' target='_blank'
                                    style='color:#111111;text-decoration:none;'>Lihat di Browser</a>.
                            </p>
                        </td>
                    </tr>
                   
                </table>
            </td>
        </tr>
    </table>
</body>
</html>");
        if ($mail->Send()) ;
        if ($conn->query("UPDATE users SET pin = '$pin' WHERE username = '" . $data_pengguna['username'] . "'") == true) {
            $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'PIN Berhasil Direset dan Dikirim Ke Email Kamu.<script>swal("Berhasil!", "Kode Verifikasi Berhasil Dikirim. Silahkan Cek Email kamu!", "success");</script>');
        } else {
            $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
        }
    }
}


require '../lib/header.php';
?>

    <div class="container-fluid">


    <!-- Content Row -->

    <div class="row">

    <div class="container">
    <div class="main-body">


    <div class="row gutters-sm">
        <div class="col-md-4 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-column align-items-center text-center">
                        <img src="/img/841490_glasses_512x512.png" alt="Admin" class="rounded-circle" width="150">
                        <div class="mt-3">
                            <h4><?php echo $data_user['nama']; ?></h4>
                            <p class="text-secondary mb-1"><?php echo $data_user['level']; ?></p>
                            <p class="text-muted font-size-sm">Saldo:
                                Rp <?php echo number_format($data_user['saldo_top_up'], 0, ',', '.'); ?>
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                Points: <?php echo number_format($data_user['koin'], 0, ',', '.'); ?>
                                <br/>
                                Email: <?php echo $data_user['status_akun'] ?>
                                <br/>Silahkan Verifikasi Email Untuk Merubah Pin
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <form class="form-horizontal" role="form" method="POST">
                <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                <div class="card mt-3">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <i class="fas fa-code"></i>
                            <span class="text-secondary"><?php echo $data_user['api_key']; ?></span>
                            <button name="ganti_api_key" id="ganti_api_key" class="btn btn-danger btn-sm">UBAH API
                            </button>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <i class="fas fa-key"></i>
                            <span class="text-secondary"><?php
                                $pin = $data_user['pin'];
                                echo substr($pin, 0, -3) . 'XXX';
                                ?></span>
                            <?php
                            $verif = $data_user['status_akun'];
                            if($verif == 'Sudah Verifikasi'){
                                ?>
                                <button name="kirim_kode" id="kirim_kode" class="btn btn-danger btn-sm">RESET PIN</button>
                                <?php
                            }
                            ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <i class="fas fa-envelope"></i>
                            <span class="text-secondary"><?php
                                $email = $data_user['email'];
                                echo $email;
                                ?></span>
                            <?php
                             $verif = $data_user['status_akun'];
                             if($verif == 'Belum Verifikasi'){
                                 ?>
                                 <button name="kirim_email" id="kirim_email" class="btn btn-danger btn-sm">Verifikasi Email</button>
                                 <?php
                             }
                            ?>
                        </li>
            </form>
            </ul>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card mb-5">
            <div class="card-body">
                <form class="kt-form kt-form--label-right" method="POST">
                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">

                    <?php
                    if (isset($_SESSION['hasil'])) {
                        ?>
                        <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible"
                             role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <?php echo $_SESSION['hasil']['pesan'] ?>
                        </div>
                        <?php
                        unset($_SESSION['hasil']);
                    }
                    ?>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-5 col-form-label">Fullname</label>
                        <div class="col-lg-10 col-xl-9">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fas fa-user text-primary"></i></span></div>
                                <input type="text" class="form-control" name="nama"
                                       value="<?php echo $data_user['nama']; ?>" readonly>
                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-5 col-form-label">Username</label>
                        <div class="col-lg-10 col-xl-9">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fas fa-user text-primary"></i></span></div>
                                <input type="text" class="form-control" name="username"
                                       value="<?php echo $data_user['username']; ?>" readonly>
                            </div>

                        </div>
                    </div>
                    <hr>

                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-5 col-form-label">Kata Sandi Lama</label>
                        <div class="col-lg-9 col-xl-9">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fas fa-lock text-primary"></i></span></div>
                                <input type="password" name="password_lama" class="form-control"
                                       value="<?php echo $password; ?>" placeholder="Old Password">
                            </div>
                            <span class="form-text text-muted"><?php echo ($error['password_lama']) ? $error['password_lama'] : ''; ?></span>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-5 col-form-label">Kata Sandi Baru</label>
                        <div class="col-lg-9 col-xl-9">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fas fa-lock text-primary"></i></i></span></div>
                                <input type="password" name="password_baru" class="form-control"
                                       value="<?php echo $password_baru; ?>" placeholder="New Password">
                            </div>
                            <span class="form-text text-muted"><?php echo ($error['password_baru']) ? $error['password_baru'] : ''; ?></span>
                        </div>
                    </div>
                    <hr>

                    <div class="form-group form-group-last row">
                        <label class="col-xl-3 col-lg-5 col-form-label">Konfirmasi Kata Sandi Baru</label>
                        <div class="col-lg-9 col-xl-9">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="fa fa-lock text-primary"></i></span></div>
                                <input type="password" name="konf_pass_baru" class="form-control"
                                       value="<?php echo $konf_pass_baru; ?>" placeholder="Same Password">
                            </div>
                            <span class="form-text text-muted"><?php echo ($error['konf_pass_baru']) ? $error['konf_pass_baru'] : ''; ?></span>
                        </div>
                    </div>
                    <hr>

                    <div class="row">

                        <div class="col-lg-9 col-xl-9 ">
                            <button type="submit" name="ganti_password"
                                    class="btn btn-primary btn-elevate btn-pill btn-elevate-air">Save
                            </button>
                            <a href="/profile" class="btn btn-danger btn-elevate btn-pill btn-elevate-air">Kembali</a>
                        </div>
                    </div>
            </div>
        </div>
    </div>


    <!-- End Tab Content -->

    <!-- End App Content -->

    <!-- Start Scrolltop -->

    <!-- End Scrolltop -->


    <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>

<?php
require '../lib/footer.php';
?>