<?php 
session_start();
require '../config.php';
require '../lib/session_login.php';
require '../lib/session_user.php';

        if (isset($_POST['ganti_profil'])) {
            $nama_depan = $conn->real_escape_string(trim(filter($_POST['nama_depan'])));
            $nama_belakang = $conn->real_escape_string(trim(filter($_POST['nama_belakang'])));
            $email = $conn->real_escape_string(trim(filter($_POST['email'])));
            $no_hp = $conn->real_escape_string(trim(filter($_POST['no_hp'])));
            $pin = $conn->real_escape_string(trim(filter($_POST['pin1'])));

            $cek_email = $conn->query("SELECT * FROM users WHERE email = '$email'");
            $cek_email_ulang = mysqli_num_rows($cek_email);
            $data_email = mysqli_fetch_assoc($cek_email);

            $cek_no_hp = $conn->query("SELECT * FROM users WHERE no_hp = '$no_hp'");
            $cek_no_hp_ulang = mysqli_num_rows($cek_no_hp);
            $data_no_hp = mysqli_fetch_assoc($cek_no_hp);

            $error = array();
            if (empty($nama_depan)) {
		        $error ['nama_depan'] = '*Tidak Boleh Kosong.';
            }
            if (empty($nama_belakang)) {
		        $error ['nama_belakang'] = '*Tidak Boleh Kosong.';
            }
            if (empty($email)) {
		        $error ['email'] = '*Tidak Boleh Kosong.';
            } else if ($cek_email_ulang == 0) {
		        $error ['email'] = '*Email Sudah Terdaftar.';
            }
            if (empty($no_hp)) {
		        $error ['no_hp'] = '*Tidak Boleh Kosong.';
            } else if ($cek_no_hp_ulang == 0) {
		        $error ['no_hp'] = '*Nomor HP Sudah Terdaftar.';
            }
            if (empty($pin)) {
		        $error ['pin1'] = '*Tidak Boleh Kosong.';
            } else if ($pin <> $data_user['pin']) {
		        $error ['pin1'] = '*PIN Yang Kamu Masukkan Salah.';
            } else {

   		    if ($conn->query("UPDATE users SET nama_depan = '$nama_depan', nama_belakang = '$nama_belakang', email = '$email', no_hp = '$no_hp' WHERE username = '$sess_username'") == true) {
   			    $_SESSION['hasil'] = array('alert' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Yeah, Data Profil Kamu Berhasil Diubah.<script>swal("Berhasil!", "Data Profil Kamu Berhasil Diubah.", "success");</script>');
            } else {
   			    $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Gagal', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
   		    }

        }

        } else if (isset($_POST['ganti_password'])) {
            $password = $conn->real_escape_string(trim(filter($_POST['password_lama'])));
            $password_baru = $conn->real_escape_string(trim(filter($_POST['password_baru'])));
            $konf_pass_baru = $conn->real_escape_string(trim(filter($_POST['konf_pass_baru'])));

            $cek_passwordnya = password_verify($password, $data_user['password']);
            $hash_passwordnya = password_hash($password_baru, PASSWORD_DEFAULT);


            $error = array();
            if (empty($password)) {
		        $error ['password_lama'] = '*Tidak Boleh Kosong.';
            }
            if (empty($password_baru)) {
		        $error ['password_baru'] = '*Tidak Boleh Kosong.';
            } else if (strlen($password_baru) < 6 ){
		        $error ['password_baru'] = '*Kata Sandi Minimal 6 Karakter.';
            }
            if (empty($konf_pass_baru)) {
		        $error ['konf_pass_baru'] = '*Tidak Boleh Kosong.';
            } else if (strlen($konf_pass_baru) < 6 ){
		        $error ['konf_pass_baru'] = '*Kata Sandi Minimal 6 Karakter.';
            } else if ($password_baru <> $konf_pass_baru){
		        $error ['konf_pass_baru'] = '*Konfirmasi Kata Sandi Baru Tidak Sesuai.';
            } else {

            if ($cek_passwordnya <> $data_user['password']) {
                $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Kata Sandi Lama Yang Kamu Masukkan Tidak Sesuai.<script>swal("Gagal!", "Kata Sandi Lama Yang Kamu Masukkan Tidak Sesuai.", "error");</script>');
            } else {

   		    if ($conn->query("UPDATE users SET pin = '$pin' AND SET password = '$hash_passwordnya' WHERE username = '$sess_username'") == true) {
   			    $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip! Kata Sandi Kamu Berhasil Diubah.<script>swal("Berhasil!", "Kata Sandi Kamu Berhasil Diubah.", "success");</script>');
            } else {
   			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
   		    }

            }
        
        }

        } else if (isset($_POST['ganti_pin'])) {
            $pin = $conn->real_escape_string(trim(filter($_POST['pin_lama'])));
            $pin_baru = $conn->real_escape_string(trim(filter($_POST['pin_baru'])));
            $konfirmasi_pin_baru = $conn->real_escape_string(trim(filter($_POST['konfirmasi_pin_baru'])));

            $error = array();
            if (empty($pin)) {
		        $error ['pin_lama'] = '*Tidak Boleh Kosong.';
            }
            if (empty($pin_baru)) {
		        $error ['pin_baru'] = '*Tidak Boleh Kosong.';
            } else if (strlen($pin_baru) <> 6) {
		        $error ['pin_baru'] = '*PIN Harus 6 Digit.';
            }
            if (empty($konfirmasi_pin_baru)) {
		        $error ['konfirmasi_pin_baru'] = '*Tidak Boleh Kosong.';
            } else if (strlen($konfirmasi_pin_baru) <> 6 ){
		        $error ['konfirmasi_pin_baru'] = '*PIN Harus 6 Digit.';
            } else if ($pin_baru <> $konfirmasi_pin_baru){
		        $error ['konfirmasi_pin_baru'] = '*Konfirmasi PIN Baru Tidak Sesuai.';
            } else {

            if ($pin <> $data_user['pin']) {
                $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, PIN Lama Yang Kamu Masukkan Tidak Sesuai.<script>swal("Gagal!", "PIN Lama Yang Kamu Masukkan Tidak Sesuai.", "error");</script>');
            } else {

   		    if ($conn->query("UPDATE users SET pin = '$pin_baru' WHERE username = '$sess_username'") == true) {
   			    $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip! PIN Kamu Berhasil Diubah.<script>swal("Berhasil!", "PIN Kamu Berhasil Diubah.", "success");</script>');
            } else {
   			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
   		    }

            }
        
        }

        } else if (isset($_POST['ganti_api_key'])) {
		    $api_barunya = acak(20);
		    if ($conn->query("UPDATE users SET api_key = '$api_barunya' WHERE username = '$sess_username'") == true) {
   		        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Yeah, Api Key Kamu Berhasil Diubah.<script>swal("Berhasil!", "API Key Kamu Berhasil Diubah.", "success");</script>');
		    } else {
   		        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
		    }
        }
        require '../lib/header.php';

?>

       <div class="container-fluid">

 
                  
                        

                    <!-- Content Row -->

                    <div class="row">

<div class="container">
    <div class="main-body">
    
    
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                   <img src="/img/logz.png" alt="Admin" class="rounded-circle" width="150">
                    <div class="mt-3">
                      <h4><?php echo $data_user['nama']; ?></h4>
                      <p class="text-secondary mb-1"><?php echo $data_user['level']; ?></p>
                      
                      <p class="text-muted font-size-sm">Saldo: Rp <?php echo number_format($data_user['saldo_top_up'],0,',','.'); ?> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  Points: <?php echo number_format($data_user['koin'],0,',','.'); ?></p>
                      <a href="/deposit" class="btn btn-outline-primary"><i class="fas fa-plus-circle"></i> Deposit</a>
                      <a href="setting"class="btn btn-outline-primary"><i class="fas fa-cogs fa-sm fa-fw mr-2"></i> Setting Akun</a>
                    </div>
                  </div>
                  
                </div>
              </div>
              <div class="card mt-3">
			  	
                <ul class="list-group list-group-flush">
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <i class="fas fa-code"></i>
				  <span class="text-secondary"><?php echo $data_user['api_key']; ?></span>

                  </li>
                  
                  
                 
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <i class="fas fa-key"></i>
					  <span class="text-secondary"><?php 
					  $pin = $data_user['pin'];
echo substr($pin, 0, -3) . 'XXX';
					 ?></span> 

                  </li>
                  
                 
                </ul>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card mb-3">
                <div class="card-body">
				                       <?php
                            if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                            }
                            ?>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      <?php echo $data_user['nama']; ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      <?php echo $data_user['email']; ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Phone</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      <?php echo $data_user['no_hp']; ?>
                    </div>
                  </div>
                  <hr>
                 <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Total Penggunaan</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      Rp <?php echo number_format($data_user['pemakaian_saldo'],0,',','.'); ?>
                    </div>
                  </div>
  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Terdaftar Sejak</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      <?php
                      date_default_timezone_set('Asia/Jakarta');
                      echo tanggal_indo($data_user['date']) .", ";
                      
					  echo $data_user['time']; 

					 ?>
                    </div>
                  </div>
                  <hr>
                  
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Log Aktivitas</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                                IP: <?php echo $log['ip']; ?>, <?php echo  tanggal_indo($log['date']); ?> <?php echo $log['time']; ?> (<?php echo $log['aksi']; ?>)<BR>
                      <?php echo $logsaldo['pesan']; ?>
                    </div>
                  </div>      
      
                </div>
              </div>

              
              </div>



            </div>
          </div>

        </div>
    </div>
       
    </div>
    <!-- End Tab Content -->

</div>
<!-- End App Content -->

        <!-- Start Scrolltop -->
		
		<!-- End Scrolltop -->
		
		<br />
		<br />
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
<script type="text/javascript">
	    function copy_to_clipboard(element) {
	        var copyText = document.getElementById(element);
	        copyText.select();
	        document.execCommand("copy");
	    }
	    </script>
<?php
require '../lib/footer.php';
?>