<?php
require 'session_login.php';
require 'database.php';
require 'csrf_token.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $data['title']; ?></title>
	<meta name="description" content="<?php echo $data['short_title']; ?>" />
    <meta name="keywords" content="<?php echo $data['short_title']; ?>, smm ppob, ppob termurah, game online terlengkap" />
    <meta name="author" content="Konterin" />
	<!-- Site favicon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/faviconn.png" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin-2.min.css" rel="stylesheet">

</head>
<?php
if (isset($_SESSION['user']) OR isset($_COOKIE['username'])) {
 
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-white sidebar sidebar-primary accordion toggled" >

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center">
                <div class="sidebar-brand-icon text-gray-200 rounded">
        <img width="100%" src="/img/logo.png">
                </div>
               
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/">
                    <i class="fas fa-home"></i>
                    <span>Home</span></a>
            </li>

            <!-- Divider -->

      <hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/status">
                    <i class="fas fa-sync"></i>
                    <span>Status Transaksi</span></a>
            </li>

<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/cara-transaksi">
                    <i class="fas fa-shopping-basket"></i>
                    <span>Cara Transaksi</span></a>
            </li>
			
			<hr class="sidebar-divider my-1">
			            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/daftar-harga">
                    <i class="fas fa-list"></i>
                    <span>Daftar Harga</span></a>
            </li>
						<hr class="sidebar-divider my-1">
			<li class="nav-item active">
                <a class="nav-link" href="/kontak-kami">
                    <i class="fas fa-envelope"></i>
                    <span>Kontak Kami</span></a>
            </li>
          
			<hr class="sidebar-divider my-1">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/api">
                    <i class="fas fa-fw fa-code"></i>
                    <span>API Documentation</span></a>
            </li>

          
<div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

               <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                 
                    <!-- Topbar Navbar -->
           <ul class="navbar-nav ml-auto">

                        
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <!-- Nav Item - Alerts -->
                        <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-bell fa-fw"></i>
                                <!-- Counter - Alerts -->
                   <span class="badge badge-danger badge-counter"><?php echo $count_berita ?></span>
                            </a>
                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                                <h6 class="dropdown-header">
                                   INFORMASI & BERITA
                                </h6>
												 <?php
			        $cek_berita = $conn->query("SELECT * FROM berita ORDER BY id DESC");
			        while ($data_berita = $cek_berita->fetch_assoc()) {
			        if ($data_berita['tipe'] == "INFO") {
                        $label = "info";
			        } else if ($data_berita['tipe'] == "PERINGATAN") {
                        $label = "warning";
			        } else if ($data_berita['tipe'] == "PENTING") {
                        $label = "danger";
			        }

			        if ($data_berita['icon'] == "PESANAN") {
                        $label_icon = "flaticon2-shopping-cart";
			        } else if ($data_berita['icon'] == "LAYANAN") {
                        $label_icon = "flaticon-signs-1";
			        } else if ($data_berita['icon'] == "DEPOSIT") {
                        $label_icon = "flaticon-coins";
			        } else if ($data_berita['icon'] == "PENGGUNA") {
                        $label_icon = "flaticon2-user";
			        } else if ($data_berita['icon'] == "PROMO") {
                        $label_icon = "flaticon2-percentage";
			        }
			        ?>
											
		<br>
                              	<center>
                                	   <div class="mr-3">
                                        <div class="icon col-xl-9 ">		   <span class="badge badge-<?php echo $label; ?> kt-badge--inline"><?php echo $data_berita['tipe']; ?></span>
										<a href="<?php echo $config['web']['url'] ?>page/news-details?id=<?php echo $data_berita['id']; ?>" class="text-dark font-weight-bold text-uppercase mb-1">
                                        <span class="font-weight-bold"><?php echo $data_berita['title']; ?></span>                                </a>             <div class="small text-gray-500">(<?php echo tanggal_indo($data_berita['date']); ?>)</div>
                                      
                                        </div>
                                    </div>
                                   	<div>
                           
								 <span class="text-xs font-weight-bold text-primary text-uppercase mb-1 "><?php 
								 			 $teks = $data_berita['konten'];

//pemotongan dengan karakter sejumlah 100
		 echo substr($teks, 0, 60) . '...';
		
								 
								 
								 
								 ?> <a href="<?php echo $config['web']['url'] ?>page/news-details?id=<?php echo $data_berita['id']; ?>" class="text-dark font-weight-bold text-uppercase mb-1">READMORE <i class="fas fa-arrow-right"></i></a></span>  
                                    </div></center>

										
                         <?php 
					}
?>
						 
                                <a class="dropdown-item text-center small text-gray-500" href="/pemberitahuan">Show All Alerts</a>
                            </div>
                        </li>

                        <!-- Nav Item - Messages -->
                        <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-envelope fa-fw"></i>
                                <!-- Counter - Messages -->
                               <?php if (mysqli_num_rows($CallDBTiket) !== 0) { ?> <span class="badge badge-danger badge-counter"><b><?php echo mysqli_num_rows($CallDBTiket); ?></b></span><?php } ?>
			</span>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                                <h6 class="dropdown-header">
                                    Message Center
                                </h6>
								 <?php
                    if ($idtiket['status'] == "Responded") {
                    ?>
                                <a class="dropdown-item d-flex align-items-center" href="<?php echo $config['web']['url']; ?>page/help-reply?id=<?php echo $idtiket['id']; ?>">
                                    <div class="dropdown-list-image mr-3">
                                        <img class="rounded-circle" src="/img/clipart2050385.png" alt="...">
                                        <div class="status-indicator bg-success"></div>
                                    </div>
                                    <div class="font-weight-bold">
									<div class="text-truncate"><?php echo $tikett['pesan']; ?></div>
                                        <div class="small text-gray-500"><?php echo $tikett['pengirim']; ?>, <?php echo time_elapsed_string($tikett['update_terakhir']); ?></div>
                                    </div>
                                </a>
					<?php 
					} ?>
                        <a class="dropdown-item text-center small text-gray-500" href="<?php echo $config['web']['url']; ?>help">Read More Messages</a>
                            </div>
                        </li>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600"> <?php echo $data_user['username']; ?></span>
                                <img class="img-profile rounded-circle" src="/img/841490_glasses_512x512.png">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="/profile">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <a class="dropdown-item" href="/profile/setting">
                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Settings
                                </a>
                                 <a class="dropdown-item" href="/profile/referral">
                                    <i class="fas fa-user-plus fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Referral
                                </a>
								 <div class="dropdown-divider"></div>
								                                <a class="dropdown-item" href="/riwayat-akun">
                                    <i class="fas fa-shopping-cart fa-sm fa-fw mr-2 text-gray-400"></i>
                                   Riwayat Pembelian
                                </a>
                              								                                <a class="dropdown-item" href="/riwayat-penggunaan">
                                    <i class="fas fa-retweet fa-sm fa-fw mr-2 text-gray-400"></i>
                                   Riwayat Penggunaan Saldo
                                </a>
                              						 <div class="dropdown-divider"></div>
									                                <a class="dropdown-item" href="/deposit">
                                    <i class="fas fa-plus-circle fa-sm fa-fw mr-2 text-gray-400"></i>
                                   Deposit
                                </a>
									                                <a class="dropdown-item" href="/help">
                                    <i class="fas fa-comments fa-sm fa-fw mr-2 text-gray-400"></i>
                                   Open Ticket
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

               

            

                </nav>
<?php } ?>