<?php
	if (!isset($_SESSION['user']) OR !isset($_COOKIE['username'])) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Silahkan Masuk terlebih dahulu untuk melanjutkan!');
		exit(header("Location: ".$config['web']['url']."auth/login"));
	}	