<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/header.php';

	    if (isset($_GET['id'])) {
		    $id = abs($_GET['id']);

		    $cek_berita = $conn->query("SELECT * FROM berita WHERE id = '$id'");
		    $data_berita = mysqli_fetch_assoc($cek_berita);

		    if ($data_berita['tipe'] == "INFO") {
                $label = "info";
		    } else if ($data_berita['tipe'] == "PERINGATAN") {
                $label = "warning";
		    } else if ($data_berita['tipe'] == "PENTING") {
                $label = "danger";
		    }

		    if ($data_berita['icon'] == "PESANAN") {
                $label_icon = "flaticon2-shopping-cart";
		    } else if ($data_berita['icon'] == "LAYANAN") {
                $label_icon = "flaticon-signs-1";
		    } else if ($data_berita['icon'] == "DEPOSIT") {
                $label_icon = "flaticon-coins";
		    } else if ($data_berita['icon'] == "PENGGUNA") {
                $label_icon = "flaticon2-user";
		    } else if ($data_berita['icon'] == "PROMO") {
                $label_icon = "flaticon2-percentage";
		    }

		    if ($cek_berita->num_rows == 0) {
			    header("Location: ".$config['web']['url']."page/news");
		    } else {

?>

  
      <!-- Begin Page Content -->
                <div class="container-fluid">

 
                    <!-- Content Row -->
                    <div class="row">
					  <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"><small>             <span class="badge badge-<?php echo $label; ?> kt-badge--inline"><?php echo $data_berita['tipe']; ?></span></small>
                                                <b style="text-transform: uppercase;">
                                                    <?php echo $data_berita['title']; ?>
                                               
												</b>
												<small> 
                                                <span class="kt-notes__desc">
                                                    (<?php echo tanggal_indo($data_berita['date']); ?>)
                                                </span></small></h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">

<p >


                                            Hallo <b><?php echo $data_user['username']; ?></b>, <?php echo nl2br($data_berita['konten']); ?>
                                        </p>  
                                               
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End News Details -->

        </div>
        <!-- End Content -->

        <!-- Start Scrolltop -->
		
		<!-- End Scrolltop -->

<?php ?>

<?php 
require '../lib/footer.php';
}
} else {
	header("Location: ".$config['web']['url']."page/news");
}
?>

