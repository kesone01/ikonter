<?php
require '../../config.php';
require '../../lib/database.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?php echo $data['title']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $data['short_title']; ?>" />
    <meta name="keywords" content="<?php echo $data['short_title']; ?>, smm ppob, ppob termurah, game online terlengkap" />
    <meta name="author" content="Konterin" />
    <!-- favicon -->
    <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/favicon.png">
      
    <!-- Bootstrap -->
    <link href="<?php echo $config['web']['url'] ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="<?php echo $config['web']['url'] ?>assets/css/materialdesignicons.min.css.kos" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo $config['web']['url'] ?>assets/css/unicons.css.kos">
    <!-- Magnific -->
    <link href="<?php echo $config['web']['url'] ?>assets/css/magnific-popup.css" rel="stylesheet" type="text/css" />
    <!-- Slider -->
    <link rel="stylesheet" href="<?php echo $config['web']['url'] ?>assets/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="<?php echo $config['web']['url'] ?>assets/css/owl.theme.default.min.css" />
    <!-- Main Css -->
    <link href="<?php echo $config['web']['url'] ?>assets/css/main.css" rel="stylesheet" type="text/css" id="theme-opt" />
    <link href="<?php echo $config['web']['url'] ?>assets/css/colors/default.css" rel="stylesheet" id="color-opt">

</head>

<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <!-- Loader -->

    <!-- Navbar STart -->
    <header id="topnav" class="defaultscroll sticky">
        <div class="container">
            <!-- Logo container-->
            <div>
                <a class="logo" href="<?php echo $config['web']['url'] ?>">Konterin<span class="text-primary">.</span></a> 
            </div>
        
            <!--end login button-->
            <!-- End Logo container-->
            <div class="menu-extras">
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li><a href="<?php echo $config['web']['url'] ?>">Home</a></li>
                    <li><a href="https://konterin.com/page/tentang-kami/">Tentang Kami</a></li>
                    <li><a href="https://konterin.com/page/list-harga/">List Harga</a></li>

                </ul>
                <!--end navigation menu-->
                
                <!--end login button-->
            </div>
            <!--end navigation-->
        </div>
        <!--end container-->
    </header>
    <!--end header-->
    <!-- Navbar End -->


    <!-- * ISI CONTENT NYA * -->
<!-- Hero Start -->
    <section class="bg-half-100 d-table w-100" id="home">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-7">
                    <div class="title-heading mt-4">
             
                        <h1 class="heading mb-3">
                            Tentang <span class="text-primary">Konterin</span>
                        </h1>
                     <p><i data-feather="map-pin" class="fea icon-ex-md text-primary"></i> Jl. Karonsih Utara 8,50181
                       
                        </p>
                        <p><i data-feather="smartphone" class="fea icon-ex-md text-primary"></i> WA 081370903694 (07.30-22.00 WIB) </p>
                        <p><i data-feather="mail" class="fea icon-ex-md text-primary"></i> cs@iaccesoris.com</p>
                            <p><i data-feather="link" class="fea icon-ex-md text-primary"></i> https://konter.in/</p>
                        <div class="mt-4">
                            <a href="<?php echo $config['web']['url'] ?>auth/login" class="btn btn-outline-danger mt-2"><i class="mdi mdi-login"></i>
                                Login</a>
                            <a href="https://konterin.com/auth/register" target="_blank"
                                class="btn btn-outline-primary mt-2"><i class="mdi mdi-google-play"></i> Daftar  </a>
                        </div>
                    </div>
                </div>
                <!--end col-->

            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
  


    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-footer">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->
<!-- Footer Start -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                    <a class="logo-footer" href="#"><?php echo $data['short_title']; ?><span class="text-primary">.</span></a>
                    <p class="mt-4"><?php echo $data['title']; ?> </p>
                    <ul class="list-unstyled social-icon social mb-0 mt-4">
                            <li class="list-inline-item"><a href="https://instagram.com/" target="_blank"
                                    class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                            <li class="list-inline-item"><a href="https://instagram.com/" target="_blank"
                                    class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                            <li class="list-inline-item"><a href="mailto:cs@iaccesoris.com" 
                                    class="rounded"><i data-feather="at-sign" class="fea icon-sm fea-social"></i></a></li>
                            <li class="list-inline-item"><a
                                    href="https://api.whatsapp.com/send?phone=6281370903694&text=Hallo%20Customer%20Services%20konterin&source=&data=&app_absent="
                                    target="_blank" class="rounded"><i data-feather="message-circle"
                                        class="fea icon-sm fea-social"></i></a></li>
                        </ul>
                    <!--end icon-->
                </div>
                <!--end col-->

                <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    <h4 class="text-light footer-head">Company</h4>
                    <ul class="list-unstyled footer-list mt-4">
                        <li><a href="https://konterin.com/page/tentang-kami/" class=" text-foot"><i class="mdi mdi-chevron-right mr-1"></i>
                                Tentang Kami</a></li>
                        <li><a href="https://konterin.com/page/list-harga/" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>
                                List Harga</a></li>
                        <li><a href="/auth/forgot-password" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>
                                Forgot Password</a></li>
                        <li><a href="/auth/register" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>
                                Register</a></li>
                    </ul>
                </div>
                <!--end col-->

               
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </footer>
    <!--end footer-->
    <footer class="footer footer-bar">
        <div class="container text-center">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="text-sm-left">
                        <p class="mb-0">© 2020 <?php echo $data['short_title']; ?>. 
                        </p>
                    </div>
                </div>
                <!--end col-->

                <div class="col-sm-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    <ul class="list-unstyled payment-cards text-sm-right mb-0">
                        <li class="list-inline-item"><a href="javascript:void(0)"><img class="avatar avatar-ex-sm"
                                    src="<?php echo $config['web']['url'] ?>assets/images/payments/indopayment/Logo-BCA.png" title="BCA"></a>
                        </li>
                      
                        <li class="list-inline-item"><a href="javascript:void(0)"><img class="avatar avatar-ex-sm"
                                    src="<?php echo $config['web']['url'] ?>assets/images/payments/indopayment/Logo-GOPAY.png" title="GOPAY"></a>
                        </li>
                        <li class="list-inline-item"><a href="javascript:void(0)"><img class="avatar avatar-ex-sm"
                                    src="<?php echo $config['web']['url'] ?>assets/images/payments/indopayment/Logo-OVO.png" title="OVO"></a></li>
                        <li class="list-inline-item"><a href="javascript:void(0)"><img class="avatar avatar-ex-sm"
                                    src="<?php echo $config['web']['url'] ?>assets/images/payments/indopayment/dana.png" title="Shoope Pay"></a>
                        </li>
                    </ul>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </footer>
    <!--end footer-->
    <!-- Footer End -->

    <!-- Back to top -->
    <a href="#" class="back-to-top rounded text-center" id="back-to-top">
        <i data-feather="chevron-up" class="icons d-inline-block"></i>
    </a>
    <!-- Back to top -->

    <!-- javascript -->
    <script src="<?php echo $config['web']['url'] ?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo $config['web']['url'] ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo $config['web']['url'] ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo $config['web']['url'] ?>assets/js/scrollspy.min.js"></script>
    <!-- Magnific Popup -->
    <script src="<?php echo $config['web']['url'] ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $config['web']['url'] ?>assets/js/magnific.init.js"></script>
    <!-- SLIDER -->
    <script src="<?php echo $config['web']['url'] ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo $config['web']['url'] ?>assets/js/owl.init.js"></script>
    <!-- Icons -->
    <script src="<?php echo $config['web']['url'] ?>assets/js/feather.min.js"></script>
    <script src="<?php echo $config['web']['url'] ?>assets/js/unicons-monochrome.js"></script>
    <script src="<?php echo $config['web']['url'] ?>assets/js/bundle.js"></script>
    <!-- Switcher -->
    <script src="<?php echo $config['web']['url'] ?>assets/js/switcher.js"></script>
    <!-- Main Js -->
    <script src="<?php echo $config['web']['url'] ?>assets/js/app.js"></script>
</body>

</html>