<?php

require '../config.php';
require '../lib/header.php';
?>

     <div class="mobile-menu-overlay"></div>

	<div class="main-container">
	
		<div class="pd-ltr-20">
		
			<div class="card-box mb-30" id="status">
				<h2 class="h4 pd-20 text-left"><i class="fa fa-code fa=lg text-primary"></i> Api Documentation</h2>
								<hr>
				<div class="card-body">
				        <div class="table-responsive">
				            <table class="table table-bordered">
					            <tbody>
						            <tr>
							            <td>HTTP Method</td>
							            <td>POST</td>
						            </tr>
						            <tr>
							            <td>API URL</td>
							            <td><?php echo $config['web']['url'] ?>api/</td>
						            </tr>
						            <tr>
							            <td>Format Respons</td>
							            <td>JSON</td>
						            </tr>
						            <tr>
							            <td>Api Kategori</td>
							            <td><form class="form-horizontal" role="form" method="POST">
									            <div class="form-group">
										            <div class="col-md-8 pull-left">
											            <select class="form-control" id="api">
											            	<option value="0">Pilih salah satu...</option>
											            	<option value="api_top_up">Api Pembelian</option>
												            <option value="api_account_information">Api Informasi Akun</option>
											            </select>
										         
								            </form>
							            </td>
						            </tr>
					            </tbody>
				            </table>
				        </div>
				  
		    </div>
		
		<!-- End Page API Documentation -->

		</div>
		<!-- End Content -->

        <!-- Start Scrolltop -->
		
		<!-- End Scrolltop -->
<div class="card-box mb-30" >
	<div class="pd-ltr-20">
				<div class="card-body" id="fitur">
	</div>
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.js"></script>
		<script type="text/javascript">
	       var htmlobjek;
            $(document).ready(function(){
                $("#api").change(function(){
                    var api = $("#api").val();
                $.ajax({
                    url: '<?php echo $config['web']['url'] ?>ajax/api-include.php',
                    data: 'api='+api,
                    type: 'POST',
                    dataType: 'html',
                    success: function(msg){
                        $("#fitur").html(msg);
                    }
                });
            });
        });
		</script>
		
		<br />
		<br />

<?php
include("../lib/footer.php");
?>