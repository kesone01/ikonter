<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/header.php';

	if (isset($_GET['oid'])) {
		$kode_pesanan = filter($_GET['oid']);

		$cek_pesanan = $conn->query("SELECT * FROM pembelian_pulsa WHERE oid = '$kode_pesanan' AND user = '$sess_username'");
		$data_pesanan = mysqli_fetch_assoc($cek_pesanan);

		if ($data_pesanan['status'] == "Pending") {
			$label = "warning";
		} else if ($data_pesanan['status'] == "Processing") {
			$label = "primary";
		} else if ($data_pesanan['status'] == "Error") {
			$label = "danger";
		} else if ($data_pesanan['status'] == "Partial") {
			$label = "danger";
		} else if ($data_pesanan['status'] == "Success") {
			$label = "success";
		}

		if ($cek_pesanan->num_rows == 0) {
			header("Location: ".$config['web']['url']."/riwayat-akun/");
		} else {
?>

      
     <div class="container-fluid">        
                    <!-- Content Row -->
                    <div class="row">
       <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">STRUK PEMBELIAN #<?php echo $data_pesanan['oid']; ?></h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
				<div class="invoice-wrap">
					<div class="invoice-box">
						<div class="invoice-header">
							<div class="logo text-center">
								<img src="/image/logo/logo.png" width="35%"alt="">
							</div>
						</div>
						<h4 class="text-center mb-30 weight-600">
						<strong><font face="BatangChe"><?php echo tanggal_indo($data_pesanan['date']); ?>, <?php echo $data_pesanan['time']; ?></font></strong></h4>
						
                         <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="kt_table_1">
                            <thead>
                                <tr >
								<td colspan="2" class="text-center">
					            <strong><font face="BatangChe"><h6><font face="Arial">Keterangan/SN: <b><?php echo $data_pesanan['keterangan']; ?></b></font></h6></font></strong>
		</td>				
		</tr>
		
						            <tr>
							            <td>PRODUK</td>
							            <td><?php echo $data_pesanan['layanan']; ?></td>
						            </tr>
						            <tr>
							            <td>TUJUAN</td>
							            <td><?php echo $data_pesanan['target']; ?></td>
						            </tr>
			
                                    <tr>
                                      <td>HARGA</td>	
                                    <td>Rp <?php echo number_format($data_pesanan['harga'],0,',','.'); ?>,-</td>
                                     </tr>
                              <tr>
							            <td>STATUS</td>
							            <td><label class="btn btn-<?php echo $label; ?> btn-elevate btn-pill btn-elevate-air btn-sm"><?php echo $data_pesanan['status']; ?></label></td>
						            </tr>

                                     <tr >
								<td colspan="2" class="text-center">
		    <h4><font face="Stencil">Terima Kasih</font></h4>
                                       </td>				
		</tr>
					            </thead>
				            </table>
                          </div>
                     

					</div>
					<div class="card-footer text-muted">
						<a href="<?php echo $config['web']['url']; ?>/riwayat-akun/" class="btn btn-warning btn-elevate btn-pill btn-elevate-air">Kembali</a>
						<a class="pull-right btn btn-primary btn-elevate btn-pill btn-elevate-air" href="#" onClick="window.print();">Print</a>
					</div>
				</div>
			</div>
		</div>
        <!-- End Page Order Struk -->
        
        </div></div>
        <!-- End Content -->


        <!-- Start Scrolltop -->

		<!-- End Scrolltop -->

<?php ?>

<?php 
require '../lib/footer.php';
}
} else {
	header("Location: ".$config['web']['url']."/riwayat-akun/");
}
?>