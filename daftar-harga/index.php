<?php
require '../config.php';
require '../lib/database.php';
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $data['title']; ?></title>
        <meta name="description" content="<?php echo $data['short_title']; ?>"/>
        <meta name="keywords"
              content="<?php echo $data['short_title']; ?>, smm ppob, ppob termurah, game online terlengkap"/>
        <meta name="author" content="Konterin"/>
        <!-- Site favicon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/faviconn.png"/>
        <!-- Custom fonts for this template-->
        <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
                href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
                rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="/css/sb-admin-2.min.css" rel="stylesheet">

    </head>

<body id="page-top">

    <!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-white sidebar sidebar-primary accordion toggled">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center">
            <div class="sidebar-brand-icon text-gray-200 rounded">
                <img width="100%" src="/image/logo/logo.png">
            </div>

        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/">
                <i class="fas fa-home"></i>
                <span>Home</span></a>
        </li>

        <!-- Divider -->

        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/status">
                <i class="fas fa-sync"></i>
                <span>Status Transaksi</span></a>
        </li>

        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/cara-transaksi">
                <i class="fas fa-shopping-basket"></i>
                <span>Cara Transaksi</span></a>
        </li>
        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/daftar-harga">
                <i class="fas fa-list"></i>
                <span>Daftar Harga</span></a>
        </li>

        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/kontak-kami">
                <i class="fas fa-envelope"></i>
                <span>Kontak Kami</span></a>
        </li>
        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/api">
                <i class="fas fa-fw fa-code"></i>
                <span>API Documentation</span></a>
        </li>


        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
        </button>
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">
    </nav>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-lg-7">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-list"></i> DAFTAR HARGA</h6>
                    </div>
                    <!-- Card Body-->
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="profile-tab-fill" data-toggle="tab" href="#pulsa"
                                   role="tab"
                                   aria-controls="pulsa" aria-selected="true">Pulsa &amp; PPOB</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="home-tab-fill" data-toggle="tab" href="#games" role="tab"
                                   aria-controls="games"
                                   aria-selected="false">Games &amp; Lainnya</a>
                            </li>
<!--                            <li class="nav-item">-->
<!--                                <a class="nav-link" id="home-tab-fill" data-toggle="tab" href="#sosmed" role="tab"-->
<!--                                   aria-controls="sosmed" aria-selected="false">Sosmed</a>-->
<!--                            </li>-->
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane show active" id="pulsa">
                                <form role="form" method="POST">
                                    <input type="hidden" id="subpulsa" name="subpulsa" value="true">
                                    <div class="row">
                                        <div class="form-group col-12 col-md-6" id="mpulsa1">
                                            <select class="form-control" id="tipe" name="tipe">
                                                <option value="0">Pilih Salah Satu</option>
                                                <option value="Pulsa">Pulsa</option>
                                                <option value="E-Money">E-Money</option>
                                                <option value="Data">Data</option>
                                                <option value="Paket SMS Telpon">Paket SMS Telpon</option>
                                                <option value="Games">Games</option>
                                                <option value="PLN">PLN</option>
                                                <option value="Pulsa Internasional">Pulsa Internasional</option>
                                                <option value="Voucher">Voucher</option>
                                                <option value="WIFI ID">WIFI ID</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-12 col-md-6" id="mpulsa2">
                                            <select class="form-control" id="operator" name="operator">
                                                <option value="0" disabled="" selected="">- Select One -</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-12 col-md-4 d-none" id="mpulsa3">
                                            <select class="form-control" id="operator-type" name="operator-type">
                                                <option value="0">Pilih Tipe Dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <div id="layanan"></div>
                            </div>
                            <div class="tab-pane" id="games">
                                <form role="form" method="POST">
                                    <div class="row">
                                        <div class="form-group col-12 col-md-12">
                                            <select class="form-control" id="operator-games" name="operator-games">
                                                <option value="<?php echo $data_layanan['kategori']; ?>"><?php echo $data_layanan['kategori']; ?> Pilih Dahulu</option>
                                                <?php
                                                $cek_kategori = $conn->query("SELECT * FROM kategori_layanan WHERE server = 'Games' ORDER BY nama ASC");
                                                while ($data_kategori = $cek_kategori->fetch_assoc()) {
                                                    ?>
                                                    <option value="<?php echo $data_kategori['kode']; ?>"><?php echo $data_kategori['kode']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <div id="layanan-game"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tipe").change(function () {
                var tipe = $("#tipe").val();
                $.ajax({
                    url: '<?php echo $config['web']['url']; ?>ajax/type-top-up.php',
                    data: 'tipe=' + tipe,
                    type: 'POST',
                    dataType: 'html',
                    success: function (msg) {
                        if(tipe == 'Data') {
                            $('#mpulsa1').removeClass('col-md-6').addClass('col-md-4');
                            $('#mpulsa2').removeClass('col-md-6').addClass('col-md-4');
                            $('#mpulsa3').removeClass('d-none');
                        } else {
                            resetMPulsa();
                        }
                        $("#operator").html(msg);
                    }
                });
            });
            $("#operator").change(function () {
                var tipe = $("#tipe").val();
                var operator = $("#operator").val();
                if(tipe == 'Data'){
                    $.ajax({
                        url: '<?php echo $config['web']['url']; ?>ajax/top-up-data.php',
                        data: 'tipe=' + tipe + '&operator=' + operator,
                        type: 'POST',
                        dataType: 'html',
                        success: function (msg) {
                            if(tipe == 'Data') {
                                $("#operator-type").html(msg);
                            }else{
                                $("#layanan").html(msg);
                            }
                        }
                    });
                }else{
                    $.ajax({
                        url: '<?php echo $config['web']['url']; ?>ajax/service-list-top-up.php',
                        data: 'tipe=' + tipe + '&operator=' + operator,
                        type: 'POST',
                        dataType: 'html',
                        success: function (msg) {
                            if(tipe == 'Data') {
                                $("#operator-type").html(msg);
                            }else{
                                $("#layanan").html(msg);
                            }
                        }
                    });
                }
            });
            $("#operator-type").change(function () {
                var operator = $("#operator").val();
                var operatorType = $("#operator-type").val();
                if(operator == 'TELKOMSEL'){
                    $.ajax({
                        url: '<?php echo $config['web']['url']; ?>ajax/telkomsel.php',
                        data: 'operator=' + operator + '&operatorType=' + operatorType,
                        type: 'POST',
                        dataType: 'html',
                        success: function (msg) {
                            $("#layanan").html(msg);
                        }
                    });
                }
            });
            $("#operator-games").change(function () {
                var operator = $("#operator-games").val();
                $.ajax({
                    url: '<?php echo $config['web']['url']; ?>ajax/service-list-top-up.php',
                    data: 'tipe=Games&operator=' + operator,
                    type: 'POST',
                    dataType: 'html',
                    success: function (msg) {
                        $("#layanan-game").html(msg);
                    }
                });
            });
            function resetMPulsa() {
                $('#mpulsa1').removeClass(['col-md-4','col-md-6']).addClass('col-md-6');
                $('#mpulsa2').removeClass(['col-md-4','col-md-6']).addClass('col-md-6');
                $('#mpulsa3').addClass('d-none');
            }
        });
    </script>
    <br/>
    <br/>

<?php
include("../lib/footer.php");
?>