<?php
<?php
require '../config.php';
require '../lib/database.php';
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $data['title']; ?></title>
        <meta name="description" content="<?php echo $data['short_title']; ?>"/>
        <meta name="keywords"
              content="<?php echo $data['short_title']; ?>, smm ppob, ppob termurah, game online terlengkap"/>
        <meta name="author" content="Konterin"/>
        <!-- Site favicon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/faviconn.png"/>
        <!-- Custom fonts for this template-->
        <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="/css/sb-admin-2.min.css" rel="stylesheet">

    </head>

<body id="page-top">

    <!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-white sidebar sidebar-primary accordion toggled">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center">
            <div class="sidebar-brand-icon text-gray-200 rounded">
                <img width="100%" src="/image/logo/logo.png">
            </div>

        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/">
                <i class="fas fa-home"></i>
                <span>Home</span></a>
        </li>

        <!-- Divider -->

        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/status">
                <i class="fas fa-sync"></i>
                <span>Status Transaksi</span></a>
        </li>

        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/cara-transaksi">
                <i class="fas fa-shopping-basket"></i>
                <span>Cara Transaksi</span></a>
        </li>
        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/daftar-harga">
                <i class="fas fa-list"></i>
                <span>Daftar Harga</span></a>
        </li>

        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/kontak-kami">
                <i class="fas fa-envelope"></i>
                <span>Kontak Kami</span></a>
        </li>
        <hr class="sidebar-divider my-1">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/api">
                <i class="fas fa-fw fa-code"></i>
                <span>API Documentation</span></a>
        </li>


        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
        </button>
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">
    </nav>
    <!-- Begin Page Content -->
    <div class="container-fluid">
    <div class="row">
        <div class="col-xl-12 col-lg-7">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-list"></i> DAFTAR HARGA</h6>
                </div>
                <!-- Card Body-->
                <div class="card-body">
                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="profile-tab-fill" data-toggle="tab" href="#pulsa"
                               role="tab"
                               aria-controls="pulsa" aria-selected="true">Pulsa &amp; PPOB</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="home-tab-fill" data-toggle="tab" href="#games" role="tab"
                               aria-controls="games"
                               aria-selected="false">Games &amp; Lainnya</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="home-tab-fill" data-toggle="tab" href="#sosmed" role="tab"
                               aria-controls="sosmed" aria-selected="false">Sosmed</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane show active" id="pulsa">
                            <form role="form" method="POST">
                                <input type="hidden" id="subpulsa" name="subpulsa" value="true">
                                <div class="row">
                                    <div class="form-group col-12 col-md-4" id="mpulsa1">
                                        <select class="form-control" id="provpulsa" name="provpulsa">
                                            <option value="0" selected="" disabled="">- Pilih Satu -</option>
                                            <option value="pascabayar">Pascabayar</option>
                                            <option value="pulsa-reguler">Pulsa Reguler</option>
                                            <option value="pulsa-transfer">Pulsa Transfer</option>
                                            <option value="pulsa-internasional">Pulsa Internasional</option>
                                            <option value="paket-internet">Paket Internet</option>
                                            <option value="paket-telepon">Paket Telepon dan SMS</option>
                                            <option value="token-pln">Token Listrik (PLN)</option>
                                            <option value="saldo-emoney">Saldo E-Money</option>
                                            <option value="voucher-game">Voucher Game</option>
                                            <option value="streaming-tv">Streaming &amp; TV</option>
                                            <option value="paket-lainnya">Kategori Lainnya</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-12 col-md-4" id="mpulsa2">
                                        <select class="form-control" id="categorypulsa" name="categorypulsa">
                                            <option value="0" disabled="" selected="">- Select One -</option>
                                            <option value="AXIS">AXIS</option>
                                            <option value="INDOSAT">INDOSAT</option>
                                            <option value="SMART">SMART</option>
                                            <option value="TELKOMSEL">TELKOMSEL</option>
                                            <option value="TRI">TRI</option>
                                            <option value="XL">XL</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-12 col-md-4" id="mpulsa3">
                                        <select class="form-control" id="subcategorypulsa" name="subcategorypulsa">
                                            <option value="0" disabled="" selected="">- Select One -</option>
                                            <option value="QWx3YXlzT24=">AlwaysOn</option>
                                            <option value="SGFwcHk=">Happy</option>
                                            <option value="TWluaQ==">Mini</option>
                                            <option value="TWl4">Mix</option>
                                            <option value="VW11bQ==">Umum</option>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <!--                                    <div class="table-responsive">-->
                                <!--                                        <table class="table table-bordered table-striped zero-configuration mb-0">-->
                                <!--                                            <thead>-->
                                <!--                                            <tr>-->
                                <!--                                                <th class="text-center" rowspan="2" style="vertical-align:middle">ID-->
                                <!--                                                </th>-->
                                <!--                                                <th class="text-center" rowspan="2" style="vertical-align:middle">-->
                                <!--                                                    Produk-->
                                <!--                                                </th>-->
                                <!--                                                <th class="text-center" rowspan="2"-->
                                <!--                                                    style="vertical-align:middle;width:30%">Catatan-->
                                <!--                                                </th>-->
                                <!--                                                <th class="text-center" colspan="2" style="vertical-align:middle">-->
                                <!--                                                    Harga-->
                                <!--                                                </th>-->
                                <!--                                                <th class="text-center" rowspan="2" style="vertical-align:middle">-->
                                <!--                                                    Status-->
                                <!--                                                </th>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <th class="text-center">Member</th>-->
                                <!--                                                <th class="text-center">Reseller</th>-->
                                <!--                                            </tr>-->
                                <!--                                            </thead>-->
                                <!--                                            <tbody id="pricelist2">-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMS5</td>-->
                                <!--                                                <td><b>Tri Mix Small 5 GB 1 Hari</b></td>-->
                                <!--                                                <td><b>1 GB (24 jam) + 4 GB (12 malam - 9 pagi) semua jaringan, Masa-->
                                <!--                                                        Aktif 1 Hari</b>-->
                                <!--                                                </td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 5.080</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 5.055</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMS275</td>-->
                                <!--                                                <td><b>Tri Mix Small 2.75 GB 3 Hari</b></td>-->
                                <!--                                                <td><b>0.75 GB (semua jaringan) + 2 GB (jaringan 4G) , Masa Aktif 3-->
                                <!--                                                        Hari</b></td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 9.995</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 9.970</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMS3</td>-->
                                <!--                                                <td><b>Tri Mix Small 3 GB 3 Hari</b></td>-->
                                <!--                                                <td><b>3 GB semua jaringan 24 jam, Masa Aktif 3 Hari</b></td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 19.855</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 19.830</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMS375</td>-->
                                <!--                                                <td><b>Tri Mix Small 3.75 GB 14 Hari</b></td>-->
                                <!--                                                <td><b>1.75 GB (semua jaringan) + 2 GB (jaringan 4G), Masa Aktif 14-->
                                <!--                                                        Hari</b></td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 24.775</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 24.750</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMC2</td>-->
                                <!--                                                <td><b>Tri Mix Combo 2 GB + 20 Menit</b></td>-->
                                <!--                                                <td><b>Paket Kombo Tri 2GB + 20 Menit telpon ke semua operator. Masa-->
                                <!--                                                        aktif 30 hari.</b>-->
                                <!--                                                </td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 34.575</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 34.550</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMS10</td>-->
                                <!--                                                <td><b>Tri Mix Super 10 GB 30 Hari</b></td>-->
                                <!--                                                <td><b>2 GB (semua jaringan) + 8 GB (jaringan 4G), Masa Aktif 30-->
                                <!--                                                        Hari</b></td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 49.175</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 49.150</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMC32</td>-->
                                <!--                                                <td><b>Tri Mix Combo 32 GB + 30 Menit</b></td>-->
                                <!--                                                <td><b>2 GB (semua jaringan) + 30 GB (1 GB per hari di jaringan 4G) + 30-->
                                <!--                                                        menit ke semua-->
                                <!--                                                        operator, Masa Aktif 30 Hari</b></td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 58.912</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 58.887</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMS24</td>-->
                                <!--                                                <td><b>Tri Mix Super 24 GB 30 Hari</b></td>-->
                                <!--                                                <td><b>24GB (4GB + 20GB 4G )</b></td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 88.199</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 88.174</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMS30</td>-->
                                <!--                                                <td><b>Tri Mix Super 30 GB 30 Hari</b></td>-->
                                <!--                                                <td><b>30GB (8GB + 22GB 4G)</b></td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 98.175</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 98.150</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            <tr>-->
                                <!--                                                <td>TDMC38</td>-->
                                <!--                                                <td><b>Tri Mix Combo 38 GB + 30 Menit</b></td>-->
                                <!--                                                <td><b>8 GB (semua jaringan) + 30 GB (1 GB per hari di jaringan 4G) + 30-->
                                <!--                                                        menit ke semua-->
                                <!--                                                        operator, Masa Aktif 30 Hari</b></td>-->
                                <!--                                                <td><span class="badge badge-primary"><b>Rp 123.175</b></span></td>-->
                                <!--                                                <td><span class="badge badge-success"><b>Rp 123.150</b></span></td>-->
                                <!--                                                <td align="center"><font color="green"><i-->
                                <!--                                                                class="far fa-check-circle mr-1"></i>Tersedia</font>-->
                                <!--                                                </td>-->
                                <!--                                            </tr>-->
                                <!--                                            </tbody>-->
                                <!--                                        </table>-->
                                <!--                                    </div>-->
                            </form>
                        </div>
<!--                        Before-->
                        <form class="form-horizontal" role="form" method="POST">
                            <div class="form-group">
                                <label>Tipe</label>
                                <select class="form-control" id="tipe" name="tipe">
                                    <option value="0">Pilih Salah Satu</option>
                                    <option value="Pulsa">Pulsa</option>
                                    <option value="E-Money">E-Money</option>
                                    <option value="Data">Data</option>
                                    <option value="Paket SMS Telpon">Paket SMS Telpon</option>
                                    <option value="Games">Games</option>
                                    <option value="PLN">PLN</option>
                                    <option value="Pulsa Internasional">Pulsa Internasional</option>
                                    <option value="Voucher">Voucher</option>
                                    <option value="WIFI ID">WIFI ID</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Kategori</label>
                                <select class="form-control" id="operator" name="operator">
                                    <option value="0">Pilih Tipe Dahulu</option>
                                </select>
                            </div>
                        </form>
                        <div id="layanan"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <div class="card-body card-dashboard">-->
    <!--        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">-->
    <!--            <li class="nav-item">-->
    <!--                <a class="nav-link active" id="profile-tab-fill" data-toggle="tab" href="#pulsa" role="tab"-->
    <!--                   aria-controls="pulsa" aria-selected="true">Pulsa &amp; PPOB</a>-->
    <!--            </li>-->
    <!--            <li class="nav-item">-->
    <!--                <a class="nav-link" id="home-tab-fill" data-toggle="tab" href="#games" role="tab" aria-controls="games"-->
    <!--                   aria-selected="false">Games &amp; Lainnya</a>-->
    <!--            </li>-->
    <!--            <li class="nav-item">-->
    <!--                <a class="nav-link" id="home-tab-fill" data-toggle="tab" href="#sosmed" role="tab"-->
    <!--                   aria-controls="sosmed" aria-selected="false">Sosmed</a>-->
    <!--            </li>-->
    <!--        </ul>-->
    <!--        <div class="tab-content">-->
    <!--            <div class="tab-pane show active" id="pulsa">-->
    <!--                <form role="form" method="POST">-->
    <!--                    <input type="hidden" id="subpulsa" name="subpulsa" value="true">-->
    <!--                    <div class="row">-->
    <!--                        <div class="form-group col-12 col-md-4" id="mpulsa1">-->
    <!--                            <select class="form-control" id="provpulsa" name="provpulsa">-->
    <!--                                <option value="0" selected="" disabled="">- Pilih Satu -</option>-->
    <!--                                <option value="pascabayar">Pascabayar</option>-->
    <!--                                <option value="pulsa-reguler">Pulsa Reguler</option>-->
    <!--                                <option value="pulsa-transfer">Pulsa Transfer</option>-->
    <!--                                <option value="pulsa-internasional">Pulsa Internasional</option>-->
    <!--                                <option value="paket-internet">Paket Internet</option>-->
    <!--                                <option value="paket-telepon">Paket Telepon dan SMS</option>-->
    <!--                                <option value="token-pln">Token Listrik (PLN)</option>-->
    <!--                                <option value="saldo-emoney">Saldo E-Money</option>-->
    <!--                                <option value="voucher-game">Voucher Game</option>-->
    <!--                                <option value="streaming-tv">Streaming &amp; TV</option>-->
    <!--                                <option value="paket-lainnya">Kategori Lainnya</option>-->
    <!--                            </select>-->
    <!--                        </div>-->
    <!--                        <div class="form-group col-12 col-md-4" id="mpulsa2">-->
    <!--                            <select class="form-control" id="categorypulsa" name="categorypulsa">-->
    <!--                                <option value="0" disabled="" selected="">- Select One -</option>-->
    <!--                                <option value="AXIS">AXIS</option>-->
    <!--                                <option value="INDOSAT">INDOSAT</option>-->
    <!--                                <option value="SMART">SMART</option>-->
    <!--                                <option value="TELKOMSEL">TELKOMSEL</option>-->
    <!--                                <option value="TRI">TRI</option>-->
    <!--                                <option value="XL">XL</option>-->
    <!--                            </select>-->
    <!--                        </div>-->
    <!--                        <div class="form-group col-12 col-md-4" id="mpulsa3">-->
    <!--                            <select class="form-control" id="subcategorypulsa" name="subcategorypulsa">-->
    <!--                                <option value="0" disabled="" selected="">- Select One -</option>-->
    <!--                                <option value="QWx3YXlzT24=">AlwaysOn</option>-->
    <!--                                <option value="SGFwcHk=">Happy</option>-->
    <!--                                <option value="TWluaQ==">Mini</option>-->
    <!--                                <option value="TWl4">Mix</option>-->
    <!--                                <option value="VW11bQ==">Umum</option>-->
    <!--                            </select>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <hr>-->
    <!--                    <div class="table-responsive">-->
    <!--                        <table class="table table-bordered table-striped zero-configuration mb-0">-->
    <!--                            <thead>-->
    <!--                            <tr>-->
    <!--                                <th class="text-center" rowspan="2" style="vertical-align:middle">ID</th>-->
    <!--                                <th class="text-center" rowspan="2" style="vertical-align:middle">Produk</th>-->
    <!--                                <th class="text-center" rowspan="2" style="vertical-align:middle;width:30%">Catatan</th>-->
    <!--                                <th class="text-center" colspan="2" style="vertical-align:middle">Harga</th>-->
    <!--                                <th class="text-center" rowspan="2" style="vertical-align:middle">Status</th>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <th class="text-center">Member</th>-->
    <!--                                <th class="text-center">Reseller</th>-->
    <!--                            </tr>-->
    <!--                            </thead>-->
    <!--                            <tbody id="pricelist2">-->
    <!--                            <tr>-->
    <!--                                <td>TDMS5</td>-->
    <!--                                <td><b>Tri Mix Small 5 GB 1 Hari</b></td>-->
    <!--                                <td><b>1 GB (24 jam) + 4 GB (12 malam - 9 pagi) semua jaringan, Masa Aktif 1 Hari</b>-->
    <!--                                </td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 5.080</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 5.055</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>TDMS275</td>-->
    <!--                                <td><b>Tri Mix Small 2.75 GB 3 Hari</b></td>-->
    <!--                                <td><b>0.75 GB (semua jaringan) + 2 GB (jaringan 4G) , Masa Aktif 3 Hari</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 9.995</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 9.970</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>TDMS3</td>-->
    <!--                                <td><b>Tri Mix Small 3 GB 3 Hari</b></td>-->
    <!--                                <td><b>3 GB semua jaringan 24 jam, Masa Aktif 3 Hari</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 19.855</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 19.830</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>TDMS375</td>-->
    <!--                                <td><b>Tri Mix Small 3.75 GB 14 Hari</b></td>-->
    <!--                                <td><b>1.75 GB (semua jaringan) + 2 GB (jaringan 4G), Masa Aktif 14 Hari</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 24.775</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 24.750</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>TDMC2</td>-->
    <!--                                <td><b>Tri Mix Combo 2 GB + 20 Menit</b></td>-->
    <!--                                <td><b>Paket Kombo Tri 2GB + 20 Menit telpon ke semua operator. Masa aktif 30 hari.</b>-->
    <!--                                </td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 34.575</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 34.550</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>TDMS10</td>-->
    <!--                                <td><b>Tri Mix Super 10 GB 30 Hari</b></td>-->
    <!--                                <td><b>2 GB (semua jaringan) + 8 GB (jaringan 4G), Masa Aktif 30 Hari</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 49.175</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 49.150</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>TDMC32</td>-->
    <!--                                <td><b>Tri Mix Combo 32 GB + 30 Menit</b></td>-->
    <!--                                <td><b>2 GB (semua jaringan) + 30 GB (1 GB per hari di jaringan 4G) + 30 menit ke semua-->
    <!--                                        operator, Masa Aktif 30 Hari</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 58.912</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 58.887</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>TDMS24</td>-->
    <!--                                <td><b>Tri Mix Super 24 GB 30 Hari</b></td>-->
    <!--                                <td><b>24GB (4GB + 20GB 4G )</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 88.199</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 88.174</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>TDMS30</td>-->
    <!--                                <td><b>Tri Mix Super 30 GB 30 Hari</b></td>-->
    <!--                                <td><b>30GB (8GB + 22GB 4G)</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 98.175</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 98.150</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>TDMC38</td>-->
    <!--                                <td><b>Tri Mix Combo 38 GB + 30 Menit</b></td>-->
    <!--                                <td><b>8 GB (semua jaringan) + 30 GB (1 GB per hari di jaringan 4G) + 30 menit ke semua-->
    <!--                                        operator, Masa Aktif 30 Hari</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 123.175</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 123.150</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            </tbody>-->
    <!--                        </table>-->
    <!--                    </div>-->
    <!--                </form>-->
    <!--            </div>-->
    <!--         <div class="tab-pane" id="sosmed">-->
    <!--             <form role="form" method="POST">-->
    <!--                 <div class="row">-->
    <!--                     <div class="form-group col-12">-->
    <!--                         <select class="form-control" id="categorysosmed" name="categorysosmed">-->
    <!--                             <option value="0" selected="" disabled="">- Pilih Satu -</option>-->
    <!--                             <option value="Akun Premium Canva / Netflix / Spotify / Youtube [ PROMO ]">Akun Premium-->
    <!--                                 Canva / Netflix / Spotify / Youtube [ PROMO ]-->
    <!--                             </option>-->
    <!--                             <option value="♕ Instagram Followers/Likes Indonesia [ PROMO ]">♕ Instagram-->
    <!--                                 Followers/Likes Indonesia [ PROMO ]-->
    <!--                             </option>-->
    <!--                             <option value="♕ Instagram Promo Murah [ ON/OFF ]">♕ Instagram Promo Murah [ ON/OFF ]-->
    <!--                             </option>-->
    <!--                             <option value="♕ Shopee Produk Terjual [ Special ]">♕ Shopee Produk Terjual [ Special-->
    <!--                                 ]-->
    <!--                             </option>-->
    <!--                             <option value="Facebook Page Likes">Facebook Page Likes</option>-->
    <!--                             <option value="Facebook Post Likes">Facebook Post Likes</option>-->
    <!--                             <option value="Facebook Video Views / Stream">Facebook Video Views / Stream</option>-->
    <!--                             <option value="Instagram Comments [ Custom ]">Instagram Comments [ Custom ]</option>-->
    <!--                             <option value="Instagram Followers Asing S2 [ No Refill - Low Quality ]">Instagram-->
    <!--                                 Followers Asing S2 [ No Refill - Low Quality ]-->
    <!--                             </option>-->
    <!--                             <option value="Instagram Followers Asing [ Refill / Garansi ]">Instagram Followers Asing-->
    <!--                                 [ Refill / Garansi ]-->
    <!--                             </option>-->
    <!--                             <option value="Instagram Followers Indonesia">Instagram Followers Indonesia</option>-->
    <!--                             <option value="Instagram Followers Indonesia [ Refill / Garansi ]">Instagram Followers-->
    <!--                                 Indonesia [ Refill / Garansi ]-->
    <!--                             </option>-->
    <!--                             <option value="Instagram Likes Asing">Instagram Likes Asing</option>-->
    <!--                             <option value="Instagram Likes Indonesia">Instagram Likes Indonesia</option>-->
    <!--                             <option value="Instagram Reels [ Likes / Views ]">Instagram Reels [ Likes / Views ]-->
    <!--                             </option>-->
    <!--                             <option value="Instagram Reels [ LIKES/VIEWS ]">Instagram Reels [ LIKES/VIEWS ]</option>-->
    <!--                             <option value="Instagram Story / Impressions / Saves">Instagram Story / Impressions /-->
    <!--                                 Saves-->
    <!--                             </option>-->
    <!--                             <option value="Instagram TV">Instagram TV</option>-->
    <!--                             <option value="Instagram Views / Profil Visit">Instagram Views / Profil Visit</option>-->
    <!--                             <option value="Shopee / Bukalapak / Tokopedia">Shopee / Bukalapak / Tokopedia</option>-->
    <!--                             <option value="TikTok Followers">TikTok Followers</option>-->
    <!--                             <option value="TikTok Likes">TikTok Likes</option>-->
    <!--                             <option value="TikTok Views">TikTok Views</option>-->
    <!--                             <option value="Twitter Followers">Twitter Followers</option>-->
    <!--                             <option value="Twitter Likes">Twitter Likes</option>-->
    <!--                             <option value="Youtube Channel Optimize [ Monetization ]">Youtube Channel Optimize [-->
    <!--                                 Monetization ]-->
    <!--                             </option>-->
    <!--                             <option value="Youtube Likes / Dislikes">Youtube Likes / Dislikes</option>-->
    <!--                             <option value="Youtube Views">Youtube Views</option>-->
    <!--                             <option value="Youtube Watch Time [ Jam Tayang ]">Youtube Watch Time [ Jam Tayang ]-->
    <!--                             </option>-->
    <!--                             <option value="Z - Test API">Z - Test API</option>-->
    <!--                         </select>-->
    <!--                     </div>-->
    <!--                 </div>-->
    <!--                 <hr>-->
    <!--                 <div class="table-responsive">-->
    <!--                     <table class="table table-bordered table-striped zero-configuration mb-0">-->
    <!--                         <thead>-->
    <!--                         <tr>-->
    <!--                             <th class="text-center" rowspan="2" style="vertical-align:middle">ID</th>-->
    <!--                             <th class="text-center" rowspan="2" style="vertical-align:middle">Produk</th>-->
    <!--                             <th class="text-center" rowspan="2" style="vertical-align:middle">Min</th>-->
    <!--                             <th class="text-center" rowspan="2" style="vertical-align:middle">Max</th>-->
    <!--                             <th class="text-center" colspan="2" style="vertical-align:middle">Harga/1000</th>-->
    <!--                             <th class="text-center" rowspan="2" style="vertical-align:middle">Status</th>-->
    <!--                         </tr>-->
    <!--                         <tr>-->
    <!--                             <th class="text-center">Member</th>-->
    <!--                             <th class="text-center">Reseller</th>-->
    <!--                         </tr>-->
    <!--                         </thead>-->
    <!--                         <tbody id="pricelist1">-->
    <!--                         <tr>-->
    <!--                             <td colspan="6" class="text-center">Silakan pilih kategori.</td>-->
    <!--                             <td class="text-center text-danger"><i class="far fa-times-circle"></i></td>-->
    <!--                         </tr>-->
    <!--                         </tbody>-->
    <!--                     </table>-->
    <!--                 </div>-->
    <!--             </form>-->
    <!--         </div>-->
    <!--            <div class="tab-pane" id="games">-->
    <!--                <form role="form" method="POST">-->
    <!--                    <div class="row">-->
    <!--                        <div class="form-group col-12">-->
    <!--                            <select class="form-control" id="categorygames" name="categorygames">-->
    <!--                                <option value="0" selected="" disabled="">- Pilih Satu -</option>-->
    <!--                                <option value="Call of Duty Mobile">Call of Duty Mobile</option>-->
    <!--                                <option value="Disney Hotstar">Disney Hotstar</option>-->
    <!--                                <option value="Free Fire">Free Fire</option>-->
    <!--                                <option value="Genshin Impact">Genshin Impact</option>-->
    <!--                                <option value="Lords Mobile">Lords Mobile</option>-->
    <!--                                <option value="Mobile Legends A">Mobile Legends A</option>-->
    <!--                                <option value="Mobile Legends B">Mobile Legends B</option>-->
    <!--                                <option value="Mobile Legends Gift">Mobile Legends Gift</option>-->
    <!--                                <option value="Mobile Legends Slow">Mobile Legends Slow</option>-->
    <!--                                <option value="Mobile Legends Starlight">Mobile Legends Starlight</option>-->
    <!--                                <option value="Netflix Premium">Netflix Premium</option>-->
    <!--                                <option value="One Punch Man">One Punch Man</option>-->
    <!--                                <option value="PB Zepetto">PB Zepetto</option>-->
    <!--                                <option value="PUBGM GLOBAL">PUBGM GLOBAL</option>-->
    <!--                                <option value="PUBGM INDO A">PUBGM INDO A</option>-->
    <!--                                <option value="PUBGM INDO B">PUBGM INDO B</option>-->
    <!--                                <option value="RagnaroK X Next Generation">RagnaroK X Next Generation</option>-->
    <!--                                <option value="Ragnarok X Razer Link">Ragnarok X Razer Link</option>-->
    <!--                                <option value="Sausage Man">Sausage Man</option>-->
    <!--                                <option value="Spotify Premium">Spotify Premium</option>-->
    <!--                                <option value="Valorant">Valorant</option>-->
    <!--                                <option value="Youtube Premium">Youtube Premium</option>-->
    <!--                                <option value="Zepeto">Zepeto</option>-->
    <!--                            </select>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <hr>-->
    <!--                    <div class="table-responsive">-->
    <!--                        <table class="table table-bordered table-striped zero-configuration mb-0">-->
    <!--                            <thead>-->
    <!--                            <tr>-->
    <!--                                <th class="text-center" rowspan="2" style="vertical-align:middle">ID</th>-->
    <!--                                <th class="text-center" rowspan="2" style="vertical-align:middle">Produk</th>-->
    <!--                                <th class="text-center" colspan="3" style="vertical-align:middle">Harga</th>-->
    <!--                                <th class="text-center" rowspan="2" style="vertical-align:middle">Status</th>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <th class="text-center">Member</th>-->
    <!--                                <th class="text-center">Reseller</th>-->
    <!--                                <th class="text-center">H2H</th>-->
    <!--                            </tr>-->
    <!--                            </thead>-->
    <!--                            <tbody id="pricelist3">-->
    <!--                            <tr>-->
    <!--                                <td>ROXTRIALCARD-S10</td>-->
    <!--                                <td><b>Kafra Trial Card</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 62.220</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 61.915</b></span></td>-->
    <!--                                <td><span class="badge badge-info"><b>Rp 61.610</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>ROXRZR2580-S10</td>-->
    <!--                                <td><b>2580 Diamonds</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 63.090</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 62.719</b></span></td>-->
    <!--                                <td><span class="badge badge-info"><b>Rp 62.596</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>ROXRZR5150-S10</td>-->
    <!--                                <td><b>5150 Diamonds</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 126.050</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 125.438</b></span></td>-->
    <!--                                <td><span class="badge badge-info"><b>Rp 125.193</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>ROXVIPCARD-S10</td>-->
    <!--                                <td><b>Kafras VIP Card</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 183.600</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 182.700</b></span></td>-->
    <!--                                <td><span class="badge badge-info"><b>Rp 198.000</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>ROXRZR8600-S10</td>-->
    <!--                                <td><b>8600 Diamonds</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 209.983</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 208.963</b></span></td>-->
    <!--                                <td><span class="badge badge-info"><b>Rp 208.554</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>ROXRZR17200-S10</td>-->
    <!--                                <td><b>17200 Diamonds</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 416.567</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 414.525</b></span></td>-->
    <!--                                <td><span class="badge badge-info"><b>Rp 413.708</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>ROXRZR55650-S10</td>-->
    <!--                                <td><b>55650 Diamonds</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 1.244.500</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 1.238.375</b></span></td>-->
    <!--                                <td><span class="badge badge-info"><b>Rp 1.235.925</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>ROXRZR104300-S10</td>-->
    <!--                                <td><b>104300 Diamonds</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 2.447.884</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 2.435.838</b></span></td>-->
    <!--                                <td><span class="badge badge-info"><b>Rp 2.431.020</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            <tr>-->
    <!--                                <td>ROXRZR185760-S10</td>-->
    <!--                                <td><b>185760 Diamonds</b></td>-->
    <!--                                <td><span class="badge badge-primary"><b>Rp 4.925.252</b></span></td>-->
    <!--                                <td><span class="badge badge-success"><b>Rp 4.903.814</b></span></td>-->
    <!--                                <td><span class="badge badge-info"><b>Rp 4.895.239</b></span></td>-->
    <!--                                <td align="center"><font color="green"><i class="far fa-check-circle mr-1"></i>Tersedia</font>-->
    <!--                                </td>-->
    <!--                            </tr>-->
    <!--                            </tbody>-->
    <!--                        </table>-->
    <!--                    </div>-->
    <!--                </form>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!-- End Content -->

    <!-- Start Scrolltop -->

    <!-- End Scrolltop -->

    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tipe").change(function () {
                var tipe = $("#tipe").val();
                $.ajax({
                    url: '<?php echo $config['web']['url']; ?>ajax/type-top-up.php',
                    data: 'tipe=' + tipe,
                    type: 'POST',
                    dataType: 'html',
                    success: function (msg) {
                        $("#operator").html(msg);
                    }
                });
            });
            $("#operator").change(function () {
                var tipe = $("#tipe").val();
                var operator = $("#operator").val();
                $.ajax({
                    url: '<?php echo $config['web']['url']; ?>ajax/service-list-top-up.php',
                    data: 'tipe=' + tipe + '&operator=' + operator,
                    type: 'POST',
                    dataType: 'html',
                    success: function (msg) {
                        $("#layanan").html(msg);
                    }
                });
            });
        });
    </script>

    <br/>
    <br/>

<?php
include("../lib/footer.php");
?>