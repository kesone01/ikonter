<?php
require("../config.php");
//require("../repository/PulsRepository.php");
if (isset($_POST['operator'])) {
    $operator = $conn->real_escape_string($_POST['operator']);
    $layanan = $conn->real_escape_string($_POST['operatorType']);
    $cek_layanan = $conn->query("SELECT * FROM layanan_pulsa WHERE layanan LIKE '%$layanan%' AND operator ='$operator' ORDER BY service_id ASC");
    if (mysqli_num_rows($cek_layanan) != 0) {
        ?>
        <div class="table-responsive">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>ID PRODUK</th>
                    <th>KATEGORI</th>
                    <th>NAMA PRODUK</th>
                    <th>HARGA PERSONAL</th>
                    <th>HARGA API</th>
                    <th>TIPE</th>
                    <th>STATUS</th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($data_layanan = mysqli_fetch_assoc($cek_layanan)) {
                    if($data_layanan['status'] == "Normal") {
                        $label = "success";
                    } else if($data_layanan['status'] == "Gangguan") {
                        $label = "danger";
                    }
                    ?>
                    <tr>
                        <th scope="row"><?php echo $data_layanan['service_id']; ?></th>
                        <td><?php echo $data_layanan['operator']; ?></td>
                        <td><?php echo $data_layanan['layanan']; ?></td>
                        <td>Rp <?php echo number_format($data_layanan['harga'],0,',','.'); ?></td>
                        <td>Rp <?php echo number_format($data_layanan['harga_api'],0,',','.'); ?></td>
                        <td><?php echo $data_layanan['tipe']; ?></td>
                        <td><label class="btn btn-sm btn-<?php echo $label; ?>"><?php echo $data_layanan['status']; ?></label></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <?php
    } else {
        ?>
        <div class="text-center">
            <div class="alert alert-primary">Maaf, Layanan Belum Tersedia</div>
        </div>
        <?php
    }
}