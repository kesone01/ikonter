<?php
session_start();
require '../config.php';
$tipe = "Daftar";
$secret_key = '6LeCxNMZAAAAAMl-aYNiwUqSeKHYNkOwCWLxYNjJ'; //masukkan secret key-nya berdasarkan secret key masing-masing saat create api key nya
$captcha = $_POST['g-recaptcha-response'];
$url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) . '&response=' . $captcha;
require '../lib/header_home.php';
?>

    <script type="text/javascript">
        $(document).ready(function () {
            //semua element dengan class text-danger akan di sembunyikan saat load
            $('.text-danger').hide();
            $('.text-info').hide();
            //untuk mengecek bahwa semua textbox tidak boleh kosong
            $('input').each(function () {
                $(this).blur(function () { //blur function itu dijalankan saat element kehilangan fokus
                    if (!$(this).val()) { //this mengacu pada text box yang sedang fokus
                        return get_error_text(this); //function get_error_text ada di bawah
                    } else {
                        $(this).removeClass('no-valid');
                        $(this).parent().find('.text-danger').hide();
                        $(this).parent().find('.text-info').hide();//cari element dengan class has-warning dari element induk text yang sedang focus
                        $(this).closest('div').removeClass('has-warning');
                        $(this).closest('div').addClass('has-success');
                        $(this).parent().find('.form-control-feedback').removeClass('glyphicon glyphicon-warning-sign');
                        $(this).parent().find('.form-control-feedback').addClass('glyphicon glyphicon-ok');
                    }
                });
            });

            //mengecek textbox Nama Valid Atau Tidak
            $('#nama').blur(function () {
                var nama = $(this).val();
                var len = nama.length;
                if (len > 0) { //jika ada isinya
                    if (!valid_nama(nama)) { //jika nama tidak valid
                        $(this).parent().find('.text-danger').text("");
                        $(this).parent().find('.text-danger').text("Nama Tidak Valid");
                        return apply_feedback_error(this);
                    } else {
                        if (len > 10) { //jika karakter >10
                            $(this).parent().find('.text-danger').text("");
                            $(this).parent().find('.text-danger').text("Maximal Karakter 10");
                            return apply_feedback_error(this);
                        }
                    }
                }
            });

            //mengecek text box username
            $('#username').blur(function () {
                var username = $(this).val();
                var len = username.length;
                if (len > 10) {
                    $(this).parent().find('.text-danger').text("");
                    $(this).parent().find('.text-danger').text("Maximal Karakter 10");
                    return apply_feedback_error(this);
                } else {
                    var valid = false;
                    $.ajax({
                        url: "../cek-register/cek-username.php",
                        type: "POST",
                        data: "username=" + username,
                        dataType: "text",
                        success: function (data) {
                            if (data == 0) { //pada file check username.php, apabila username sudah ada di database makan akan mengembalikan nilai 0
                                $('#username').parent().find('.text-danger').text("");
                                $('#username').parent().find('.text-danger').text("username sudah terdaftar.");
                                return apply_feedback_error('#username');
                            }else{
                                $(this).removeClass('no-valid');
                            }
                        }
                    });
                }
            });

            //mengecek text box email
            $('#email').blur(function () {
                var email = $(this).val();
                var len = email.length;
                if (len > 0) {
                    if (!valid_email(email)) {
                        $(this).parent().find('.text-danger').text("");
                        $(this).parent().find('.text-danger').text("E-mail Tidak Valid (ex: example@gmail.com)");
                        return apply_feedback_error(this);
                    } else {
                        if (len > 30) {
                            $(this).parent().find('.text-danger').text("");
                            $(this).parent().find('.text-danger').text("Maximal Karakter 30");
                            return apply_feedback_error(this);
                        } else {
                            var valid = false;
                            $.ajax({
                                url: "../cek-register/cek-email.php",
                                type: "POST",
                                data: "email=" + email,
                                dataType: "text",
                                success: function (data) {
                                    if (data == 0) { //pada file check email.php, apabila email sudah ada di database makan akan mengembalikan nilai 0
                                        $('#email').parent().find('.text-danger').text("");
                                        $('#email').parent().find('.text-danger').text("Email sudah terdaftar.");
                                        return apply_feedback_error('#email');
                                    }
                                }
                            });
                        }

                    }
                }
            });

            //mengecek password
            $('#password').blur(function () {
                var password = $(this).val();
                var len = password.length;
                if (len > 0 && len < 6) {
                    $(this).parent().find('.text-danger').text("");
                    $(this).parent().find('.text-danger').text("password minimal 6 karakter");
                    return apply_feedback_error(this);
                } else {
                    if (len > 35) {
                        $(this).parent().find('.text-danger').text("");
                        $(this).parent().find('.text-danger').text("password maximal 35 karakter");
                        return apply_feedback_error(this);
                    }
                }
            });

            //mengecek konfirmasi password
            $('#password2').blur(function () {
                var pass = $("#password").val();
                var conf = $(this).val();
                var len = conf.length;
                if (len > 0 && pass !== conf) {
                    $(this).parent().find('.text-danger').text("");
                    $(this).parent().find('.text-danger').text("Konfirmasi Password tidak sama.");
                    return apply_feedback_error(this);
                }
            });

            //mengecek nomer no_hp
            $('#no_hp').blur(function () {
                var no_hp = $(this).val();
                var len = no_hp.length;
                if (len > 0 && len <= 10) {
                    $(this).parent().find('.text-danger').text("");
                    $(this).parent().find('.text-danger').text("Nomer HP terlalu pendek.");
                    return apply_feedback_error(this);
                } else {
                    if (!valid_hp(no_hp)) {
                        $(this).parent().find('.text-danger').text("");
                        $(this).parent().find('.text-danger').text("Format nomer hp wajib 62 (ex: 6285600xxx245)");
                        return apply_feedback_error(this);
                    } else {
                        if (len > 15) {
                            $(this).parent().find('.text-danger').text("");
                            $(this).parent().find('.text-danger').text("Nomer HP terlalu Panjang.");
                            return apply_feedback_error(this);
                        } else {
                            var valid = false;
                            $.ajax({
                                url: "../cek-register/cek-hp.php",
                                type: "POST",
                                data: "no_hp=" + no_hp,
                                dataType: "text",
                                success: function (data) {
                                    if (data == 0) { //pada file check email.php, apabila email sudah ada di database makan akan mengembalikan nilai 0
                                        $('#no_hp').parent().find('.text-danger').text("");
                                        $('#no_hp').parent().find('.text-danger').text("Nomor Hp sudah terdaftar.");
                                        return apply_feedback_error('#no_hp');
                                    }
                                }
                            });
                        }
                    }
                }
            });

            //mengecek nomer pin
            $('#pin').blur(function () {
                var pin = $(this).val();
                var lenp = pin.length;
                if (lenp > 0 && lenp <= 5) {
                    $(this).parent().find('.text-danger').text("");
                    $(this).parent().find('.text-danger').text("PIN Transaksi Harus 6 Digit.");
                    return apply_feedback_error(this);
                } else {
                    if (!valid_pin(pin)) {
                        $(this).parent().find('.text-danger').text("");
                        $(this).parent().find('.text-danger').text("PIN Transaksi Harus 6 Digit.");
                        return apply_feedback_error(this);
                    } else {
                        if (lenp > 6) {
                            $(this).parent().find('.text-danger').text("");
                            $(this).parent().find('.text-danger').text("PIN Transaksi Harus 6 Digit.");
                            return apply_feedback_error(this);
                        }
                    }
                }
            });

            //mengecek nomer referral
            // $('#referral').blur(function () {
            //     var referral = $(this).val();
            //     var len = referral.length;
            //     if (len > 1 && len <= 4) {
            //         $(this).parent().find('.text-info').text("");
            //         $(this).parent().find('.text-info').text("Terlalu pendek.");
            //         return apply_feedback_error(this);
            //     } else if(len = 0){
            //         $(this).removeClass('no-valid');
            //     }else {
            //         if (len > 12) {
            //             $(this).parent().find('.text-info').text("");
            //             $(this).parent().find('.text-info').text("Kode Referral terlalu Panjang.");
            //             return apply_feedback_error(this);
            //         } else {
            //             var valid = false;
            //             $.ajax({
            //                 url: "../cek-register/cek-reff.php",
            //                 type: "POST",
            //                 data: "referral=" + referral,
            //                 dataType: "text",
            //                 success: function (data) {
            //                     if (data == 0) { //pada file check email.php, apabila email sudah ada di database makan akan mengembalikan nilai 0
            //                         $('#referral').parent().find('.text-info').text("");
            //                         $('#referral').parent().find('.text-info').text("Ditemukan");
            //                         return apply_feedback_error('#referral');
            //                     }
            //                     if (data == 1) { //pada file check email.php, apabila email sudah ada di database makan akan mengembalikan nilai 0
            //                         $('#referral').parent().find('.text-info').text("");
            //                         $('#referral').parent().find('.text-info').text("Kode Referral Tidak Ditemukan.");
            //                         return apply_feedback_error('#referral');
            //                     }
            //                 }
            //             });
            //         }
            //     }
            // });
            //submit form validasi
            // $('#formInput').submit(function (e) {
            //     e.preventDefault();
            //     var valid = true;
            //     $(this).find('.textbox').each(function () {
            //         if (!$(this).val()) {
            //             get_error_text(this);
            //             valid = false;
            //             $('html,body').animate({scrollTop: 0}, "slow");
            //         }
            //         if ($(this).hasClass('no-valid')) {
            //             valid = false;
            //             $('html,body').animate({scrollTop: 0}, "slow");
            //         }
            //     });
            //     if (valid) {
            //         swal({
            //             title: "Konfirmasi Akun",
            //             text: "Silahkan Klik Yakin",
            //             type: "info",
            //             showCancelButton: true,
            //             confirmButtonColor: "#1da1f2",
            //             confirmButtonText: "Yakinn!",
            //             closeOnConfirm: false,
            //             showLoaderOnConfirm: true,
            //         }, function () { //apabila sweet alert d confirm maka akan mengirim data ke simpan.php melalui proses ajax
            //             $.ajax({
            //                 url: "../cek-register/cek-register.php",
            //                 type: "POST",
            //                 data: $('#formInput').serialize(), //serialize() untuk mengambil semua data di dalam form
            //                 dataType: "html",
            //                 success: function () {
            //                     setTimeout(function () {
            //                         swal({
            //                             title: "Data Berhasil Disimpan",
            //                             text: "Terimakasih, Silahkan Login!",
            //                             type: "success"
            //                         }, function () {
            //                             window.location = "auth/login.php";
            //                         });
            //                     }, 2000);
            //                 },
            //                 error: function (xhr, ajaxOptions, thrownError) {
            //                     setTimeout(function () {
            //                         swal("Error", "Tolong Cek Koneksi Lalu Ulangi", "error");
            //                     }, 2000);
            //                 }
            //             });
            //         });
            //     }
            // });
        });

        //fungsi cek nama
        function valid_nama(nama) {
            var pola = new RegExp(/^[a-z A-Z]+$/);
            return pola.test(nama);
        }

        //fungsi cek tanggal lahir
        function valid_tanggal(tanggal) {
            var pola = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
            return pola.test(tanggal);
        }

        //fungsi cek email
        function valid_email(email) {
            var pola = new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
            return pola.test(email);
        }

        //fungsi cek phone
        function valid_hp(no_hp) {
            var pola = new RegExp(/628/);
            return pola.test(no_hp);
        }

        //fungsi cek pin
        function valid_pin(pin) {
            var pola = new RegExp(/^[0-9-+]+$/);
            return pola.test(pin);
        }

        //menerapkan gaya validasi form bootstrap saat terjadi eror
        function apply_feedback_error(textbox) {
            $(textbox).addClass('no-valid'); //menambah class no valid
            $(textbox).parent().find('.text-danger').show();
            $(textbox).parent().find('.text-info').show();
            $(textbox).closest('div').removeClass('has-success');
            $(textbox).closest('div').addClass('has-warning');
            $(textbox).parent().find('.form-control-feedback').removeClass('glyphicon glyphicon-ok');
            $(textbox).parent().find('.form-control-feedback').addClass('glyphicon glyphicon-warning-sign');
        }

        //untuk mendapat eror teks saat textbox kosong, digunakan saat submit form dan blur fungsi
        function get_error_text(textbox) {
            $(textbox).parent().find('.text-danger').text("");
            $(textbox).parent().find('.text-danger').text("*Tidak Boleh Kosong");
            $(textbox).parent().find('.text-info').text("");
            $(textbox).parent().find('.text-info').text("*Kosongkan Jika Tidak Ada.");
            return apply_feedback_error(textbox);
        }
    </script>

    <div class="login-2 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-section" style="background: #ffffff;">
                        <img src="/img/logo.png" style="width: 150px;">
                        <h3 style="color:#2f55d4;">Daftar Akun</h3>
                        <?php
                        if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible"
                                 role="alert">
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                        }
                        ?>
                        <div class="login-inner-form" class="form-horizontal" method="POST">
                            <form id="formInput">
                                <input type="hidden" id="csrf_token" name="csrf_token"
                                       value="<?php echo $config['csrf_token'] ?>">
                                <div class="row">
                                    <div class="form-group form-box col-md-6 col-12">
                                        <input id="nama" onchange="onChanges()" type="text" class="input-text textbox"
                                               placeholder="Nama Depan" name="nama_depan" required="">
                                        <i class="form-control-feedback"></i>
                                        <small class="text-danger font-13 pull-left"></small>
                                    </div>
                                    <div class="form-group form-box col-md-6 col-12">
                                        <input id="name" onchange="onChanges()" type="text" class="input-text textbox"
                                               placeholder="Nama Belakang" name="nama_belakang" required="">
                                        <i class="form-control-feedback"></i>
                                        <small class="text-danger font-13 pull-left"></small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group form-box col-md-6 col-12">
                                        <input id="email" onchange="onChanges()" type="email" class="input-text textbox"
                                               placeholder="Email Aktif" name="email" required="">
                                        <i class="form-control-feedback"></i>
                                        <small class="text-danger pull-left"></small>

                                    </div>
                                    <div class="form-group form-box col-md-6 col-12">
                                        <input id="no_hp" onchange="onChanges()" type="number"
                                               class="input-text textbox" placeholder="Nomor HP" name="no_hp"
                                               required="">
                                        <i class="form-control-feedback"></i>
                                        <small class="text-danger pull-left"></small>
                                    </div>
                                </div>
                                <div class="form-group form-box">
                                    <input id="username" type="text" class="input-text textbox" placeholder="Username"
                                           onchange="onChanges()" name="username" required="">
                                    <i class="flaticon-user"></i>
                                    <i class="form-control-feedback"></i>
                                    <small class="text-danger pull-left"></small>
                                </div>

                                <!-- Password -->

                                <div class="form-group form-box">
                                    <input id="password" onchange="onChanges()" type="password"
                                           class="active input-text input textbox" placeholder="Password"
                                           name="password" required="">
                                    <i id="icon" class="flaticon-password"></i>
                                    <i class="form-control-feedback"></i>
                                    <small class="text-danger pull-left"></small>
                                </div>

                                <div class="form-group form-box">
                                    <input id="password2" onchange="onChanges()" type="password"
                                           class="active input-text input textbox" placeholder="Konfirmasi Password"
                                           name="password2" required="">
                                    <i id="icon" class="flaticon-password"></i>
                                    <i class="form-control-feedback"></i>
                                    <small class="text-danger pull-left"></small>
                                </div>

                                <!-- End Password -->
                                <div class="form-group form-box">
                                    <input id="pin" onchange="onChanges()" type="number" class="input-text textbox"
                                           placeholder="PIN Transaksi Harus 6 Digit" name="pin" required="">
                                    <i class="flaticon-key"></i>
                                    <i class="form-control-feedback"></i>
                                    <small class="text-danger pull-left"></small>
                                </div>
                                <i class="form-control-feedback"></i>
                                <small class="text-info pull-left"></small>
                                <div class="checkbox clearfix">
                                    <div class="form-check checkbox-theme">
                                        <input class="form-check-input" type="checkbox" value="" id="rememberMe">
                                        <label class="form-check-label" for="rememberMe" style="color:#2f55d4;">
                                            Saya Setuju Dengan Ketentuan Layanan
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <button id="submit" class="btn btn-primary btn-block" name="daftar">Daftar</button>
                                </div>
                                <br/>
                                <p style="color:#2f55d4;">Sudah Punya Akun?<a
                                            href="<?php echo $config['web']['url'] ?>auth/login">
                                        <b style="color:#2f55d4;">Masuk</b></a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Register -->


    <script type="text/javascript">
        var input = document.getElementById('password'),
            icon = document.getElementById('icon');

        icon.onclick = function () {

            if (input.className == 'active') {
                input.setAttribute('type', 'text');
                icon.className = 'flaticon-key';
                input.className = '';

            } else {
                input.setAttribute('type', 'password');
                icon.className = 'flaticon-password';
                input.className = 'active';
            }

        }

        $("#username").on({
            keydown: function (e) {
                if (e.which === 32)
                    return false;
            },
            keyup: function () {
                this.value = this.value.toLowerCase();
            },
            change: function () {
                this.value = this.value.replace(/\s/g, "");

            }
        });
    </script>
    <script type="text/javascript">
        $('#submit').prop('disabled', true);
        $('#submit').addClass('disabled');
        $('#submit').css('cursor', 'not-allowed');

        function onChanges() {
            if (empty($('#nama').val()) || empty($('#email').val()) || empty($('#name').val()) || empty($('#no_hp').val()) || empty($('#username').val()) || empty($('#password').val()) || empty($('#password2').val()) || empty($('#pin').val())) {
                $('#submit').prop('disabled', true);
                $('#submit').addClass('disabled');
                $('#submit').css('cursor', 'not-allowed');
            } else {
                $('#submit').prop('disabled', false);
                $('#submit').removeClass('disabled');
                $('#submit').css('cursor', 'pointer');
            }
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            //submit form validasi
            $('#formInput').submit(function (e) {
                e.preventDefault();
                var valid = true;
                $(this).find('.textbox').each(function () {
                    if (!$(this).val()) {
                        get_error_text(this);
                        valid = false;
                        $('html,body').animate({scrollTop: 0}, "slow");
                    }
                    if ($(this).hasClass('no-valid')) {
                        console.log("Masuk");
                        valid = false;
                        $('html,body').animate({scrollTop: 0}, "slow");
                    }
                });
                swal({
                    title: "Konfirmasi Pendaftaran",
                    text: "Apakah Data di Atas Sudah Benar?",
                    icon: "info",
                    type: "info",
                    showCancelButton: true,
                    buttons: [
                        'Batal',
                        'Yakin!'
                    ],
                }).then(function () {
                    console.log(valid)
                    if (valid) {
                        $.ajax({
                            url: "../cek-register/cek-register.php",
                            type: "POST",
                            data: $('#formInput').serialize(), //serialize() untuk mengambil semua data di dalam form
                            dataType: "html",
                            success: function () {
                                setTimeout(function () {
                                    swal({
                                        title: 'Terdaftar!',
                                        text: 'Pembuatan Akun telah Berhasil, Silahkan Login',
                                        icon: 'success'
                                    }).then(function () {
                                        window.location = "../auth/login.php";// <--- submit form programmatically
                                    });
                                }, 2000);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                setTimeout(function () {
                                    swal("Error", "Tolong Cek Koneksi Lalu Ulangi", "error");
                                }, 2000);
                            }
                        });
                    } else {
                        swal("Membatalkan!", "Silahkan Benarkan Form", "error");
                    }
                })

            });
        });


        function empty(e) {
            switch (e) {
                case "":
                case 0:
                case "0":
                case null:
                case false:
                case typeof (e) == "undefined":
                    return true;
                default:
                    return false;
            }
        }

        function show_xpanel(class_) {
            $('.' + class_).show();
        }

        function hide_xpanel(class_) {
            $('.' + class_).hide();
        }
    </script>

    <script src='https://www.google.com/recaptcha/api.js'></script>

<?php
require '../lib/footer_home.php';
?>