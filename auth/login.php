<?php
session_start();
require '../config.php';
require("../lib/class.phpmailer.php");
$tipe = "Masuk";

        if (isset($_SESSION['user'])) {
            header("Location: ".$config['web']['url']);
        } else {

            if (isset($_POST['masuk'])) {
                $username = $conn->real_escape_string(trim(filter($_POST['username'])));
                $password = $conn->real_escape_string(trim(filter($_POST['password'])));

                $cek_pengguna = $conn->query("SELECT * FROM users WHERE username = '$username'");
                $cek_pengguna_ulang = mysqli_num_rows($cek_pengguna);
                $data_pengguna = mysqli_fetch_assoc($cek_pengguna);

                $verif_password = password_verify($password, $data_pengguna['password']);

                $error = array();
                if (empty($username)) {
        		    $error ['username'] = '*Tidak Boleh Kosong';
                } else if ($cek_pengguna_ulang == 0) {
        		    $error ['username'] = '*Pengguna Tidak Terdaftar';
                }
                if (empty($password)) {
        		    $error ['password'] = '*Tidak Boleh Kosong';
                } else if ($verif_password <> $data_pengguna['password']) {
                    $error ['username'] = '*Username Atau Password Salah';
                } else {

                if ($data_pengguna['status'] == "Tidak Aktif") {
                    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Akun Sudah Tidak Aktif.<script>swal("Gagal!", "Akun Sudah Tidak Aktif.", "error");</script>');

//                } else if ($data_pengguna['status_akun'] == "Belum Verifikasi") {
//                    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Akun Kamu Belum Di Verifikasi.<script>swal("Gagal!", "Akun Kamu Belum Di Verifikasi.", "error");</script>');
//
                }
                else {
                        if ($cek_pengguna_ulang == 1) {
						if ($verif_password == true) {
                          
			$pengguna = $data_pengguna['username'];
			$namalengkap = $data_pengguna['nama'];
            $dtindo = tanggal_indo($data_pengguna['date']);
            $email = $data_pengguna['email'];
	        $mail = new PHPMailer;
	        $mail->IsSMTP();
	        $mail->SMTPSecure = 'ssl'; 
            $mail->Host = "mail.iaccesoris.com"; //host masing2 provider email
            $mail->SMTPDebug = 2;
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->Username = "no-reply@iaccesoris.com"; //user email
            $mail->Password = "Mask!@#$";  //password email
            $mail->SetFrom("no-reply@iaccesoris.com","Iaccesoris"); //set email pengirim
	        $mail->Subject = "Kamu Telah Masuk ".get_client_ip()." "; //subyek email
	        $mail->AddAddress("$email","");  //tujuan email
	        $mail->MsgHTML("<html>

<head>
    
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name='author' content='Konterin'>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.0.7/css/all.css'>
    <link rel='shortcut icon' href='https://konter.in/assets/media/logos/faviconn.png' />
    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet'>
    <style type='text/css'>
        body,table,td,a{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{mso-table-lspace:0pt;mso-table-rspace:0pt}table{border-collapse:collapse!important}body{font-family:'Dosis';height:100%!important;margin:0!important;padding:0!important;width:100%!important}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}@media screen and (max-width:600px){h1{font-size:32px!important;line-height:32px!important}}div[style*='margin: 16px 0;']{margin:0!important}
    </style>
</head>

<body>
<p>Ini adalah email otomatis yang dikirim oleh sistem. Mohon untuk tidak membalas ke email ini karena kami tidak memantau pesan yang masuk ke email ini. Untuk bertanya/keluhan, silahkan menghubungi kami melalui whatsapp</p>
<p>Hallo <b>$pengguna</b> Kamu telah masuk dengan IP ".get_client_ip()." </p>
<p>Pada Tanggal <b>$dtindo</b> Pukul <b>$time</b></p>
<b>Terima Kasih</b>
<font color='#888888'><br>
Konter.in<br>
<br>
</font>
</body>
</html>");
	        if ($mail->Send());
                                $conn->query("INSERT INTO aktifitas VALUES ('','$username', 'Masuk', '".get_client_ip()."','$date','$time')");
                                $_SESSION['user'] = $data_pengguna;
                
                                $cookie_name = "username";
                                $cookie_value = $username;
                                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
                                exit(header("Location: ".$config['web']['url']));
                            } else {
                                $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
                            }
                        }
                    }
                }
            }
        }

        require '../lib/header_home.php';

?>

        <!-- Start Page Login -->
        <div class="login-2 bg-white" >
 <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-section" style="background: #ffffff;">

                           <img src="/img/logo.png" style="width: 150px;">
                            <?php
                            if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                                <?php echo $_SESSION['hasil']['user'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                            }
                            ?>

                            <div class="login-inner-form">
                                <form class="form-horizontal" role="form" method="POST">
                                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                                    <div class="form-group form-box">
                                        <input type="text" name="username" class="active input-text" placeholder="Username" value="<?php echo $username; ?>">
                                        <i class="flaticon-user"></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['username']) ? $error['username'] : '';?></small>
                                    </div>
                                    <div class="form-group form-box">
                                        <input id="pswd" type="password" name="password" class="active input-text" placeholder="Password">
                                        <i id="icon" class="flaticon-password"></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['password']) ? $error['password'] : '';?></small>
                                    </div>
                                    <div class="checkbox clearfix">
                                        <div class="form-check checkbox-theme">
                                            <input hidden class="form-check-input" type="checkbox" value="$username" id="rememberMe">
                                            <label style="color:#2f55d4;" class="form-check-label" for="rememberMe">
                                                Remember me
                                            </label>
                                        </div>
                                        <span class="pull-right"> </span><a style="color:#2f55d4;" href="<?php echo $config['web']['url'] ?>auth/forgot-password">Lupa Kata Sandi?</a></span>
                                    </div>
                              <div class="row">
                                    <div class="form-group mb-1 col-md-6">
                                        <button type="submit" class="btn btn-primary btn-block" name="masuk"><i class="flaticon-lock"></i> Masuk</button>
									 </div>	  
                                    <div class="form-group mb-1 col-md-6">
                                      <a href="<?php echo $config['web']['url'] ?>auth/register" class="btn btn-info btn-block"><i class="flaticon-users-1"></i> Daftar</a>
									 </div>
									     </div>                               
                                    <br />
<!--									 <div class="form-group mb-0 ">-->
<!--									<b> Belum Verifikasi akun? </b>-->
<!--									 	<a href="--><?php //echo $config['web']['url'] ?><!--auth/verification-account" class="btn btn-warning btn-block"><i class="flaticon2-warning"></i> Verifikasi Akun</a>-->
<!--									  </div>-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Login -->
<script type="text/javascript">
var input = document.getElementById('pswd'),
    icon = document.getElementById('icon');

   icon.onclick = function () {

     if(input.className == 'active') {
        input.setAttribute('type', 'text');
        icon.className = 'flaticon-key';
       input.className = '';

     } else {
        input.setAttribute('type', 'password');
        icon.className = 'flaticon-password';
       input.className = 'active';
    }

   }
</script>

<?php
require '../lib/footer_home.php';
?>