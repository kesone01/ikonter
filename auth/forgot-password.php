<?php
session_start();
require("../config.php");
require("../lib/class.phpmailer.php");
$tipe = "Lupa Kata Sandi";

        if (isset($_POST['lupa'])) {
            $email = $conn->real_escape_string(filter(trim($_POST['email'])));

            $cek_pengguna = $conn->query("SELECT * FROM users WHERE email = '$email'");
            $cek_pengguna_ulang = mysqli_num_rows($cek_pengguna);
            $data_pengguna = mysqli_fetch_assoc($cek_pengguna);

            $error = array();
            if (empty($email)) {
    		    $error ['email'] = '*Tidak Boleh Kosong';
            } else if ($cek_pengguna_ulang == 0) {
    		    $error ['email'] = '*Email Tidak Ditemukan';
            } else {

            $acakin_password = acak(10).acak_nomor(10);
            $hash_pass = password_hash($acakin_password, PASSWORD_DEFAULT);
			$pengguna = $data_pengguna['username'];
            $mail = new PHPMailer;
            $mail->IsSMTP();
	        $mail->SMTPSecure = 'ssl';            
           $mail->Host = "mail.iaccesoris.com"; //host masing2 provider email
            $mail->SMTPDebug = 2;
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->Username = "no-reply@iaccesoris.com"; //user email
            $mail->Password = "Mask!@#$";  //password email
            $mail->SetFrom("no-reply@iaccesoris.com","Iaccesoris"); //set email pengirim
	        $mail->Subject = "Reset Kata Sandi Akun Anda"; //subyek email
	        $mail->AddAddress("$email","");  //tujuan email
            $mail->MsgHTML("<html>

<head>
    
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name='author' content='Konterin'>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.0.7/css/all.css'>
    <link rel='shortcut icon' href='https://konter.in/assets/media/logos/faviconn.png' />
    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet'>
    <style type='text/css'>
        body,table,td,a{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{mso-table-lspace:0pt;mso-table-rspace:0pt}table{border-collapse:collapse!important}body{font-family:'Dosis';height:100%!important;margin:0!important;padding:0!important;width:100%!important}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}@media screen and (max-width:600px){h1{font-size:32px!important;line-height:32px!important}}div[style*='margin: 16px 0;']{margin:0!important}
    </style>
</head>

<body>
<p>Ini adalah email otomatis yang dikirim oleh sistem. Mohon untuk tidak membalas ke email ini karena kami tidak memantau pesan yang masuk ke email ini. Untuk bertanya/keluhan, silahkan menghubungi kami melalui whatsapp</p>
<p>Hallo <b>$pengguna</b></p>
<p>Kamu telah melakukan permintaan reset password pada akun kamu</p>
<p>Berikut ini adalah password baru kamu <b>$acakin_password</b></p>
<b>Terima Kasih</b>
<font color='#888888'><br>
Konter.in<br>
<br>
</font>
</body>
</html>");
            if ($mail->Send());
                if ($conn->query("UPDATE users SET password = '$hash_pass', random_kode = '$acakin_password' WHERE username = '".$data_pengguna['username']."'") == true) {
                    $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Permintaan Kata Sandi Baru Telah Dikirim Ke Email Kamu atau cek di forder spam.<script>swal("Berhasil!", "Kata Sandi Baru Telah Dikirim Ke Email Kamu atau cek di forder spam.", "success");</script>');
                } else {
                    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');    
                }
            }
        }

        require '../lib/header_home.php';

?>

        <!-- Start Page Forgot Password -->
        <div class="login-2 bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-section" style="background: #fffff;">
                            <h3 style="color:#2f55d4;">Lupa Kata Sandi</h3>
                            <?php
                            if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                            }
                            ?>
                            <div class="login-inner-form">
                                <form class="form-horizontal" role="form" method="POST">
                                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                                    <div class="form-group form-box">
                                        <input type="email" class="input-text" placeholder="Masukkan Email" name="email" value="<?php echo $email; ?>">
                                        <i class="flaticon-mail"></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['email']) ? $error['email'] : '';?></small>
                                    </div>
                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-primary btn-block" name="lupa">Submit</button>
                                    </div>
                                    <br />
                                    <p style="color:black;">Sudah Punya Akun?<a href="<?php echo $config['web']['url'] ?>auth/login"> 
                                    <b style="color:blue;">Masuk</b></a></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Forgot Password -->

<?php
require '../lib/footer_home.php';
?>