<?php
session_start();
require("../config.php");
require("../lib/class.phpmailer.php");
$tipe = "Verifikasi Akun";
$username = $conn->real_escape_string(filter(trim($_COOKIE['username'])));
$kode = $conn->real_escape_string(filter(trim($_POST['kode'])));
$cek_kode = $conn->query("SELECT * FROM users WHERE kode_verifikasi = '$kode'");
$cek_kode_ulang = mysqli_num_rows($cek_kode);
$data_kode = mysqli_fetch_assoc($cek_kode);

$cek_referral = $conn->query("SELECT * FROM setting_referral WHERE status = 'Aktif'");
$cek_referral_ulang = mysqli_num_rows($cek_referral);
$data_referral = mysqli_fetch_assoc($cek_referral);
$error = array();
$data = $conn->query("Select * from users where username = '$username'");
$data_pengguna = mysqli_fetch_assoc($data);
if (empty($kode)) {
    $error ['kode'] = '*Tidak Boleh Kosong';
} else if ($data_pengguna['kode_verifikasi'] <> $kode) {
    $error ['kode'] = '*Kode Verifikasi Tidak Sesuai';
} else {
    if ($data_pengguna['status_akun'] == "Sudah Verifikasi") {
        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Akun Kamu Sudah Di Verifikasi.<script>swal("Gagal!", "Akun Kamu Sudah Di Verifikasi.", "error");</script>');
    }
        if ($conn->query("UPDATE users SET status_akun = 'Sudah Verifikasi' WHERE username = '" . $data_kode['username'] . "'") == true) {

            $conn->query("UPDATE users SET koin = koin+" . $data_referral['jumlah'] . " WHERE username = '" . $data_pengguna['uplink_referral'] . "'");

            $conn->query("INSERT INTO riwayat_saldo_koin VALUES ('', '" . $data_pengguna['uplink_referral'] . "', 'Koin', 'Penambahan Koin', '" . $data_referral['jumlah'] . "', 'Mendapatkan Koin Melalui Referral Akun Dengan Nama Pengguna :  " . $data_kode['username'] . "', '$date', '$time')");

            $conn->query("INSERT INTO riwayat_referral VALUES ('', '" . $data_kode['username'] . "', '" . $data_pengguna['uplink_referral'] . "', '" . $data_pengguna['kode_referral'] . "', '" . $data_referral['jumlah'] . "', '$date', '$time')");

            $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip, Verifikasi Akun Berhasil, Silahkan Login Untuk Melanjutkan Transaksi. <script>swal({title: "Berhasil!", text: "Akun Kamu Berhasil Di Verifikasi.", icon: "success"}).then(function(){window.location="/auth/login";});</script>');
        } else {
            $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
        }
}
require '../lib/header_home.php';

?>
    <div class="login-2 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-section" style="background: #ffffff;">
                        <img src="/img/logo.png" style="width: 150px;">
                        <h3 style="color:#2f55d4;">Verifikasi Akun</h3>
                        <?php
                        if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible"
                                 role="alert">
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                        }
                        ?>
                        <div class="login-inner-form">
                            <form class="form-horizontal" role="form" method="POST">
                                <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">

                                <p style="color:#2f55d4;" id="gambar" class="gambar"> Masukan Kode Verifikasi Kamu yang
                                    kita kirim melalu email!</p><br>

                                <div class="gambar form-group form-box" id="gambar">
                                    <input type="number" name="kode" class="input-text"
                                           placeholder="Masukkan Kode Verifikasi">
                                    <i class="flaticon-key"></i>
                                    <small class="text-danger font-13 pull-right"><?php echo ($error['kode']) ? $error['kode'] : ''; ?></small>
                                </div>
                                <div class="gambar form-group mb-0" id="gambar">
                                    <button type="submit" class="btn btn-primary btn-block" name="verifikasi">
                                        Verifikasi
                                    </button>
                                </div>
                                <br/>
                                <p style="color:#2f55d4;"> Sudah Verifikasi Akun ?<a
                                            href="<?php echo $config['web']['url'] ?>auth/login">
                                        <b style="color:#2f55d4;">Masuk</b></a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Verification Account -->
    <script type="text/javascript" src="jquery-1.7.min.js"></script>

    <!-- </script>-->
<?php
require '../lib/footer_home.php';
?>